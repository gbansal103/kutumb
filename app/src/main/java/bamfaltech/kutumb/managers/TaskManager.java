package bamfaltech.kutumb.managers;

import android.os.Handler;
import android.os.Message;

public class TaskManager {
    private static TaskManager mInstanse;
    private final String TAG = "Task_Manager";
    //private final int POOL_SIZE = 5;
    //private final ExecutorService mThreadPool;

    /*private TaskManager() {
        mThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }*/

    public static TaskManager getInstanse() {
        if (mInstanse == null) {
            mInstanse = new TaskManager();
        }
        return mInstanse;
    }

    /*public void queueJob(final TaskListner taskListner) {
        *//*final Handler handler = new Handler(){
            @Override
			public void handleMessage(android.os.Message msg) {
				taskListner.onBackGroundTaskCompleted();
			};
		};*//*
        final TaskHandler taskHandler = new TaskHandler(taskListner);
        mThreadPool.submit(new Runnable() {

            @Override
            public void run() {
                taskListner.doBackGroundTask();
                Message message = Message.obtain();
                message.obj = "Task Performed";
                taskHandler.sendMessage(message);
            }
        });
    }*/

    public static interface TaskListner {
        public void doBackGroundTask();

        public void onBackGroundTaskCompleted();
    }

    /**
     * NonStatic inner handlers won't be eligible for garbage collection
     *
     * @author deweshkumar
     */
    static class TaskHandler extends Handler {
        //private final WeakReference<TaskListner> taskListner;
        private final TaskListner taskListner;

        TaskHandler(TaskListner taskListner) {
            //this.taskListner = new WeakReference<TaskListner>(taskListner);
            this.taskListner = taskListner;
        }

        @Override
        public void handleMessage(Message msg) {
            if (this.taskListner != null) {
                this.taskListner.onBackGroundTaskCompleted();
            }
        }
    }
}
