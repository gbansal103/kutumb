package bamfaltech.kutumb.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 21/12/16.
 */

public class GoogleLoginManager implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks{

    private static GoogleLoginManager mInstance = null;
    private String TAG = "GoogleLoginManager";
    private FirebaseAuth mAuth;
    private Context mContext;
    private FirebaseManager mFirebaseManager;
    private static GoogleApiClient mGoogleApiClient;

    public static GoogleLoginManager getInstance(){
        if(mInstance == null){
            mInstance = new GoogleLoginManager();
        }
        return mInstance;
    }


    public void initialize(Context context){
        this.mContext = context;
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mContext.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        if(mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                    .enableAutoManage((FragmentActivity) mContext /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addConnectionCallbacks(this)
                    .build();
        }
        mFirebaseManager = FirebaseManager.getInstance();
        mAuth = FirebaseManager.getInstance().getFirebaseAuth();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void setupGoogleSignin(SignInButton signInButton){
        signInButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignInGoogle:
                Util.showPB(mContext, "Logging in...");
                mGoogleApiClient.connect();
                signIn();
                break;
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        ((Activity)mContext).startActivityForResult(signInIntent, Constants.GOOGLE_REQUEST_SIGNIN);
    }

    public void signOut() {
        if(mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            mGoogleApiClient.disconnect();
                            Toast.makeText(mContext, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
                            //Log.v("gb103", status.getStatusMessage());
                        }
                    });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        Util.hidePB();
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            // Google Sign In was successful, authenticate with Firebase
            GoogleSignInAccount account = result.getSignInAccount();
            firebaseAuthWithGoogle(account);
        } else {
            // Google Sign In failed, update UI appropriately
            // ...
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        Util.showPB(mContext, "Logging in as...");
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        Constants.LOGIN_TYPE = Constants.LOGIN_TYPES.GOOGLE.ordinal();
        mFirebaseManager.signIn(credential);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
