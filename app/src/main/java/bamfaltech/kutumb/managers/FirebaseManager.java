package bamfaltech.kutumb.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.BasicInfoActivity;
import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.MainActivity;
import bamfaltech.kutumb.models.ChatModel;
import bamfaltech.kutumb.models.FileModel;
import bamfaltech.kutumb.models.NotificationModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.services.Serializer;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;
import bamfaltech.kutumb.util.Util;

/*import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;*/

/**
 * Created by gaurav.bansal1 on 21/12/16.
 */

public class FirebaseManager {

    private String TAG = "FirebaseManager";
    static FirebaseManager mFirebaseManager = null;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase;
    private Context mContext;


    public static FirebaseManager getInstance(){
        if(mFirebaseManager == null){
            mFirebaseManager = new FirebaseManager();
        }
        return mFirebaseManager;
    }

    public void initializeFirebaseAuth(Context context){
        this.mContext = context;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    Util.hidePB();
                    launchCompleteProfilePage(user);
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };
    }

    public FirebaseAuth getFirebaseAuth(){
        return mAuth;
    }

    public void start(){
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void stop(){
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void signInWithPhone(final String phoneNumber) {
        final String email = phoneNumber + "@bamfaltech.com";
        mAuth.signInWithEmailAndPassword(email, phoneNumber)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()) {
                            mAuth.createUserWithEmailAndPassword(email, phoneNumber)
                                    .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            Toast.makeText(mContext, "Create New User",
                                                    Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        }
                    }
                });
    }

    public void signIn(AuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(mContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    public void signout(){
        if(Constants.LOGIN_TYPE == Constants.LOGIN_TYPES.FACEBOOK.ordinal()) {
            FacebookLoginManager.getInstance().logout();
        } else if(Constants.LOGIN_TYPE == Constants.LOGIN_TYPES.GOOGLE.ordinal()) {
            GoogleLoginManager.getInstance().signOut();
        }
        mAuth.signOut();
        Constants.LOGIN_TYPE = -1;
        Intent loginIntent = new Intent(mContext, MainActivity.class);
        mContext.startActivity(loginIntent);
    }


    private void launchCompleteProfilePage(FirebaseUser user) {
        //hideProgressDialog();
        /*if(user != null) {
            initiateAppLozicUser(user);
        }*/

        KutumbApplication.sessionCount = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_APPLICATION_SESSION_COUNT, 0, false);

        boolean isProfilePageCompleted = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_COMPLETE_USER_PROFILE, false, true);

        if(user == null) {
            return;
        }

        if (!isProfilePageCompleted) {
            Log.v(TAG, user.getDisplayName()+user.getEmail()+user.getUid());
            String modelID = Util.getDeviceID(user);
            Util.sendRegistrationIdToBackend(modelID, Util.getRegistrationId(Constants.PREFERENCE_REGISTRATION_ID));
            Intent intent = new Intent();
            intent.putExtra("userID", user.getUid());
            intent.putExtra("userName", user.getDisplayName());
            intent.putExtra("userMailID",user.getEmail());
            intent.putExtra("userPhotoLink", user.getPhotoUrl());
            intent.putExtra("fromStart", true);
            intent.setClass(mContext, BasicInfoActivity.class);
            mContext.startActivity(intent);




            //findViewById(R.id.button_facebook_login).setVisibility(View.GONE);
            //findViewById(R.id.button_facebook_signout).setVisibility(View.VISIBLE);
        } else {
            //mStatusTextView.setText(R.string.signed_out);
            //mDetailTextView.setText(null);

            //findViewById(R.id.button_facebook_login).setVisibility(View.VISIBLE);
            //findViewById(R.id.button_facebook_signout).setVisibility(View.GONE);
            Intent intent = new Intent(mContext, KutumbActivity.class);
            intent.putExtra("userPhotoLink", user.getPhotoUrl().toString());
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            mContext.startActivity(intent);
        }

        ((MainActivity)mContext).finish();
    }

    public void getFrontUserInfoFromFirebase(final String regid, final Interfaces.OnUserModelRetrieved onUserModelRetrieved) {
        //UserModel userModel = null;
        if(regid == null) {
            onUserModelRetrieved.onError("regID is null");
        }
        DatabaseReference firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mRef = firebaseDatabaseReference.child("users").child(regid);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UserModel userModel = dataSnapshot.getValue(UserModel.class);
                if(userModel != null) {
                    userModel.setId(regid);
                    onUserModelRetrieved.onRetreivalComplete(userModel);
                } else {
                    onUserModelRetrieved.onError("No user");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    private FirebaseUser getCurrentFirebaseUser(){
        FirebaseUser firebaseUser = null;
        if(firebaseUser == null) {
            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            firebaseUser = firebaseAuth.getCurrentUser();
        }
        return firebaseUser;
    }

    public UserModel getCurrentUser() {
        UserModel userModel = null;
        String savedUserInfo = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_USER_INFO, null, false);
        if(savedUserInfo != null) {
            userModel = (UserModel) Serializer.deserialize(savedUserInfo);
        }
        if(userModel == null) {
            FirebaseUser firebaseUser = getCurrentFirebaseUser();
            userModel = new UserModel(firebaseUser.getDisplayName(), firebaseUser.getPhotoUrl().toString(), firebaseUser.getUid(), firebaseUser.getEmail());
        }
        return userModel;
    }

    public void completeProfile(UserModel userModel){
        DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_COMPLETE_USER_PROFILE, true, true);
        mDatabase.child("users").child(userModel.getId()).setValue(userModel);
        DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_USER_INFO, Serializer.serialize(userModel), false);
    }

    public void saveNotification(NotificationModel notificationModel){
        String key = mDatabase.child("user-notifications/" + getCurrentUser().getId()).push().getKey();
        notificationModel.setId(key);
        Map<String, Object> postValues = notificationModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        //childUpdates.put("/notifications/" + key, postValues);
        childUpdates.put("/user-notifications/" + getCurrentUser().getId() + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        if(mNotificationListener != null) {
            mNotificationListener.onNotificationChanged(1);
        }
    }

    Interfaces.NotificationChangedListener mNotificationListener;
    public void setNotificationListener(Interfaces.NotificationChangedListener notificationListener) {
        this.mNotificationListener = notificationListener;
    }

    String userName = "";
    UserModel userModel;
    public UserModel getFrontUserName(String userId, final Toolbar toolbar) {
        userName = "";
        userModel = new UserModel();
        Query query = mDatabase.child("users").equalTo(userId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String,Object> value = (Map<String, Object>) dataSnapshot.getValue();
                userName = String.valueOf(value.get("name"));
                userModel.setName(userName);
                userModel.setFCMRegID(String.valueOf(value.get("fcm_reg_id")));
                toolbar.setTitle(userName);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map<String,Object> value = (Map<String, Object>) dataSnapshot.getValue();
                userName = String.valueOf(value.get("name"));
                userModel.setName(userName);
                userModel.setFCMRegID(String.valueOf(value.get("fcm_reg_id")));
                toolbar.setTitle(userName);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Map<String,Object> value = (Map<String, Object>) dataSnapshot.getValue();
                userName = String.valueOf(value.get("name"));
                userModel.setName(userName);
                userModel.setFCMRegID(String.valueOf(value.get("fcm_reg_id")));
                toolbar.setTitle(userName);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
        return userModel;
    }

    public void getTopicList(DatabaseReference ref, final Interfaces.OnRetrievalCompleteListener onRetrievalCompleteListener) {
        final List<String> modelStringList = new ArrayList<>();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot topicSnapShot : dataSnapshot.getChildren()) {
                    TopicModel topicModel = topicSnapShot.getValue(TopicModel.class);
                    modelStringList.add(topicModel.getTopic());
                }
                if(onRetrievalCompleteListener != null) {
                    onRetrievalCompleteListener.onRetrievalCompleted(modelStringList);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(onRetrievalCompleteListener != null) {
                    onRetrievalCompleteListener.onError(databaseError.getMessage());
                }
            }
        });
    }

    public Query getTopicSpecificCatagory(String topic) {
        Query query = mDatabase.child("posts").orderByChild("title").equalTo(topic);
        return query;
    }

    public Query getUtilitiesSpecificCatagory(String catagory) {
        Query query = mDatabase.child("utility").orderByChild("title").equalTo(catagory);
        return query;
    }

    public Query getFlatMemebers(String flatno) {
        //return mDatabase.child("users").orderByChild("flat_number").equalTo(flatno);
        Query query = mDatabase.child("users").orderByChild("flatNumber").equalTo(flatno);
        return query;
    }

    public void sendFileFirebase(StorageReference storageReference, final Uri file, final Interfaces.OnUploadFileCompleteListener onUploadFileCompleteListener){
        if (storageReference != null){
            final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
            StorageReference imageGalleryRef = storageReference.child(name+"_gallery");
            UploadTask uploadTask = imageGalleryRef.putFile(file);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if(onUploadFileCompleteListener != null) {
                        onUploadFileCompleteListener.onError(e.getMessage());
                    }
                    //mChatFirebaseAdapter.setFileSendingInProgress(false);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG,"onSuccess sendFileFirebase");

                    //mChatFirebaseAdapter.setFileSendingInProgress(false);
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img", downloadUrl.toString(),name,"");

                    if(onUploadFileCompleteListener != null) {
                        onUploadFileCompleteListener.onUploadCompleted(fileModel);
                    }
                }
            });
        }else{
            if(onUploadFileCompleteListener != null) {
                onUploadFileCompleteListener.onError("storageReference is null");
            }
        }

    }

    /**
     * Envia o arvquivo para o firebase
     */
    public void sendFileFirebase(StorageReference storageReference, final File file, final Interfaces.OnUploadFileCompleteListener onUploadFileCompleteListener){
        if (storageReference != null){
            UploadTask uploadTask = storageReference.putFile(Uri.fromFile(file));
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if(onUploadFileCompleteListener != null) {
                        onUploadFileCompleteListener.onError(e.getMessage());
                    }
                    //mChatFirebaseAdapter.setFileSendingInProgress(false);
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.i(TAG,"onSuccess sendFileFirebase");
                    //mChatFirebaseAdapter.setFileSendingInProgress(false);
                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                    FileModel fileModel = new FileModel("img",downloadUrl.toString(),file.getName(),file.length()+"");
                    if(onUploadFileCompleteListener != null) {
                        onUploadFileCompleteListener.onUploadCompleted(fileModel);
                    }
                }
            });
        }else{
            if(onUploadFileCompleteListener != null) {
                onUploadFileCompleteListener.onError("storageReference is null");
            }
        }

    }

    public void removeNodeById(DatabaseReference mRef, String tableName, String id){
        Query tableQuery = mRef.child(tableName).child(id);

        tableQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
    }

    public void removeNodeById(DatabaseReference mRef, String tableName, String nextLevelID, String id){
        Query tableQuery;
        if(TextUtils.isEmpty(nextLevelID)) {
            tableQuery = mRef.child(tableName).child(id);
        } else {
            tableQuery = mRef.child(tableName).child(nextLevelID).child(id);
        }

        tableQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot: dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
    }


}
