package bamfaltech.kutumb.managers;

import java.util.ArrayList;

public class TaskActivityMap {
    private int taskId = -1;
    private ArrayList<TaskManager.TaskListner> arrLstTaskListner;

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public ArrayList<TaskManager.TaskListner> getArrLstTaskListner() {
        if (arrLstTaskListner == null) {
            arrLstTaskListner = new ArrayList<TaskManager.TaskListner>();
        }
        return arrLstTaskListner;
    }

    public void setArrLstUrl(ArrayList<TaskManager.TaskListner> arrLstTaskListner) {
        this.arrLstTaskListner = arrLstTaskListner;
    }
}
