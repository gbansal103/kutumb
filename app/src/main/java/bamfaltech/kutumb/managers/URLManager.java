package bamfaltech.kutumb.managers;

//import com.android.volley.Request;

import com.android.volley.Request;

import java.io.Serializable;
import java.util.HashMap;

public class URLManager implements Serializable {
    private static final long serialVersionUID = 1L;
    private BusinessObjectType businessObjectType;
    private Class<?> className = null;
    private BusinessObjectType parentBusinessObjectType;
    private String finalUrl = null;
    private HashMap<String, String> hmpParams;
    private UserType mUserType;
    private Boolean isCachable = true;
    private Boolean hasLoadMoreEnabled = false;
    private int cachingDurationInMinutes = -1;
    private boolean isHourlyPlaylistUrl = false;
    private Boolean isToBeRefresh = false;
    private boolean isLocalMedia = false;
    private boolean isSearchFromMyMusic = false;
    private boolean includeDownlaodedItems = false;
    private boolean includeFavoriteItems = false;
    private boolean isDynamicCache = false;
    private String searchString = null;
    private int method = Request.Method.GET;
    private boolean isTranslationRequired = true;

    private boolean parseItemsIntoTrack = false;
    public boolean isParseItemsIntoTrack() {
        return parseItemsIntoTrack;
    }

    public void setParseItemsIntoTrack(boolean parseItemsIntoTrack) {
        this.parseItemsIntoTrack = parseItemsIntoTrack;
    }

    private Request.Priority priority = Request.Priority.NORMAL;

    public URLManager() {
    }

    public void setIsHourlyPlaylistUrl(boolean isHourlyPlaylistUrl) {
        this.isHourlyPlaylistUrl = isHourlyPlaylistUrl;
    }

    public boolean isHourlyPlaylistUrl() {
        return isHourlyPlaylistUrl;
    }

    public int getCachingDurationInMinutes() {
        return cachingDurationInMinutes;
    }

    public void setCachingDurationInMinutes(int minutes) {
        this.cachingDurationInMinutes = minutes;
    }

    public Boolean hasLoadMoreEnabled() {
        return hasLoadMoreEnabled;
    }

    public void setLoadMoreOption(Boolean hasLoadMoreEnabled) {
        this.hasLoadMoreEnabled = hasLoadMoreEnabled;
    }

    public void setCachable(Boolean isCacheble) {
        this.isCachable = isCacheble;
    }
    public Boolean isCacheble() {
        return this.isCachable;
    }

    public UserType getUserType() {
        return mUserType;
    }

    public void setUserType(UserType mUserType) {
        this.mUserType = mUserType;
    }

    public HashMap<String, String> getParams() {
        return hmpParams;
    }

    public void setParams(HashMap<String, String> hmpParams) {
        this.hmpParams = hmpParams;
    }

    public BusinessObjectType getBusinessObjectType() {
        return businessObjectType;
    }

    public void setBusinessObjectType(BusinessObjectType businessObjectType) {
        this.businessObjectType = businessObjectType;
    }

    public void setClassName(final Class<?> className){
        this.className = className;
    }

    public Class<?> getClassName(){
        return this.className;
    }

    public String getFinalUrl() {
        return finalUrl;
    }

    public void setFinalUrl(String finalUrl) {
        this.finalUrl = finalUrl;
    }

    public BusinessObjectType getParentBusinessObjectType() {
        return parentBusinessObjectType;
    }

    public void setParentBusinessObjectType(
            BusinessObjectType parentBusinessObjectType) {
        this.parentBusinessObjectType = parentBusinessObjectType;
    }

    public Boolean isDataToBeRefreshed() {
        return this.isToBeRefresh;
    }

    public void setDataRefreshStatus(Boolean isToBeRefresh) {
        this.isToBeRefresh = isToBeRefresh;
    }

    public boolean isLocalMedia() {
        return isLocalMedia;
    }

    public void setLocalMedia(boolean isLocalMedia) {
        this.isLocalMedia = isLocalMedia;
    }

    public boolean isSearchFromMyMusic() {
        return isSearchFromMyMusic;
    }

    public void setSearchFromMyMusic(boolean searchFromMyMusic) {
        isSearchFromMyMusic = searchFromMyMusic;
    }

    public boolean isDynamicCache() {
        return isDynamicCache;
    }

    public void setDynamicCache(boolean isDynamicCache) {
        this.isDynamicCache = isDynamicCache;
    }

    public enum BusinessObjectType {Tracks, Artists, Albums, Geners, Playlists, Charts, User, Friends,
                                    History, SocialInfo, Activities, Discover, Radios, TopCharts,
                                    ProfileUsers, Notifications, Products, CampaignPromo, BasicResponse,
                                    AppDetails, TrendingSearches, GenericItems, RadioMoods, YouTubeVideos,
                                    DynamicViews,UberResponse, FavoriteData, PlaylistDetails, PersonaDedications, SocialProfile, SocialFollow, SocialFollowing, SocialPending, SocialPendingFollowing, SocialFeed, BlockedUsers, SocialPreferences,Dedicate}

    public enum BusinessObjectViewType {ViewGRID, ViewTransparent, ViewListing}

    public enum UserType {PUBLIC, PRIVATE, FRIENDS}

    public String getSearchString(){
        return searchString;
    }

    public void setSearchString(String searchString){
        this.searchString = searchString;
    }

    public boolean isIncludeFavoriteItems() {
        return includeFavoriteItems;
    }

    public void setIncludeFavoriteItems(boolean includeFavoriteItems) {
        this.includeFavoriteItems = includeFavoriteItems;
    }

    public boolean isIncludeDownlaodedItems() {
        return includeDownlaodedItems;
    }

    public void setIncludeDownlaodedItems(boolean includeDownlaodedItems) {
        this.includeDownlaodedItems = includeDownlaodedItems;
    }
   public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public Request.Priority getPriority(){ return priority; }

    public void setPriority(Request.Priority priority){
        this.priority = priority;
    }

    public int getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }

    private int maxRetry = 0;

    public boolean isTranslationRequired() {
        return isTranslationRequired;
    }

    public void setIsTranslationRequired(boolean isTranslationRequired){
        this.isTranslationRequired = isTranslationRequired;
    }
}
