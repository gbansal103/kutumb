package bamfaltech.kutumb.managers;

import android.os.Handler;
import android.os.Message;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class KutumbTaskManager {

    private static ArrayList<TaskActivityMap> arrListTaskActivityMap = new ArrayList<TaskActivityMap>();
    private static KutumbTaskManager mInstanse;
    private final String TAG = "Task_Manager";
    private final int POOL_SIZE = 3;
    private final ExecutorService mThreadPool;

    private KutumbTaskManager() {
        mThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    public static KutumbTaskManager getInstanse() {
        if (mInstanse == null) {
            mInstanse = new KutumbTaskManager();
        }
        return mInstanse;
    }

    private static Boolean hasCallbackRemoved(int activityTaskId) {
        if (activityTaskId == -1)//Means activityTaskId is not supplied.
        {
            return false;
        }
        if (activityTaskId != -1) {
            for (TaskActivityMap taskUrlMap : arrListTaskActivityMap) {
                if (taskUrlMap.getTaskId() == activityTaskId) {
                    return false;//Activity task id found.Send callback
                }
            }
        }
        return true;
    }

    /**
     * @param taskListner
     * @param activityTaskId :
     *                       -1 = Will force post execute
     *                       all removeCallBacks(getTaskId()) from onDestroy. To be used for cancelling tasks for destroyed activity
     */
    public void queueJob(final TaskManager.TaskListner taskListner, int activityTaskId) {
        queueJob(taskListner, activityTaskId, false);
    }

    /**
     * @param taskListner
     * @param activityTaskId :
     *                       -1 = Will force post execute
     *                       all removeCallBacks(getTaskId()) from onDestroy. To be used for cancelling tasks for destroyed activity
     */

    public void queueJob(final TaskManager.TaskListner taskListner, int activityTaskId, final boolean isMultipleCallAllowed) {
        /*final Handler handler = new Handler(){
            @Override
			public void handleMessage(android.os.Message msg) {
				taskListner.onBackGroundTaskCompleted();
			};
		};*/
        final TaskHandler taskHandler = new TaskHandler(taskListner, activityTaskId);
        addTaskActivityIdMapping(activityTaskId, taskListner);
        mThreadPool.submit(new Runnable() {

            @Override
            public void run() {
                taskListner.doBackGroundTask();
                Message message = Message.obtain();
                message.obj = "Task Performed";
                taskHandler.sendMessage(message);
            }

            @Override
            public boolean equals(Object o) {
                if(isMultipleCallAllowed)
                    return false;
                return super.equals(o);
            }
        });
    }

    /**
     * Will remove all backs associated with Activity if which activity task id has been provided
     *
     * @param activityHashId
     */
    public void removeCallBacks(int activityHashId) {
        int indexToBeremoved = -1;
        for (TaskActivityMap taskUrlMap : arrListTaskActivityMap) {
            if (taskUrlMap.getTaskId() == activityHashId) {
                indexToBeremoved = arrListTaskActivityMap.indexOf(taskUrlMap);
            }
        }
        if (indexToBeremoved != -1) {
            arrListTaskActivityMap.remove(indexToBeremoved);
        }
    }

    private void addTaskActivityIdMapping(int activityTaskId, TaskManager.TaskListner taskListner) {
        if (activityTaskId != -1) {//-1 is the default value
            Boolean isMapExist = false;
            for (TaskActivityMap taskActivityIdMap : arrListTaskActivityMap) {
                if (taskActivityIdMap.getTaskId() == activityTaskId) {
                    taskActivityIdMap.getArrLstTaskListner().add(taskListner);
                    isMapExist = true;
                    break;
                }
            }
            if (!isMapExist) {
                TaskActivityMap taskActivityIdMap = new TaskActivityMap();
                taskActivityIdMap.setTaskId(activityTaskId);
                taskActivityIdMap.getArrLstTaskListner().add(taskListner);
                arrListTaskActivityMap.add(taskActivityIdMap);
            }

        }
    }

    /**
     * NonStatic inner handlers won't be eligible for garbage collection
     *
     * @author deweshkumar
     */
    static class TaskHandler extends Handler {
        private final TaskManager.TaskListner taskListner;
        private int activityTaskId = -1;

        TaskHandler(TaskManager.TaskListner taskListner, int activityTaskId) {
            this.taskListner = taskListner;
            this.activityTaskId = activityTaskId;
        }

        @Override
        public void handleMessage(Message msg) {
            if (this.taskListner != null && !hasCallbackRemoved(activityTaskId)) {
                this.taskListner.onBackGroundTaskCompleted();
            }
        }
    }
}
