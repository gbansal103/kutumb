package bamfaltech.kutumb.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONException;
import org.json.JSONObject;

import bamfaltech.kutumb.MainActivity;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 21/12/16.
 */

public class FacebookLoginManager{

    private static FacebookLoginManager mInstance = null;
    /* The callback manager for Facebook */
    private CallbackManager mFacebookCallbackManager;
    /* Used to track user logging in/out off Facebook */
    private AccessTokenTracker mFacebookAccessTokenTracker;
    private String TAG = "FacebookLoginManager";
    private FirebaseAuth mAuth;
    private Context mContext;
    private FirebaseManager mFirebaseManager;

    public static FacebookLoginManager getInstance(){
        if(mInstance == null){
            mInstance = new FacebookLoginManager();
        }
        return mInstance;
    }


    public void initialize(Context context){
        mFacebookCallbackManager = CallbackManager.Factory.create();
        mFirebaseManager = FirebaseManager.getInstance();
        mAuth = FirebaseManager.getInstance().getFirebaseAuth();
        this.mContext = context;
    }

    public void loginWithFacebook(LoginButton loginButton){
        loginButton.setReadPermissions("email", "public_profile");
        // Callback registration
        loginButton.registerCallback(mFacebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (response.getError() != null) {
                                    // handle error
                                } else {
                                    // get email and id of the user
                                    JSONObject data = response.getJSONObject();
                                    if (data.has("picture")) {
                                        try {
                                            String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                            DeviceResourceManager.getInstance().addToSharedPref("facebook_profile", profilePicUrl, false);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    String email = me.optString("email");
                                    String id = me.optString("id");
                                }

                            }
                        }).executeAsync();
                        handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }


    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        Util.showPB(mContext, "Logging in as...");
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        Constants.LOGIN_TYPE = Constants.LOGIN_TYPES.FACEBOOK.ordinal();
        mFirebaseManager.signIn(credential);
    }

    public void logout(){
        LoginManager.getInstance().logOut();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
