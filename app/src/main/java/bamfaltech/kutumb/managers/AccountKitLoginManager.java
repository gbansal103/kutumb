package bamfaltech.kutumb.managers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.google.firebase.auth.FirebaseAuth;
import com.facebook.accountkit.AccountKit;

import bamfaltech.kutumb.util.Constants;

/**
 * Created by gaurav.bansal1 on 15/03/17.
 */

public class AccountKitLoginManager {


    private static AccountKitLoginManager mInstance = null;
    /* The callback manager for Facebook */
    private CallbackManager mFacebookCallbackManager;
    /* Used to track user logging in/out off Facebook */
    private AccessTokenTracker mFacebookAccessTokenTracker;
    private String TAG = "AccountKitLoginManager";
    private FirebaseAuth mAuth;
    private Context mContext;
    private FirebaseManager mFirebaseManager;


    public static AccountKitLoginManager getInstance(){
        if(mInstance == null){
            mInstance = new AccountKitLoginManager();
        }
        return mInstance;
    }

    public void setParams(Context context) {
        this.mContext = context;
    }

    public void phoneLogin(final View view) {
        final Intent intent = new Intent(mContext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.CODE); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        ((Activity)mContext).startActivityForResult(intent, Constants.ACCOUNTKIT_REQUEST_LOGIN);
    }

    public void emailLogin(final View view) {
        final Intent intent = new Intent(mContext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.EMAIL,
                        AccountKitActivity.ResponseType.CODE); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        ((Activity)mContext).startActivityForResult(intent, Constants.ACCOUNTKIT_REQUEST_LOGIN);
    }

    public void logout() {
        AccountKit.logOut();
    }

    public void signIn() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();

                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                String phoneNumberString = phoneNumber.toString();

                // Get email
                String email = account.getEmail();

                FirebaseManager.getInstance().signInWithPhone(phoneNumberString);
            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
                Log.v("gb103", error.toString());
                Toast.makeText(mContext, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
