package bamfaltech.kutumb.managers;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.android.volley.Request;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import bamfaltech.kutumb.BasicInfoActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.adapter.CustomListAdapter;
import bamfaltech.kutumb.adapter.NothingSelectedSpinnerAdapter;
import bamfaltech.kutumb.fragment.PersonalChatListFragment;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.models.PeopleObject;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.RWADetails;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 19/02/17.
 */

public class HttpManager {

    HttpPost httppost;
    StringBuffer buffer;
    //HttpResponse response;
    HttpClient httpclient;
    static HttpManager mInstance = null;
    String user_name = "";

    private String TAG = "HttpManager";


    public static HttpManager getInstance(){
        if(mInstance == null){
            mInstance = new HttpManager();
        }
        return mInstance;
    }

    List<NameValuePair> nameValuePairs;

    /*public void sendMessage(final String messageToSend, final String frontUserFCMRegID, final String purpose, final String key) {
        if(messageToSend.length() > 0) {
            HashMap<String, String> hmpCredentials;
            UserModel sourceUserModel = FirebaseManager.getInstance().getCurrentUser();
            hmpCredentials = new HashMap<String, String>();
            hmpCredentials.put("senderID", sourceUserModel.getId());
            hmpCredentials.put("senderName", sourceUserModel.getName());
            hmpCredentials.put("senderFlat", sourceUserModel.getFlatNumber());
            hmpCredentials.put("message", messageToSend);
            hmpCredentials.put("registrationIDs", frontUserFCMRegID);
            hmpCredentials.put("sendPurpose", purpose);
            if(key != null) {
                hmpCredentials.put("contentKey", key);
            }

            URLManager urlManager = new URLManager();
            urlManager.setClassName(String.class);
            urlManager.setPriority(Request.Priority.NORMAL);
            urlManager.setCachable(false);
            urlManager.setMethod(Request.Method.POST);
            urlManager.setParams(hmpCredentials);
            urlManager.setFinalUrl(URLConstants.GCM_ENGINE);

            VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
                @Override
                public void onRetrievalCompleted(Object object) {
                    String response = (String) object;
                    if (response.trim().isEmpty()) {
                        Log.d(TAG, "Message Not Sent");
                    }
                }

                @Override
                public void onError(String errorMsg) {

                }
            }, urlManager);
        }
    }*/

    public void sendMessage(final String messageToSend, final String frontUserFCMRegID, final String purpose, final String key) {
        if (messageToSend.length() > 0) {

            Log.i(TAG, "sendMessage");

            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        httpclient = new DefaultHttpClient();
                        httppost = new HttpPost(URLConstants.GCM_ENGINE);
                        nameValuePairs = new ArrayList<NameValuePair>(1);
                        UserModel sourceUserModel = FirebaseManager.getInstance().getCurrentUser();
                        nameValuePairs.add(new BasicNameValuePair("senderID",
                                sourceUserModel.getId()));
                        nameValuePairs.add(new BasicNameValuePair("senderName",
                                sourceUserModel.getName()));
                        nameValuePairs.add(new BasicNameValuePair("senderFlat",
                                sourceUserModel.getFlatNumber()));
                        nameValuePairs.add(new BasicNameValuePair("message",
                                messageToSend));
                        nameValuePairs.add(new BasicNameValuePair(
                                "registrationIDs", frontUserFCMRegID));
                        nameValuePairs.add(new BasicNameValuePair(
                                "sendPurpose", purpose));
                        if(key != null) {
                            nameValuePairs.add(new BasicNameValuePair(
                                    "contentKey", key));
                        }
                        nameValuePairs.add(new BasicNameValuePair("apiKey",
                                Constants.API_KEY));

                        httppost.setEntity(new UrlEncodedFormEntity(
                                nameValuePairs));
                        ResponseHandler<String> responseHandler = new BasicResponseHandler();
                        final String response = httpclient.execute(httppost,
                                responseHandler);
                        Log.i(TAG, "Response : " + response);
                        if (response.trim().isEmpty()) {
                            Log.d(TAG, "Message Not Sent");
                        }

                    } catch (Exception e) {
                        Log.d(TAG, "Exception : " + e.getMessage());
                    }
                }
            };

            thread.start();

        }

    }

    public void sendMessage(final String messageToSend, final String frontUserFCMRegID, final String purpose) {
        sendMessage(messageToSend, frontUserFCMRegID, purpose, null);
    }

    public void getObjectResponse(final String url, final List<NameValuePair> nameValuePairs, final Class<?> targetClass, final Interfaces.OnRetrievalCompleteListener onRetrievalCompleteListener) {
        KutumbTaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            private String response;
            @Override
            public void doBackGroundTask() {
                try {

                    httpclient = new DefaultHttpClient();
                    httppost = new HttpPost(url);
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    response = httpclient.execute(httppost, responseHandler);

                } catch (Exception ex) {
                    Log.d(TAG, "Error : " + ex.getMessage());
                }
            }

            @Override
            public void onBackGroundTaskCompleted() {
                String myCustom_JSONResponse="{\"rwa\":"+response+"}";
                try {
                    Object object = (Object) Util.getObjectResponse(myCustom_JSONResponse, targetClass);
                    if(onRetrievalCompleteListener != null) {
                        onRetrievalCompleteListener.onRetrievalCompleted(object);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, -1);

    }
}
