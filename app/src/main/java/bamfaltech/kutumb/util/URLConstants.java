package bamfaltech.kutumb.util;

/**
 * Created by gaurav.bansal1 on 19/02/17.
 */

public class URLConstants {

    public static final String BASE_KUTUMB_URL = "http://bamfaltech.com/";

    //public static final String BASE_KUTUMB_URL = "http://192.168.1.5/kutumb-server/";

    public static final String SAVE_REG_ID = BASE_KUTUMB_URL + "save_reg_id.php";
    public static final String GET_PERSONAL_CHAT_LIST = BASE_KUTUMB_URL + "get_chat_list.php";
    public static final String GET_RWA_DETAILS = BASE_KUTUMB_URL + "get_rwa_details.php";
    public static final String GET_RWA_NAMES_CITY_SPECIFIC = BASE_KUTUMB_URL + "get_rwa_list.php";
    public static final String INSERT_NEW_CHAT_USER = BASE_KUTUMB_URL + "chat_list.php";
    public static final String GCM_ENGINE = BASE_KUTUMB_URL + "gcm_engine.php";
    public static final String INSERT_CHAT_RECORD = BASE_KUTUMB_URL + "save_chat_record.php";
    public static final String UPDATE_CHAT_RECORD = BASE_KUTUMB_URL + "update_chat_record.php";
    public static final String GET_PERSONAL_CHAT_RECORD_LIST = BASE_KUTUMB_URL + "get_chat_record_list.php?";
    public static final String GET_SOCIETY_IMP_CONTACTS = BASE_KUTUMB_URL + "get_society_contacts.php?";
}
