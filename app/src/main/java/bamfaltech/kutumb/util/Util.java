package bamfaltech.kutumb.util;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.services.Interfaces;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Alessandro Barreto on 23/06/2016.
 */
public class Util {

    public static final String URL_STORAGE_REFERENCE = "gs://bamfaltechchatapp.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "images";
    static HttpPost httppost;
    StringBuffer buffer;
    HttpResponse response;
    static HttpClient httpclient;
    static List<NameValuePair> nameValuePairs;
    String user_name = "";
    static String TAG = "Util";
    private static ConnectivityManager mCM;

    public static void initToast(Context c, String message){
        Toast.makeText(c,message,Toast.LENGTH_SHORT).show();
    }

    public  static boolean verifyConnection(Context context) {
        boolean isConnected;
        ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        isConnected = conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected();
        return isConnected;
    }

    public static String local(String latitudeFinal,String longitudeFinal){
        return "https://maps.googleapis.com/maps/api/staticmap?center="+latitudeFinal+","+longitudeFinal+"&zoom=18&size=280x280&markers=color:red|"+latitudeFinal+","+longitudeFinal;
    }

    public static boolean isEmpty(String str) {
        if(str == null || str.isEmpty()) {
            return true;
        }
        return false;
    }

    public static String[] convertListToStringArray(List<String> list) {
        String[] stringArray = new String[list.size()];
        list.toArray(stringArray);
        return stringArray;
    }

    public static  void bindImage(ImageView imgView, String urlPhoto) {
        Glide.with(imgView.getContext()).load(urlPhoto).centerCrop().into(imgView);
    }

    public static void bindCircularImage(ImageView imgView, String urlPhoto){
        Glide.with(KutumbApplication.getContext()).load(urlPhoto).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(imgView.getContext())).override(40,40).into(imgView);
    }

    public static String getAllCapitalWordsString(String sourceString) {
        if(!TextUtils.isEmpty(sourceString)) {
            String str = sourceString;
            String[] strArray = str.split(" ");
            StringBuilder builder = new StringBuilder();
            for (String s : strArray) {
                String cap = s.substring(0, 1).toUpperCase() + s.substring(1);
                builder.append(cap + " ");
            }
            return builder.toString();
        } else {
            return "";
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use
     * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
     * since the device sends upstream messages to a server that echoes back the
     * message using the 'from' address in the message.
     */
    public static void sendRegistrationIdToBackend(final String user_name, final String regid) {

        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("username", user_name);
        hmpCredentials.put("reg_id", regid);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(String.class);
        urlManager.setPriority(Request.Priority.HIGH);
        urlManager.setCachable(false);
        urlManager.setMethod(Request.Method.POST);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.SAVE_REG_ID);


        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                String status = (String) object;
                if (status != null) {
                    if (status.equalsIgnoreCase("Username already registered")) {
                        showToast("Username already registered");
                    } else {
                        if (status.equalsIgnoreCase("New Device Registered successfully")) {
                            showToast("Device registration successful");
                        }
                    }
                }
            }

            @Override
            public void onError(String errorMsg) {

            }
        }, urlManager);


        /*Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    httpclient = new DefaultHttpClient();
                    httppost = new HttpPost(URLConstants.SAVE_REG_ID);
                    nameValuePairs = new ArrayList<NameValuePair>(1);
                    nameValuePairs.add(new BasicNameValuePair("username",
                            user_name));
                    nameValuePairs.add(new BasicNameValuePair("reg_id", regid));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    final String response = httpclient.execute(httppost,
                            responseHandler);
                    //Log.i(TAG, "Response : " + response);

                    if (response != null) {

                        if (response
                                .equalsIgnoreCase("Username already registered")) {

                            showToast("Username already registered");

                            //hidePB();

                        } else {
                            if (response
                                    .equalsIgnoreCase("New Device Registered successfully")) {

                                //savePreferences(Utils.UserName, user_name);
                                // Persist the regID - no need to register
                                // again.
                                //storeRegistrationId(regid);

                                showToast("Device registration successful");

								*//*runOnUiThread(new Runnable() {
									public void run() {

										setPeopleList();
									}
								});*//*

                            }
                        }

                    }

                } catch (Exception e) {

                    //hidePB();
                    //Log.d(TAG, "Exception : " + e.getMessage());
                }
            }
        };

        thread.start();*/

    }

    public static String getCurrentIPAddress() {
        return "http://192.168.1.5/";
    }


    public static void showToast(final String txt) {
        initToast(getApplicationContext(), txt);
    }

    public static String getRegistrationId(String prefName) {
        final SharedPreferences prefs = Util.getGCMPreferences(prefName);
        String registrationId = prefs.getString(Constants.PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        /*int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }*/
        return registrationId;
    }

    public static void storeRegistrationId(String prefName, String regId) {
        final SharedPreferences prefs = getGCMPreferences(prefName);
        //int appVersion = Utils.getAppVersion();
        //Log.i(TAG, "Saving regId on app version " + appVersion);
        Log.i(TAG, "Reg ID : " + regId);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.PROPERTY_REG_ID, regId);
        //editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    public static SharedPreferences getGCMPreferences(String prefName) {
        return getApplicationContext().getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

   /* public void savePreferences(String key, String value) {
        final SharedPreferences prefs = getGCMPreferences();
        Log.i(TAG, key + " : " + value);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }*/

    public static String getDeviceID(FirebaseUser user){
        String deviceID = "";
        deviceID = user.getUid() /*+ android.os.Build.MODEL*/;
        return deviceID;
    }

    static ProgressDialog pb;
    public static void showPB(Context context, final String message) {

        /*runOnUiThread(new Runnable() {

            @Override
            public void run() {*/
        pb = new ProgressDialog(context);
        pb.setMessage(message);
        pb.show();
        //}
        /*});*/

    }

    public static Object getObjectResponse(String jsonResponse, Class modelClass) {
        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED).create();
        Object modelObject = gson.fromJson(jsonResponse, modelClass);
        return modelObject;
    }

    public static void hidePB() {

        /*runOnUiThread(new Runnable() {

            @Override
            public void run() {*/
        if (pb != null && pb.isShowing())
            pb.dismiss();
            /*}
        });*/

    }

    public static boolean hasInternetAccess(Context context) {
        if (context == null) {
            return false;
        }
        if (mCM == null) {
            mCM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo netInfo = mCM.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected())    // Modified in Rev. 211
        {
            return true;
        }
        return false;
    }

    /**
     * Compare with : ConnectivityManager.TYPE_MOBILE,ConnectivityManager.TYPE_WIFI
     *
     * @param context
     * @return No Network = -1
     */
    public static int getNetworkType(Context context) {
        if (mCM == null) {
            mCM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo netInfo = mCM.getActiveNetworkInfo();
        if (netInfo != null) {
            return netInfo.getType();
        } else {
            return -1;
        }
    }

    public static Bitmap getImageIfExists(Context context, String urlName) {
// Find the SD Card path
        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/KutumbImages/");
        if(dir.exists()) {
            File file = new File(dir, urlName + ".jpg" );
            if(file.exists()) {
                try {
                    Bitmap b = BitmapFactory.decodeStream(new FileInputStream(file));
                    return b;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static void saveImageToGallery(Context context, Bitmap bitmap, String urlName) {
        OutputStream output;
        // Find the SD Card path
        File filepath = Environment.getExternalStorageDirectory();

        // Create a new folder in SD Card
        File dir = new File(filepath.getAbsolutePath()
                + "/KutumbImages/");
        dir.mkdirs();

        // Retrieve the image from the res folder
        /*BitmapDrawable drawable = (BitmapDrawable) principal.getDrawable();
        Bitmap bitmap1 = drawable.getBitmap();*/

        // Create a name for the saved image
        File file = new File(dir, urlName + ".jpg" );

        try {

            output = new FileOutputStream(file);

            // Compress into png format image from 0% - 100%
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, output);
            output.flush();
            output.close();

        }

        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, String.valueOf(filepath));

//        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

    }

}
