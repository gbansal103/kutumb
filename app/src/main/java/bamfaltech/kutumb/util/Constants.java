package bamfaltech.kutumb.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gaurav.bansal1 on 21/12/16.
 */

public class Constants {
    public enum LOGIN_TYPES {FACEBOOK, GOOGLE};
    public static final int GOOGLE_REQUEST_SIGNIN = 101;
    public static final int ACCOUNTKIT_REQUEST_LOGIN = 102;
    public static final List<String> towerList = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E", "F", "G", "H"));
    public static final List<String> flatList = new ArrayList<>(Arrays.asList("N901", "N902", "N903", "N904", "N905", "N906", "N907", "N908"));
    public static List<String> catagoryList = new ArrayList<>();
    public static final String PROPERTY_REG_ID = "PROPERTY_REG_ID";
    public static final String API_KEY = "AIzaSyCH7K8RB4y73mJLotdVnlORjm2xuf_zDgQ";
    public static final String SENDER_ID = "722586070974";
    public static final int UTILITY_CATAGORY_SELECTION = 1;
    public static final int USER_PROFILE_HEADER = 2;
    public static int LOGIN_TYPE = -1;

    public static final int TOPIC_LIST_TYPE_POST = 2001;
    public static final int TOPIC_LIST_TYPE_UTILITY = 2002;

    public static final String COACHMARK_VALUE = "COACHMARK_VALUE";
    public static final String COACHMARK_VALUE_SEARCH = "COACHMARK_VALUE_SEARCH";
    public static final String COACHMARK_VALUE_NOTIFICATION = "COACHMARK_VALUE_NOTIFICATION";
    public static final String COACHMARK_VALUE_NEW_POST = "COACHMARK_VALUE_NEW_POST";
    public static final String COACHMARK_VALUE_NEW_UTILITY = "COACHMARK_VALUE_NEW_UTILITY";
    public static final String COACHMARK_VALUE_NEW_NOTICE = "COACHMARK_VALUE_NEW_NOTICE";
    public static final String COACHMARK_VALUE_NEW_CARPOOL = "COACHMARK_VALUE_NEW_CARPOOL";


    public static final String PREFERENCE_APPLICATION_SESSION_COUNT = "PREFERENCE_APPLICATION_SESSION_COUNT";
    public static final String PREFERENCE_REGISTRATION_ID = "PREFERENCE_REGISTRATION_ID";
    public static final String PREFERENCE_USER_INFO = "PREFERENCE_USER_INFO";
    public static final String PREFERENCE_COMPLETE_USER_PROFILE = "PREFERENCE_COMPLETE_USER_PROFILE";
    public static final String PREFERENCE_CURRENT_RWA_DETAILS = "PREFERENCE_CURRENT_RWA_DETAILS";
    public static final String PREFERENCE_UPDATE_CHAT_LIST = "PREFERENCE_UPDATE_CHAT_LIST";
    public static final String PREFERENCE_BOARD_SCREEN_COMPLETE = "PREFERENCE_BOARD_SCREEN_COMPLETE";

    public static final String NOTIFICATION_PURPOSE_CHAT = "NOTIFICATION_PURPOSE_CHAT";
    public static final String NOTIFICATION_PURPOSE_UTILITY_INTEREST = "NOTIFICATION_PURPOSE_UTILITY_INTEREST";
    public static final String NOTIFICATION_PURPOSE_POST_LIKE = "NOTIFICATION_PURPOSE_POST_LIKE";
    public static final String NOTIFICATION_PURPOSE_NOTICE = "NOTIFICATION_PURPOSE_NOTICE";
    public static final String NOTIFICATION_PURPOSE_NODETAIL = "NOTIFICATION_PURPOSE_NODETAIL";
}
