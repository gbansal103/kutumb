package bamfaltech.kutumb.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;

import java.io.Serializable;

import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.managers.FirebaseManager;

/**
 * Created by gaurav.bansal1 on 28/12/16.
 */

public class DeviceResourceManager {

    private static DeviceResourceManager mDeviceResourceManager = null;
    private SharedPreferences mPref = null;
    private SharedPreferences.Editor mEditor = null;
    private Context mContext;

    private DeviceResourceManager() {
        mContext = KutumbApplication.getContext();
    }

    public static DeviceResourceManager getInstance(){
        if(mDeviceResourceManager == null){
            mDeviceResourceManager = new DeviceResourceManager();
        }
        return mDeviceResourceManager;
    }

    public void addToSharedPref(String prefName, int data, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }

        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        mEditor = mPref.edit();
        mEditor.putInt(prefName, data);
        mEditor.commit();
    }

    public void addToSharedPref(String prefName, String data, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }

        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        mEditor = mPref.edit();
        mEditor.putString(prefName, data);
        mEditor.commit();
    }

    public void addToSharedPref(String prefName, boolean data, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }

        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        mEditor = mPref.edit();
        mEditor.putBoolean(prefName, data);
        mEditor.commit();
    }

    public String getDataFromSharedPref(String prefName, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            if(FirebaseAuth.getInstance().getCurrentUser() == null) {
                return null;
            }
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }
        //		Log.d(LOG_TAG, "Getting Preference: " + prefName);
        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        return mPref.getString(prefName, null);
    }


    public String getDataFromSharedPref(String prefName, String defaultValue, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }
        //		Log.d(LOG_TAG, "Getting Preference: " + prefName);
        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        return mPref.getString(prefName, defaultValue);
    }

    public int getDataFromSharedPref(String prefName, int defaultValue, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }
        //		Log.d(LOG_TAG, "Getting Preference: " + prefName);
        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        return mPref.getInt(prefName, defaultValue);
    }

    public boolean getDataFromSharedPref(String prefName, boolean defaultValue, boolean isUserSpecificPreference) {
        if (isUserSpecificPreference)        // Added in Rev. 149
        {
            prefName = FirebaseAuth.getInstance().getCurrentUser().getUid() + prefName;
        }
        //		Log.d(LOG_TAG, "Getting Preference: " + prefName);
        mPref = mContext.getSharedPreferences(prefName, getSharedPreferenceMode());
        return mPref.getBoolean(prefName, defaultValue);
    }

    private int getSharedPreferenceMode() {
        if (android.os.Build.VERSION.SDK_INT >= 11) {
            //			Log.d("Anshul", "MODE_MULTI_PROCESS");
            return Context.MODE_MULTI_PROCESS;
        } else {
            //			Log.d("Anshul", "MODE_PRIVATE");
            return Context.MODE_PRIVATE;
        }
    }
}
