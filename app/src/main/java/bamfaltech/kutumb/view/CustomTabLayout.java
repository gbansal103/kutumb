package bamfaltech.kutumb.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.util.AttributeSet;

import bamfaltech.kutumb.R;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class CustomTabLayout extends TabLayout {

    private int[] tabDrawables = {R.drawable.ic_home_black_24dp, R.drawable.ic_chat_black_24dp, R.drawable.ic_business_black_24dp};

    public CustomTabLayout(Context context) {
        super(context);
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void setTabsFromPagerAdapter(@Nullable PagerAdapter adapter) {
        this.removeAllTabs();
        int i = 0;
        for (int count = adapter.getCount(); i < count; ++i) {
            this.addTab(this.newTab().setCustomView(R.layout.custom_tab)
                    .setIcon(tabDrawables[i])
                    .setText(adapter.getPageTitle(i)));
        }
    }
}
