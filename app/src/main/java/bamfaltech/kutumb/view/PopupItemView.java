package bamfaltech.kutumb.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.managers.FirebaseManager;

import static android.R.attr.fragment;

/**
 * Created by gaurav.bansal1 on 24/02/17.
 */

public class PopupItemView extends BottomSheetDialog implements android.view.View.OnClickListener {

    private View mView;
    private KutumbApplication mAppState;
    private LayoutInflater mInflater;
    private Context mContext;
    private BaseKutumbFragment mFragment;

    private ListView mListView = null;
    private PopupMenuAdapter listAdapter = null;

    private int[] currentArrayOfOptions = null;

    private final int LOGOUT = 0;
    private final int CAMERA = 1;
    private final int GALLERY = 2;
    private final int LOCATION = 3;

    private int[] HomeMoreoptionsIndex = {LOGOUT};

    private int[] ChatMoreOptionsIndex = {CAMERA, GALLERY, LOCATION, LOGOUT};

    private String[] textArray = {
            "Logout",
            "Camera",
            "Gallery",
            "Location",
    };

    private String[] chatTextArray = {
            "Camera",
            "Gallery",
            "Location",
            "Logout"
    };

    private int[] menuId = {
            R.id.action_logout,
            R.id.action_camera,
            R.id.action_gallery,
            R.id.action_location
    };

    private int[] drawableAttrIds = {
            R.drawable.ic_exit_to_app_black_24dp,
            R.drawable.ic_photo_library_black_24dp,
            R.drawable.ic_photo_camera_black_24dp,
            R.drawable.ic_location_on_black_24dp
    };

    private Drawable[] drawables = new Drawable[4];


    public PopupItemView(Context context) {
        //		super(context, fragment);
        super(context);
        mAppState = KutumbApplication.getInstance();
        mInflater = LayoutInflater.from(context);
        mContext = context;
        //mFragment = fragment;
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        init(context);
    }

    private void init(Context context) {
        TypedArray typedArray = context.obtainStyledAttributes(drawableAttrIds);
//        typedArray.getIndexCount();
        for (int i = 0; i < typedArray.getIndexCount(); i++) {
            drawables[i] = typedArray.getDrawable(i);
        }
        typedArray.recycle();
    }

    public View populatePopupMenu() {
        if (mView == null) {
            mView = mInflater.inflate(R.layout.popup_context_menu, null);
        }

        setContentView(mView);

        mListView = (ListView) mView.findViewById(R.id.playerrQueueListView);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(mListView);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        listAdapter = new PopupMenuAdapter();
        currentArrayOfOptions = HomeMoreoptionsIndex;
        listAdapter.setCount(currentArrayOfOptions.length);
        mListView.setAdapter(listAdapter);
        return mView;
    }

    public View populateChatPopupMenu() {
        if (mView == null) {
            mView = mInflater.inflate(R.layout.popup_context_menu, null);
        }

        setContentView(mView);

        mListView = (ListView) mView.findViewById(R.id.playerrQueueListView);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(mListView);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        listAdapter = new PopupMenuAdapter();
        currentArrayOfOptions = ChatMoreOptionsIndex;
        listAdapter.setCount(currentArrayOfOptions.length);
        mListView.setAdapter(listAdapter);
        return mView;
    }

    private View populateMenu(int position, View convertView, ViewGroup parent) {

        convertView = mInflater.inflate(R.layout.popup_item, parent, false);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        TextView textView = (TextView) convertView.findViewById(R.id.textView);
        int menu_id = 0;
        try {
//            imageView.setImageResource(drawableId[currentArrayOfOptions[position]]);
            imageView.setImageDrawable(mContext.getResources().getDrawable(drawableAttrIds[currentArrayOfOptions[position]]));
            textView.setText(textArray[currentArrayOfOptions[position]]);
            menu_id = menuId[currentArrayOfOptions[position]];
            convertView.setTag(menu_id);
            if (position == currentArrayOfOptions.length - 1) {
                convertView.findViewById(R.id.horizontalLine).setVisibility(View.INVISIBLE);
            }
        } catch (Exception e) {
        }
        convertView.setOnClickListener(this);
        //   Util.setFonts(mContext, convertView, Util.FontFamily.PROXIMANOVA_REGULAR);
        return convertView;//showHideView(convertView, menu_id);
    }

    @Override
    public void onClick(View view) {
        final int itemId = (Integer) view.getTag();
        switch (itemId) {
            case R.id.action_logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setTitle("Logout user");
                builder.setMessage("Are you sure, do you want to exit ?");

                String positiveText = "yes";
                builder.setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // positive button logic
                                FirebaseManager.getInstance().signout();
                                Toast.makeText(mContext, "Logout user!", Toast.LENGTH_LONG).show();
                            }
                        });

                String negativeText = "no";
                builder.setNegativeButton(negativeText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // negative button logic
                            }
                        });
                AlertDialog dialog = builder.create();
                // display dialog
                dialog.show();
                break;
            case R.id.action_camera:
                if(mContext instanceof StartChatActivity){
                    ((StartChatActivity)mContext).onOptionItemSelected(R.id.sendPhoto);
                }
                break;
            case R.id.action_gallery:
                if(mContext instanceof StartChatActivity){
                    ((StartChatActivity)mContext).onOptionItemSelected(R.id.sendPhotoGallery);
                }
                break;
            case R.id.action_location:
                if(mContext instanceof StartChatActivity){
                    ((StartChatActivity)mContext).onOptionItemSelected(R.id.sendLocation);
                }
                break;
        }
        this.hide();
    }

    class PopupMenuAdapter extends BaseAdapter {
        private ArrayList<Object> mArrrListItems;
        private int count;

        public ArrayList<Object> getAdapterArrayList() {
            return mArrrListItems;
        }

        public void setAdapterArrayList(ArrayList<?> arrList) {
            this.mArrrListItems = (ArrayList<Object>) arrList;
            this.count = mArrrListItems.size();
        }

        public void removeItem(Object pObject) {
            this.mArrrListItems.remove(pObject);
            this.notifyDataSetChanged();
        }

        public void clearAdapter() {
            this.mArrrListItems.clear();
            this.notifyDataSetChanged();
        }

        public void updateAdapterArrayList(ArrayList<?> pNewArrayList) {
            if (pNewArrayList != null && pNewArrayList.size() != 0) {
                for (int i = 0; i < pNewArrayList.size(); i++) {
                    Object newObject = (Object) pNewArrayList.get(i);
                    this.mArrrListItems.add(newObject);
                }
            }
            this.notifyDataSetChanged();
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public Object getItem(int position) {
            Object object = this.mArrrListItems.get(position);
            return object;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return populateMenu(position, convertView, parent);
        }
    }
}
