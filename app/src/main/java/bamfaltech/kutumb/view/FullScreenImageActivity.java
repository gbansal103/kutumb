package bamfaltech.kutumb.view;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.adapter.CircleTransform;
import bamfaltech.kutumb.util.Util;

public class FullScreenImageActivity extends AppCompatActivity {

    private TouchImageView mImageView;
    //private ImageView ivUser;
    //private TextView tvUser;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        bindViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setValues();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.gc();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private void bindViews(){
        progressDialog = new ProgressDialog(this);
        mImageView = (TouchImageView) findViewById(R.id.imageView);
        Bundle bundle = getIntent().getExtras();

        setupToolbar(bundle.getString("nameUser"), bundle.getString("urlPhotoUser"));
       /* ivUser = (ImageView)toolbar.findViewById(R.id.avatar);
        tvUser = (TextView)toolbar.findViewById(R.id.title);*/
    }

    private void setupToolbar(String toolbarTitle, String frontUserArtwork) {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(this);
        mDetailsMaterialActionBar.setParams(null, toolbarTitle, frontUserArtwork);
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(toolbarTitle);
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private void setValues(){
        String nameUser,urlPhotoUser,urlPhotoClick;
        nameUser = getIntent().getStringExtra("nameUser");
        urlPhotoUser = getIntent().getStringExtra("urlPhotoUser");
        urlPhotoClick = getIntent().getStringExtra("urlPhotoClick");
        final String imageName = urlPhotoClick;
        Log.i("TAG","imagem recebida "+urlPhotoClick);
        //tvUser.setText(nameUser); // Name
        //Glide.with(this).load(urlPhotoUser).centerCrop().transform(new CircleTransform(this)).override(40,40).into(ivUser);

        Bitmap bitmap = Util.getImageIfExists(this, imageName);
        if(bitmap != null) {
            mImageView.setImageBitmap(bitmap);

        } else {

            Glide.with(this).load(urlPhotoClick).asBitmap().override(640, 640).fitCenter().into(new SimpleTarget<Bitmap>() {

                @Override
                public void onLoadStarted(Drawable placeholder) {
                    progressDialog.setMessage("Loading picture...");
                    progressDialog.show();
                }

                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    progressDialog.dismiss();
                    mImageView.setImageBitmap(resource);
                    Util.saveImageToGallery(FullScreenImageActivity.this, resource, imageName);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    Toast.makeText(FullScreenImageActivity.this, "Sorry, Some Error has been occured !", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            });
        }
    }


}
