package bamfaltech.kutumb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;

import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.RWAListModel;
import bamfaltech.kutumb.services.Serializer;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;

/**
 * Created by gaurav.bansal1 on 13/02/17.
 */

public class SplashScreenActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        KutumbApplication.sessionCount = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_APPLICATION_SESSION_COUNT, 0, false);
        DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_APPLICATION_SESSION_COUNT, ++KutumbApplication.sessionCount, false);
        final boolean isBoardingFinished = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_BOARD_SCREEN_COMPLETE, false, false);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!isBoardingFinished) {
                    Intent intent = new Intent(SplashScreenActivity.this, BoardScreenActivity.class);
                    startActivity(intent);
                    SplashScreenActivity.this.finish();
                } else {
                    Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(intent);
                    SplashScreenActivity.this.finish();
                }
            }
        }, 1000);
    }
}
