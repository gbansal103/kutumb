package bamfaltech.kutumb;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by gaurav.bansal1 on 14/09/16.
 */
public class ProfileActivity extends Activity implements View.OnClickListener {

    private Button joinChatRoom;
    private ImageView selectImage;
    private ImageView profilePic;
    private String TAG = "ProfileActivity";
    private static final int SELECT_PICTURE = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        joinChatRoom = (Button) findViewById(R.id.completeSetup);
        joinChatRoom.setOnClickListener(this);
        selectImage = (ImageView)findViewById(R.id.select_img);
        profilePic = (ImageView)findViewById(R.id.profile_pic);
        selectImage.setOnClickListener(this);
    }

    /* Choose an image from Gallery */
    void openImageChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            //if (requestCode == SELECT_PICTURE) {
            // Get the url from data
            Uri selectedImageUri = data.getData();
            if (null != selectedImageUri) {
                // Get the path from the Uri
                String path = getPathFromURI(selectedImageUri);
                Log.i(TAG, "Image Path : " + path);
                // Set the image in ImageView
                selectImage.setVisibility(View.GONE);
                profilePic.setVisibility(View.VISIBLE);
                profilePic.setImageURI(selectedImageUri);

            }
            //}
        }
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    @Override
    public void onClick(View v) {
        if(v == selectImage) {
            openImageChooser();
        } else if(v == joinChatRoom) {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
            intent.setClass(this, KutumbActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
