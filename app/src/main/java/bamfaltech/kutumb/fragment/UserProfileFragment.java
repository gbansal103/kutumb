package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import bamfaltech.kutumb.BasicInfoActivity;
import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.viewholder.PostViewHolder;
import bamfaltech.kutumb.viewholder.UserProfileHeaderHolder;

/**
 * Created by gaurav.bansal1 on 02/03/17.
 */

public class UserProfileFragment extends BaseKutumbFragment implements View.OnClickListener {

    private static final String TAG = "UserProfileFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private View headerView;

    private Activity activity;
    UserModel userModel;


    public UserProfileFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.user_profile_fragment, container, false);
        activity = getActivity();
        mContext = getActivity();

         userModel = (UserModel) getArguments().getSerializable("userModel");

        setupActionBar(rootView);

        headerView = inflater.inflate(R.layout.user_profile_header, container, false);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }


    private void setupActionBar(View rootView) {
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle(){
        return "Profile";
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        Query postsQuery = getQuery(mDatabase);
        mAdapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(Post.class, R.layout.item_post,
                PostViewHolder.class, postsQuery) {

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                if(viewType == Constants.USER_PROFILE_HEADER) {
                    return new UserProfileHeaderHolder(headerView);
                } else {
                    return super.onCreateViewHolder(parent, viewType);
                }
            }

            @Override
            public int getItemCount() {
                int itemCount = super.getItemCount();
                itemCount = itemCount + 1;
                return itemCount;
            }

            @Override
            public Post getItem(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItem(position);
                } else {
                    Post post = new Post();
                    post.setPhoto_profile(FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());
                    post.setAuthor("What you want to share ?");
                    return post;
                }
            }

            @Override
            public DatabaseReference getRef(int position) {
                if(position < getItemCount() - 1) {
                    return super.getRef(position);
                } else {
                    return null;
                }
            }

            @Override
            public int getItemViewType(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemViewType(position);
                } else {
                    return Constants.USER_PROFILE_HEADER;
                }
            }

            @Override
            public long getItemId(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemId(position);
                } else {
                    return -1;
                }
            }


            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final Post model, final int position) {
                /*if(getItemCount() == 1) {
                    mNoResultsText.setVisibility(View.VISIBLE);
                } else {
                    mNoResultsText.setVisibility(View.GONE);
                }*/
                mProgressBar.setVisibility(View.GONE);
                if (viewHolder instanceof UserProfileHeaderHolder) {
                    ((UserProfileHeaderHolder)viewHolder).bindToUserProfileHeader(userModel, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(activity, BasicInfoActivity.class);
                            startActivity(intent);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            NewPostFragment fragment = new NewPostFragment();
                            ((KutumbActivity)activity).displayFragment(fragment);
                        }
                    });
                } else {
                    final DatabaseReference postRef = getRef(position);

                    // Set click listener for the whole post view
                    final String postKey = postRef.getKey();
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Launch PostDetailFragment
                            PostDetailFragment postDetailFragment = new PostDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                            postDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(postDetailFragment);
                        }
                    });

                    // Determine if the current user has liked this post and set UI accordingly
                    if (model.stars.containsKey(getUid())) {
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                        viewHolder.mLikeImage.setImageResource(R.drawable.heart_red);
                    } else {
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.black));
                        viewHolder.mLikeImage.setImageResource(R.drawable.ic_favorite_black_24dp);
                    }

                    // Bind Post to ViewHolder, setting OnClickListener for the star button
                    viewHolder.bindToPost(postRef, model, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FirebaseManager.getInstance().getFrontUserInfoFromFirebase(model.uid, new Interfaces.OnUserModelRetrieved() {
                                @Override
                                public void onRetreivalComplete(UserModel userModel) {
                                    UserProfileFragment userProfileFragment = new UserProfileFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("userModel", userModel);
                                    userProfileFragment.setArguments(bundle);
                                    ((KutumbActivity)activity).displayFragment(userProfileFragment);
                                }

                                @Override
                                public void onError(String errormsg) {

                                }
                            });

                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {
                            // Need to write to both places the post is stored
                            DatabaseReference globalPostRef = mDatabase.child("posts").child(postRef.getKey());
                            DatabaseReference userPostRef = mDatabase.child("user-posts").child(model.uid).child(postRef.getKey());

                            // Run two transactions
                            onStarClicked(globalPostRef);
                            onStarClicked(userPostRef);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            PostDetailFragment postDetailFragment = new PostDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                            postDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(postDetailFragment);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TopicFragment topicFragment = new TopicFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("topic", model.title);
                            topicFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(topicFragment);
                        }
                    }, UserProfileFragment.this);


                }
            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    // [START post_stars_transaction]
    private void onStarClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                }

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }
    // [END post_stars_transaction]


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        return userModel.getId();
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query query = mDatabase.child("user-posts").child(getUid());
        return query;
    }

}
