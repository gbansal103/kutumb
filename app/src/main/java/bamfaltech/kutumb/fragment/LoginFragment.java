package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.FirebaseAuth;

import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.MainActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.managers.AccountKitLoginManager;
import bamfaltech.kutumb.managers.FacebookLoginManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.GoogleLoginManager;
import bamfaltech.kutumb.util.Constants;

/**
 * Created by gaurav.bansal1 on 14/03/17.
 */

public class LoginFragment extends BaseKutumbFragment {

    Button phoneLoginButton;
    LoginButton loginButton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CallbackManager mCallbackManager;
    private String TAG = "FacebookLogin";

    private AccountKitLoginManager mAccountKitLoginManager = null;
    private FacebookLoginManager mFacebookLoginManager = null;
    private GoogleLoginManager mGoogleLoginManager = null;
    private FirebaseManager mFirebaseManager = null;

    private KutumbApplication mApp;
    private TextView termsTextView;

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.login_fragment_layout, container, false);

        mContext = getActivity();

        phoneLoginButton = (Button) mView.findViewById(R.id.phone_login);
        if(mAccountKitLoginManager == null) {
            mAccountKitLoginManager = AccountKitLoginManager.getInstance();
        }
        mAccountKitLoginManager.setParams(getActivity());
        phoneLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAccountKitLoginManager.phoneLogin(phoneLoginButton);
            }
        });

        loginButton = (LoginButton) mView.findViewById(R.id.btnSignInFacebook);
        if(mFacebookLoginManager == null) {
            mFacebookLoginManager = FacebookLoginManager.getInstance();
        }
        mFacebookLoginManager.initialize(getActivity());
        mFacebookLoginManager.loginWithFacebook(loginButton);

        SignInButton signInButton = (SignInButton) mView.findViewById(R.id.btnSignInGoogle);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        if(mGoogleLoginManager == null) {
            mGoogleLoginManager = GoogleLoginManager.getInstance();
        }
        mGoogleLoginManager.initialize(getActivity());
        mGoogleLoginManager.setupGoogleSignin(signInButton);

        termsTextView = (TextView) mView.findViewById(R.id.terms);

        setTermsText();


        return mView;
    }

    private void setTermsText(){
        String string1 = "By Signingup, I agree to App's ";
        String string2 = "Terms of Service";
        String string3 = " and ";
        String string4 = "Private Policy.";

        String finalString = string1 + string2 + string3 + string4;

        SpannableString spannableString = new SpannableString(finalString);

        ClickableSpan termsClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
                ((MainActivity)mContext).displayFragment(termsAndConditionsFragment);
            }
            /*@Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }*/
        };

        ClickableSpan privacyClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                PrivacyPolicyFragment privacyPolicyFragment = new PrivacyPolicyFragment();
                ((MainActivity)mContext).displayFragment(privacyPolicyFragment);
            }
           /* @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }*/
        };

        spannableString.setSpan(termsClickableSpan, finalString.indexOf(string2), finalString.indexOf(string2) + string2.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(privacyClickableSpan, finalString.indexOf(string4), finalString.indexOf(string4) + string4.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsTextView.setText(spannableString);
        termsTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Constants.GOOGLE_REQUEST_SIGNIN:
                mGoogleLoginManager.onActivityResult(requestCode, resultCode, data);
                break;
            case Constants.ACCOUNTKIT_REQUEST_LOGIN:
                AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
                String toastMessage;
                if (loginResult.getError() != null) {
                    toastMessage = loginResult.getError().getErrorType().getMessage();
                    //showErrorActivity(loginResult.getError());
                } else if (loginResult.wasCancelled()) {
                    toastMessage = "Login Cancelled";
                } else {
                    if (loginResult.getAccessToken() != null) {
                        toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                    } else {
                        toastMessage = String.format(
                                "Success:%s...",
                                loginResult.getAuthorizationCode().substring(0,10));
                    }

                    // If you have an authorization code, retrieve it from
                    // loginResult.getAuthorizationCode()
                    // and pass it to your server and exchange it for an access token.

                    // Success! Start your next activity...
                    //goToMyLoggedInActivity();
                }
                //Toast.makeText(getActivity(), toastMessage, Toast.LENGTH_SHORT).show();
                mAccountKitLoginManager.signIn();
                break;
            default:
                mFacebookLoginManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
