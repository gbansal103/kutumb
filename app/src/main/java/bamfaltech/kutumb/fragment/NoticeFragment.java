package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.NoticeModel;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.view.FullScreenImageActivity;
import bamfaltech.kutumb.viewholder.NoticeModelViewHolder;

/**
 * Created by gaurav.bansal1 on 28/02/17.
 */

public class NoticeFragment extends BaseKutumbFragment implements View.OnClickListener{

    private static final String TAG = "NoticeFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<NoticeModel, NoticeModelViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private Activity activity;


    private View headerView;

    public NoticeFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.notice_fragment, container, false);
        mContext = getActivity();
        activity = getActivity();
        setupActionBar(rootView);


        ImageView currenrUserPic = (ImageView) rootView.findViewById(R.id.current_user_pic);
        Util.bindCircularImage(currenrUserPic, FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());

        rootView.findViewById(R.id.top_layout).setOnClickListener(this);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }

    private void setupActionBar(View rootView) {
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    private String getPageTitle(){
        return "Circular";
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        Query postsQuery = getQuery(mDatabase);
        mAdapter = new FirebaseRecyclerAdapter<NoticeModel, NoticeModelViewHolder>(NoticeModel.class, R.layout.item_notice,
                NoticeModelViewHolder.class, postsQuery) {

            @Override
            public int getItemCount() {
                mProgressBar.setVisibility(View.GONE);
                return super.getItemCount();
            }


            @Override
            protected void populateViewHolder(NoticeModelViewHolder viewHolder, final NoticeModel model, int position) {
                mProgressBar.setVisibility(View.GONE);
                final DatabaseReference postRef = getRef(position);

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToNotice(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        Intent fullScreenImageActivity = new Intent(activity, FullScreenImageActivity.class);
                        if(model.getFileModel() != null) {
                            fullScreenImageActivity.putExtra("urlPhotoClick", model.getFileModel().getUrl_file());
                            activity.startActivity(fullScreenImageActivity);
                        } else {
                            return;
                        }
                    }
                }, NoticeFragment.this);

            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query recentPostsQuery = databaseReference.child("notice")
                .limitToFirst(100);
        // [END recent_posts_query]

        return recentPostsQuery;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.top_layout:
                NewPostFragment fragment = new NewPostFragment();
                ((KutumbActivity)activity).displayFragment(fragment);
                break;
        }
    }
}
