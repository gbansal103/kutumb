package bamfaltech.kutumb.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.actionbar.GenericActionBar;
import bamfaltech.kutumb.R;


/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class BaseKutumbFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public Toolbar getmToolbar() {
        return mToolbar;
    }

    public void setmToolbar(Toolbar mToolbar) {
        this.mToolbar = mToolbar;
    }

    private Toolbar mToolbar;
    public Context mContext;

    public void setActionBar(View parent, View actionBar, boolean attachMenuItem) {


        mToolbar = (Toolbar) parent.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        if (actionBar.getParent() != null) {
            ((ViewGroup) actionBar.getParent()).removeAllViews();
        }
        mToolbar.addView(actionBar);
        mToolbar.setVisibility(View.VISIBLE);
        mToolbar.getMenu().clear();
        if (attachMenuItem) {

            if (actionBar instanceof GenericActionBar) {

                mToolbar.inflateMenu(R.menu.cast_menu_home);
                MenuItem item = mToolbar.getMenu().findItem(R.id.notif_actionbar);
                MenuItemCompat.setActionView(item, R.layout.view_notification_bubble);
                ((GenericActionBar) actionBar).setToolbar(mToolbar);

            }
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public String getFragmentTitle() {
        return null;
    }

    public void setCurrentFragment(Fragment fragment) {
        ((KutumbActivity)mContext).setCurrentFragment(fragment);
    }
}
