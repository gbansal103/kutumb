package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.models.TopicModels;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 27/12/16.
 */

public class NewUtilityPostFragment extends BaseKutumbFragment {


    private static final String TAG = "NewUtilityPostFragment";
    private static final String REQUIRED = "Required";

    // [START declare_database_ref]
    private DatabaseReference mDatabase;
    // [END declare_database_ref]

    //private EditText mTitleField;
    private EditText mBodyField;
    private FloatingActionButton mSubmitButton;
    private Spinner catagorySpinner;

    private AutoCompleteTextView catagoryAuto;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.activity_new_utility, container, false);
        mContext = getActivity();
        activity = getActivity();
        init(mView);

       /* catagorySpinner = (Spinner) mView.findViewById(R.id.catagory);
        ArrayAdapter<String> towerListAdapter = new ArrayAdapter<String>(activity, R.layout.spinner_list_item, Constants.catagoryList);
        towerListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catagorySpinner.setAdapter(towerListAdapter);*/

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        //mTitleField = (EditText) findViewById(R.id.field_title);
        mBodyField = (EditText) mView.findViewById(R.id.field_body);
        mSubmitButton = (FloatingActionButton) mView.findViewById(R.id.fab_submit_post);

        catagoryAuto = (AutoCompleteTextView) mView.findViewById(R.id.catagory_auto);



        getCatagoryList();
        catagoryAuto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    catagoryAuto.showDropDown();
                }
            }
        });

       /* mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });*/

        return mView;
    }

    boolean isAutoCompleteCatagory = false;

    public void getCatagoryList() {
        DatabaseReference ref = mDatabase.child("catagory");
        FirebaseManager.getInstance().getTopicList(ref, new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                if(object != null && object instanceof List) {
                    List<String> catagoryStringList = (List)object;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                            android.R.layout.simple_dropdown_item_1line, Util.convertListToStringArray(catagoryStringList));
                    catagoryAuto.setThreshold(0);
                    catagoryAuto.setAdapter(adapter);
                    catagoryAuto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            isAutoCompleteCatagory = true;
                        }
                    });
                    catagoryAuto.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            isAutoCompleteCatagory = false;
                        }
                    });

                }
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query query = databaseReference.child("catagory")
                .limitToFirst(100);
        return query;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Utility";
    }

    public void submitPost() {
        final String title = catagoryAuto.getText().toString();
        final String body = mBodyField.getText().toString();

        // Title is required
        if(TextUtils.isEmpty(title)) {
            Toast.makeText(mContext, "Utility Subject can not be blank", Toast.LENGTH_LONG).show();
            return;
        }


        // Body is required
        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(REQUIRED);
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        Toast.makeText(activity, "Posting...", Toast.LENGTH_SHORT).show();

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        // [START_EXCLUDE]
                        if (user == null) {
                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(activity,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewPost(userId, user, title, body);
                            if(!isAutoCompleteCatagory) {
                                writeNewCatagory(userId, title);
                            }
                        }

                        // Finish this Activity, back to the stream
                        ((KutumbActivity)activity).removeFragment();
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // [START_EXCLUDE]
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }
                });
        // [END single_value_read]
    }

    @Override
    public String getFragmentTitle() {
        return "newutility";
    }

    private void setEditingEnabled(boolean enabled) {
        //mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    // [START write_fan_out]
    private void writeNewPost(String userId, UserModel user, String title, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("utility").push().getKey();
        Post post = new Post(userId, user.getName(), user.getFlatNumber(), title, body);
        post.setPhoto_profile(user.getPhoto_profile());
        RWADetailModel.RWA rwa = user.getRwa();
        if(rwa != null) {
            post.setRwaName(rwa.getRwaname());
        }

        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/utility/" + key, postValues);
        childUpdates.put("/user-utility/" + userId + "/" + key, postValues);
        RWADetailModel.RWA userRWA = user.getRwa();
        if(userRWA != null) {
            String societyKey = userRWA.getRwaid();
            childUpdates.put("/society-utility/"+societyKey+"/"+key, postValues);
        }

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewCatagory(String userId, String postTitle) {
        String key = mDatabase.child("catagory").push().getKey();
        TopicModel topicModel = new TopicModel(userId, postTitle);
        Map<String, Object> topicValues = topicModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/catagory/" + key, topicValues);
        //childUpdates.put("/user-topics/" + userId + "/" + key, topicValues);

        mDatabase.updateChildren(childUpdates);
    }
}
