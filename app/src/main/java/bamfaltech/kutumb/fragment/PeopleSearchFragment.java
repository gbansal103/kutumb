package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.viewholder.FlatMemberHolder;

/*import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;*/

/**
 * Created by gaurav.bansal1 on 21/12/16.
 */

public class PeopleSearchFragment extends BaseKutumbFragment implements View.OnClickListener, NumberPicker.OnValueChangeListener{

    private FirebaseManager mFirebaseManager;
    NumberPicker npFlat;
    NumberPicker npFloor;
    NumberPicker npTower;
    private FirebaseRecyclerAdapter<UserModel, FlatMemberHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private ProgressBar mProgressBar;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.people_search_fragment, container, false);
        activity = getActivity();
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mFirebaseManager = FirebaseManager.getInstance();
        mContext = getActivity();
        initUI(view);
        return view;
    }

    @Override
    public String getFragmentTitle() {
        return "flatchooser";
    }

    private String getPageTitle() {
        return "Flat Members";
    }

    private void initUI(View container){
        mRecycler = (RecyclerView) container.findViewById(R.id.flat_member_list);
        mRecycler.setHasFixedSize(true);


        TextView setButton = (TextView) container.findViewById(R.id.set_button);
        //Button cancelButton = (Button) container.findViewById(R.id.cancel_button);

        npTower = (NumberPicker) container.findViewById(R.id.numberPickerTower);

        npFloor = (NumberPicker) container.findViewById(R.id.numberPickerFloor);


        npFlat = (NumberPicker) container.findViewById(R.id.numberPickerFlat);

        getTowerList();


        setButton.setOnClickListener(this);
        //cancelButton.setOnClickListener(this);
    }

    List<String> flatNumber;
    List<String> towerNumbers;

    private void getTowerList() {
        final RWADetailModel.RWA rwa = KutumbApplication.getInstance().getCurrentRWA();
        String towerList = rwa.getRwaTower();
        String towers[] = towerList.split(",");
        towerNumbers = new ArrayList<String>(Arrays.asList(towers));

        npTower.setMaxValue(towers.length - 1);
        npTower.setMinValue(0);
        npTower.setDisplayedValues(towers);
        npTower.setWrapSelectorWheel(false);
        npTower.setOnValueChangedListener(PeopleSearchFragment.this);


        int towerFloor = Integer.parseInt(rwa.getRwaFloor());
        final int towerFlat = Integer.parseInt(rwa.getRwaFlat());

        final List<String> floorNumber = new ArrayList<String>();
        for(int i=0; i<=towerFloor; i++) {
            if(i ==0) {
                floorNumber.add("G");
            } else if( i == 13) {
                if(rwa.getThirteenFloorBehaviour().equalsIgnoreCase("absent")) {
                    //add nothing
                } else if(rwa.getThirteenFloorBehaviour().equalsIgnoreCase("present_with_A")) {
                    floorNumber.add("12A");
                }
            } else {
                floorNumber.add((i) + "");
            }
        }

        npFloor.setMaxValue(floorNumber.size() - 1);
        npFloor.setMinValue(0);

        String[] floors = new String[floorNumber.size()];
        floors = floorNumber.toArray(floors);

        npFloor.setDisplayedValues(floors);
        npFloor.setWrapSelectorWheel(false);
                /*npFloor.setOnScrollListener(new NumberPicker.OnScrollListener() {
                    @Override
                    public void onScrollStateChange(NumberPicker numberPicker, int i) {
                        int floor = i -1;//Integer.parseInt(floorNumber.get(i));
                        flatNumber = new ArrayList<String>();
                        for (int j = 1; j <= towerFlat; j++) {
                            if(floor == 0) {
                                flatNumber.add("00"+j);
                            } else {
                                flatNumber.add((floor * 100 + j) + "");
                            }
                        }
                        npFlat.setMaxValue(flatNumber.size() - 1);
                        npFlat.setMinValue(0);
                        npFlat.setDisplayedValues((String[]) flatNumber.toArray());
                        npFlat.setWrapSelectorWheel(false);
                    }
                });*/
        npFloor.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldValue, int newValue) {
                int floor = newValue;//Integer.parseInt(floorNumber.get(i));
                flatNumber = new ArrayList<String>();
                for (int j = 1; j <= towerFlat; j++) {
                    if(floor < 0) {
                        return;
                    }
                    if(floor == 0) {
                        flatNumber.add("00"+j);
                    } else if(floor >= 13) {
                        if(rwa.getThirteenFloorBehaviour().equalsIgnoreCase("absent")) {
                            //add nothing
                            flatNumber.add(((floor+1) * 100 + Integer.parseInt(rwa.getRwaCountInitial()) + j) + "");
                        } else if(rwa.getThirteenFloorBehaviour().equalsIgnoreCase("present_with_A")) {
                            if(floor == 13) {
                                flatNumber.add(((floor-1) * 100 + Integer.parseInt(rwa.getRwaCountInitial()) + j) + "A");
                            } else {
                                flatNumber.add((floor * 100 + Integer.parseInt(rwa.getRwaCountInitial()) + j) + "");
                            }

                        }
                    } else {
                        flatNumber.add((floor * 100 + j) + "");
                    }
                }
                npFlat.setMaxValue(flatNumber.size() - 1);
                npFlat.setMinValue(0);
                String[] flats = new String[flatNumber.size()];
                flats = flatNumber.toArray(flats);
                npFlat.setDisplayedValues(flats);
                npFlat.setWrapSelectorWheel(false);
                npFlat.setOnValueChangedListener(PeopleSearchFragment.this);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.set_button:
                mProgressBar.setVisibility(View.VISIBLE);
                Query flatMemQuery = mFirebaseManager.getFlatMemebers(towerNumbers.get(npTower.getValue()) + flatNumber.get(npFlat.getValue()));
                populateFlatMembers(flatMemQuery);
                break;

        }
    }


    public void populateFlatMembers(Query flatMemberQuery) {

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);
        // Set up FirebaseRecyclerAdapter with the Query
        mAdapter = new FirebaseRecyclerAdapter<UserModel, FlatMemberHolder>(UserModel.class, R.layout.flat_member_item,
                FlatMemberHolder.class, flatMemberQuery) {
            @Override
            protected void populateViewHolder(FlatMemberHolder viewHolder, final UserModel model, int position) {
                final DatabaseReference postRef = getRef(position);
                mProgressBar.setVisibility(View.GONE);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                final String userId = model.getId();

                viewHolder.bindToFlat(postKey, model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        if(!postKey.equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
                            // Launch PostDetailFragment
                            Intent intent = new Intent(getActivity(), StartChatActivity.class);
                            intent.putExtra("front_user_id", postKey);
                            intent.putExtra("front_user_name", model.getName());
                            intent.putExtra("front_user_reg_id", model.getFCMRegID());
                            //intent.putExtra(PostDetailFragment.EXTRA_POST_KEY, postKey);
                            startActivity(intent);
                        } else {
                            Toast.makeText(activity, "It will be funny if you will chat with yourself.", Toast.LENGTH_LONG).show();
                        }
                    }
                }, PeopleSearchFragment.this);

            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

    }
}
