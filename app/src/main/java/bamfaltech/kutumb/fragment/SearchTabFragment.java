package bamfaltech.kutumb.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.actionbar.GenericActionBar;

/**
 * Created by gaurav.bansal1 on 01/03/17.
 */

public class SearchTabFragment extends BaseKutumbFragment {
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private FragmentStatePagerAdapter mAdapter;
    private String[] TAB_NAME = {"People", "Topics"};
    public static final String SHOW_TAB_POSITION = "tab_position";
    public static final int SHOW_TAB_PEOPLE = 0;
    public static final int SHOW_TAB_TOPICS = 1;

    //public static final int SHOW_TAB_MYMUSIC = 3;
    private static final int TOTAL_NUM_OF_TABS = 2; // total number of pages to show
    private Bundle mBundle; // need it for supporting deeplinking
    private int selectedPosition = 0; // -1 denotes fragment was not created, positive value will specify selected tab position

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View tabContainerView = inflater.inflate(R.layout.tab_container_fragment_layout, container, false);
        Toolbar toolbar = (Toolbar) tabContainerView.findViewById(R.id.main_toolbar);
        mBundle = getArguments();
        if (mBundle != null && selectedPosition == -1) {
            selectedPosition = mBundle.getInt(SHOW_TAB_POSITION, SHOW_TAB_PEOPLE);
        }

        mContext = getActivity();
        init(tabContainerView);

        return tabContainerView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    private void init(View view){
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        setupActionBar(view);

        mAdapter = new TabPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(selectedPosition).select();

        setupTabIcons(mTabLayout);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //((KutumbActivity)mContext).setCurrentPosition(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                selectedPosition = tab.getPosition();
                mTabLayout.getTabAt(selectedPosition).select();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setupActionBar(View rootView) {
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Search";
    }

    @Override
    public String getFragmentTitle() {
        return "SearchTabContainer";
    }

    /**
     * PagerAdapter which provides pages to the viewpager
     */
    class TabPagerAdapter extends FragmentStatePagerAdapter {

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case SHOW_TAB_PEOPLE:
                    fragment = new PeopleSearchFragment();
                    break;
                case SHOW_TAB_TOPICS:
                    fragment = new TopicListFragment();
                    break;

                /*case SHOW_TAB_MYMUSIC:
                    fragment = new MyMusicFragment();
                    break;*/

            }
            if (mBundle != null) {
                fragment.setArguments(mBundle);
            } else {
                fragment.setArguments(new Bundle());
            }
            //mListOfFragment[position] = (BaseGaanaFragment) fragment;
            //((KutumbActivity)mContext).setCurrentFragment(fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return TOTAL_NUM_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TAB_NAME[position];
        }


        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
            try {
                super.restoreState(state, loader);
            }catch (Exception e){
            }
        }
    }

    private void setupTabIcons(TabLayout tabLayout) {


        TextView tabOne = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabOne.setText(TAB_NAME[0]);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_people_black_24dp, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabTwo.setText(TAB_NAME[1]);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_subtitles_black_24dp, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
    }
}
