package bamfaltech.kutumb.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;

/**
 * Created by gaurav.bansal1 on 14/03/17.
 */

public class TermsAndConditionsFragment extends BaseKutumbFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.about_us, container, false);
        init(mView);
        TextView textView = (TextView) mView.findViewById(R.id.about_us_text);
        textView.setText(/*"Terms of Service\n" +
                "\n" +*/
                "Kutumb is a Social Networking site or application (hereinafter referred to as the “Website”, \"Site\" or “App”). The registered users or any users using the Site are agreed to be bound by these Terms of Service.\n" +
                "\n" +
                "1. Your relationship with Kutumb\n" +
                "\n" +
                "1.1 Your use of Kutumb's services and the Site/App (hereinafter individually referred to as the \"Service\" and collectively referred to as the \"Services\" in this document; and excluding any services provided to you by Kutumb under a separate written agreement) is subject to the terms of this legal agreement between you and Kutumb. \"Kutumb\" is Bamfaltech Pvt. Ltd., whose registered Office is situated at Sector‐120, Noida, UP – 201301. Kutumb enables its registered users to create a unique profile (which is made available on the Web and/or mobile devices) and provides users with access to a collection of services and resources, including various communication tools, content, applications, and programming offered from time to time through its network of properties. Please note that some information of your profile information provided by you may be publicly available. However, other profile information of your will be available based on your profile settings.\n" +
                "\n" +
                "1.2 Unless otherwise agreed in writing with Kutumb, your agreement with Kutumb will always include, at a minimum, the terms and conditions set out in this document. These are referred to below as the \"Terms\".\n" +
                "\n" +
                "1.3 The Terms, form a legally binding agreement between you and Kutumb in relation to your use of the Services. It is important that you take the time to read them carefully.\n" +
                "\n" +
                "2. Accepting the Terms\n" +
                "\n" +
                "2.1 By using the Services, you agree that you have read and understood the Terms and you agree to be bound by the Terms and use the Services in compliance with the Terms. PLEASE READ THE TERMS CAREFULLY. IF YOU DO NOT AGREE TO BE BOUND BY (OR CANNOT COMPLY WITH) ANY OF THE TERMS BELOW, DO NOT COMPLETE THE REGISTRATION PROCESS, AND DO NOT ATTEMPT TO USE THE SITE/APP. You expressly represent and warrant that you will not use the Service if you do not understand, agree to become a party to, and abide by all of the Terms specified below. Violation of these Terms can result in legal liability to you. Nothing in the Terms should be construed to confer any rights to third party beneficiaries.\n" +
                "\n" +
                "For purposes of these Terms, \"we\", \"us\", \"our\", \"Kutumb\" refers to Kutumb App Private Limited and its directors, officers, agents, employees and representatives (collectively, \"Kutumb\"), and \"you\", \"your\" and \"yours\" refers to you, the customer using the Service who indicates his/her acceptance/rejection of the Terms below.\n" +
                "\n" +
                "2.2 You can accept the Terms by:\n" +
                "\n" +
                "a. completing the sign up process made available to you by Kutumb in the user interface of any Service; or\n" +
                "\n" +
                "b. by actually using the Services. In this case, you understand and agree that Kutumb will treat your use of the Services as acceptance of the Terms from that point onwards.\n" +
                "\n" +
                "2.3 You may not use the Services and may not accept the Terms if (a) you are incapable, by reason of age or mental capacity, to form a binding contract with Kutumb,  (b) you are not at least 13 years old, (c) you are a person barred from receiving the Services under the laws of India or other countries including the country in which you are resident or from which you use the Services.\n" +
                "\n" +
                "2.4 Before you continue, you should print off or save a local copy of the Terms for your records.\n" +
                "\n" +
                "3. Account Registration: Use of the Services by you\n" +
                "\n" +
                "3.1 In order to access certain Services, you may be required to provide information about yourself (such as identification, personal details or contact details) as part of the registration process for the Service, or as part of your continued use of the Services. You agree that any registration information you give to Kutumb will always be true, accurate, correct, complete and up to date.\n" +
                "\n" +
                "3.2 You agree to use the Services only for purposes that are permitted by (a) the Terms and (b) any applicable law, regulation or generally accepted practices or guidelines in India.\n" +
                "\n" +
                "3.3 You agree not to access (or attempt to access) any of the Services by any means other than through the interface that is provided by Kutumb, unless you have been specifically allowed to do so in a separate agreement with Kutumb.\n" +
                "\n" +
                "3.4 You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services).\n" +
                "\n" +
                "3.5 Unless you have been specifically permitted to do so in a separate agreement with Kutumb, you agree that you will not reproduce, duplicate, copy, sell, trade any information made avail to you as registered user or user for any purpose.\n" +
                "\n" +
                "3.6 You agree that you are solely responsible for (and that Kutumb has no responsibility to you or to any third party for) any breach of your obligations under the Terms and for the consequences (including any loss or damage which Kutumb may suffer) of any such breach.\n" +
                "\n" +
                "3.7 You agree to not promote or market other 3rd party similar or other services on Kutumb's platform. Violation of this clause could result in termination of account.\n" +
                "\n" +
                "4. Your passwords and account security\n" +
                "\n" +
                "4.1 You agree and understand that you are responsible for maintaining the confidentiality of passwords or OTPs (One Time Passwords) associated with your account you use to access the Services.\n" +
                "\n" +
                "4.2 Accordingly, you agree that you will be solely responsible to Kutumb for all activities that occur under your account.\n" +
                "\n" +
                "4.3 If you become aware of any unauthorized use of your password or of your account, you agree to notify Kutumb immediately at (contact@letsKutumb.com).\n" +
                "\n" +
                "5. Access to the Service / Modifications to the Service\n" +
                "\n" +
                "5.1 While Kutumb makes all attempts and ensures that the Services are routed most efficiently and effectively, However, it has no control over inherent network congestion and delays.\n" +
                "\n" +
                "5.2 We do not provide you with the equipment to access the Services. You are responsible for all fees charged by third parties to access the Services (for example charges to send SMS messages, or data charges by wireless carriers etc.)\n" +
                "\n" +
                "5.3 We reserve the right to modify or discontinue, temporarily or permanently, all or a part of the Services without notice. We will not be liable to you or to any third party for any modification, suspension, or discontinuance of the Services, or parts thereof, except that you are only entitled to a prorated refund representing the unused (as of the date of termination) portion of any subscription fees that you have paid in advance if we permanently discontinue the Service and you have already paid the subscription fees.\n" +
                "\n" +
                "5.4 Kutumb reserve the right to communicate directly with users through any of the community tools provided, as well as to use automation to do some or all parts of their jobs, including the removal of photos, pornography, or hate communications, as well as to communicate with users through the Site, mobile phone applications, or text messaging, with the objective of compliance with the Terms and the applicable law for the time being in force. You agree that your public profile information, including but not limited to your display name, photograph, interests may be utilized by Kutumb to encourage other users to communicate with you or to avail the Service.\n" +
                "\n" +
                "5.5 You agree that Kutumb can, at its sole discretion, add tag line footer messages, help messages, branding messages, and internal advertisements (whether internal, contextual or any other advertisement) at the end of all Kutumb web pages or app screens, including your profile page.\n" +
                "\n" +
                "6 Restrictions\n" +
                "\n" +
                "6.1 You agree to use the Services only for purposes that are permitted by (a) the Terms and (b) any applicable law, regulation or generally accepted practices or guidelines in India.\n" +
                "\n" +
                "6.2 You agree to not host, display, upload, modify, publish, transmit, update or share any information that:\n" +
                "\n" +
                "belongs to another person and to which the user does not have any right to;\n" +
                "is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic, libelous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;\n" +
                "harm minors in any way;\n" +
                "infringes any patent, trademark, copyright or other proprietary rights;\n" +
                "violates any law for the time being in force;\n" +
                "deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;\n" +
                "impersonate another person;\n" +
                "contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;\n" +
                "threatens the unity, integrity, defense, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation.\n" +
                "6.3 Except as may be expressly permitted by applicable laws or authorized by us in writing, you will not (and you will not permit/allow/abet anyone else to):\n" +
                "\n" +
                "Store, copy, modify, distribute, publish or resell any of information; audio, visual, and audiovisual works; or other content made available on the Site (\"Website Content\") or on the App (“App Content”) or compile or collect any Website Content or App Content as a part of database or other work;\n" +
                "Use any automated tool (e.g., robots, spiders) to avail the Service or store, copy, modify, distribute, or resell any Website Content;\n" +
                "Rent, lease, or sub‐license your access to the Services to another person;\n" +
                "Avail the Services or use the Website or App Content for any purpose except for your own personal use;\n" +
                "Circumvent or disable any digital rights management, usage rules, or other security features of the Service;\n" +
                "Use the Service in a manner that threatens the integrity, performance, or availability of the Services; or\n" +
                "Remove, alter, or obscure any proprietary notices (including copyright notices) on any portion of the Services or Website Content.\n" +
                "6.4 You specifically agree that your account will be permanently blocked (i.e. it may not be unblocked in future under any circumstances without assigning any reason thereof) in the event of committing or attempt to commit any or all of the following type of activities:\n" +
                "\n" +
                "Morphing content which is blocked by our spam filters. For example, posting a mobile number in the message by adding alphabets, words or special characters in the same, so as to pass through our spam filters.\n" +
                "Posting content which is prohibited under the Term. For example, posts related to terrorism, obscenity, vulgarity, defamation, hate or religious disharmony etc.\n" +
                "Posting advertisements or commercial content, without our written permission to do so.\n" +
                "Contacting and/or harassing and/or threatening and/or influencing Kutumb or its associates or any of its users and/or any attempt of the same whether directly or indirectly by any means or modes to do anything beyond the Kutumb Terms or any law of the land.\n" +
                "Contravenes any or all of the Term of the Kutumb.\n" +
                "6.5 You agree not to do any of the following actions while using the Services:\n" +
                "\n" +
                "Harass, \"stalk\", threaten, embarrass or cause distress or discomfort upon another Kutumb participant, user, or other individual.\n" +
                "Upload, post, email or otherwise transmit via the Services any Content that Kutumb considers to be unlawful, harmful, threatening, abusive, harassing, defamatory.\n" +
                "Upload, post, email, or otherwise transmit any material that contains pornography, obscenity or content that can be found offensive by any race, religion, caste, group, sex, community.\n" +
                "Upload, post, email, or otherwise transmit any material that contains software viruses or any other computer code, files, or programs designed to interrupt, destroy, or limit functionality of any computer software or hardware or telecommunications equipment;\n" +
                "Upload, post, email, or otherwise transmit any Content that infringes any patent, trademark, trade secret, copyright, or other proprietary rights (\"Rights\") of any party;\n" +
                "Cause any screen in the Services to \"scroll\" faster than other users are able to type to it or any action to a similar disruptive effect;\n" +
                "Impersonate in the Services any person, including but not limited to, Kutumb official, forum leader, guide or host;\n" +
                "State or imply that Content you post or transmit through the Services is created or endorsed by Kutumb, nor design it in such a way to imply such creation or endorsement;\n" +
                "Disrupt the normal flow of dialogue within the Services or otherwise act in a manner that negatively affects other participants;\n" +
                "Forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted via the Services;\n" +
                "Collect or store personal data about other users;\n" +
                "Intentionally or unintentionally violate any applicable local, state, national or international law, including but not limited to any regulations having the force of law while using or accessing the Services or in connection with your use of the Services in any manner;\n" +
                "Post or transmit any unsolicited advertising, promotional materials, or any other forms of solicitation in the Services.\n" +
                "Provide or send any content which may be harmful or detrimental to Kutumb or its business associates, or which violates any restriction or policy established by Kutumb or its business associates.\n" +
                "The sensibilities of the recipient of the transmissions whether they hold a difference religious or ethical viewpoint, are of a different race or heritage, have a mental disorder or handicap, or any other person having special circumstances;\n" +
                "Content which promotes or incites terrorism, the misuse of weapons, or encourages or incites a person to commit a criminal offense.\n" +
                "6.6 Risk Assumption and Precautions: You acknowledge that Kutumb does not pre‐screen Content, but that Kutumb shall have the right (but not the obligation) in their sole discretion to refuse or move any Content that is available via the Services. Without limitation, Kutumb shall have the right to remove any Content that violates this Terms of Service or is otherwise objectionable, in the opinion of Kutumb. All risks when using the Services and the Site, including but not limited to all of the risks associated with any online or offline interactions with others, including meeting other matches remain with you. You are expected to use your best judgment in experiencing any of the benefits/ requirements of the Services. You agree that you must independently evaluate, and bear all risks associated with, the use of any Content, including any reliance on the accuracy, completeness, or usefulness of such Content. In this regard, you acknowledge that you do the necessary due diligence before relying on any Content created by users, including registered users, of Kutumb, transmitted through the Services, or submitted to Kutumb. You are also agreeing to take all necessary precautions when meeting or interacting with individuals through the Site and under the Services. In addition, you agree and confirms to review and follow the recommendations set forth in the Site as pointers to Safety and/ or mailed to you from time to time. You undertake to adhere to the Safety pointers and shall all times be responsible for all outcomes of meeting/ sharing information and/ or being in a short or a long term relationship through the Services/ Site. It is accepted and understood by you that at all times you shall use your judgement and discretion in receiving the Services through the Site or proceeding with a match and/ or deciding to be in a relationship with a match here from.\n" +
                "\n" +
                "6.7 You acknowledge and agree that Kutumb may preserve Content and may also disclose Content if required to do so by law or in the good faith belief that such preservation or disclosure is reasonably necessary to: (a) comply with legal process; (b) enforce this Agreement; (c) respond to claims that any Content violates the rights of third‐parties; or (d) protect the rights, property, or personal safety of Kutumb, its users and the public. You understand that the technical processing and transmission of the Service, including your Content, may involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices.\n" +
                "\n" +
                "7. Content in the Services\n" +
                "\n" +
                "7.1 You understand that all information (such as files, graphics, links, messages, data files, written text, software, music, audio files or other sounds, photographs, videos or other images, communication or other materials), whether publicly posted or privately transmitted, which you may have access to as part of, or through your use of the Services are the sole responsibility of the person from which such content originated. All such information is referred to below as the \"Content\". Kutumb does not control the Content posted or transmitted in any way via the Services and, as such, does not guarantee the accuracy, integrity or quality, completeness, reliability, fitness for any particular purpose or usefulness of such Content.\n" +
                "\n" +
                "7.2 You should be aware that the Content presented to you as part of the Services, including but not limited to advertisements in the Services and sponsored Content within the Services may be protected by intellectual property rights which are owned by the sponsors or advertisers who provide that Content to Kutumb (or by other persons or companies on their behalf). You shall not modify, rent, lease, loan, sell, distribute or create derivative works based on this Content (either in whole or in part) unless you have been specifically told that you may do so by Kutumb or by the owners of that Content, in a separate agreement.\n" +
                "\n" +
                "7.3 You agree that you are solely responsible for (and that Kutumb has no responsibility to you or to any third party for) any Content that you create, transmit or display while using the Services and for the consequences of your actions (including any loss or damage which Kutumb may suffer) by doing so.\n" +
                "\n" +
                "7.4 You agree to resolve directly and exclusively with third parties any disputes you may have about Content that they have posted or transmitted via the Services. In this regard, you understand that Kutumb does not monitor or control the Content posted by others, and instead simply provides a service by allowing users to access information that has been made available.\n" +
                "\n" +
                "8. Proprietary rights\n" +
                "\n" +
                "8.1 You acknowledge and agree that Kutumb or Kutumb's licensors, as the case may be, own all legal right, title and interest in and to the Services, including any intellectual property rights which subsist in the Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist). You further acknowledge that the Services may contain information which is designated confidential by Kutumb and that you shall not disclose such information without Kutumb's prior written consent.\n" +
                "\n" +
                "8.2 Unless you have agreed otherwise in writing with Kutumb, nothing in the Terms gives you a right to use any of Kutumb's trade names, trademarks, service marks, logos, domain names, and other distinctive brand features.\n" +
                "\n" +
                "8.3 You agree that you shall not remove, obscure, or alter any proprietary rights notices which may be affixed to or contained within the Services.\n" +
                "\n" +
                "8.4 You agree that in using the Services, you will not use any trade mark, service mark, trade name, logo or any other Intellectual Properties of any company or organization in a way that is likely or intended to cause confusion about the owner or authorized user of such marks, names or logos or other Intellectual Properties, unless you have good right, full power and authority to use such respective marks, names or logos or other Intellectual Properties.\n" +
                "\n" +
                "9. Content License from You\n" +
                "\n" +
                "9.1 You retain copyright and any other rights you already hold in Content which you submit, post or display on or through, the Services. By submitting, posting or displaying the content you give Kutumb a perpetual, irrevocable, worldwide, royalty‐free, and non‐exclusive license to reproduce, adapt, modify, translate, publish, publicly perform, publicly display and distribute any Content which you submit, post or display on or through, the Services.\n" +
                "\n" +
                "9.2 You agree that this license includes a right for Kutumb to make such Content available to other companies, organizations or individuals with whom Kutumb has relationships for the provision of syndicated services, and to use such Content in connection with the provision of those services.\n" +
                "\n" +
                "9.3 You understand that Kutumb, in performing the required technical steps to provide the Services to our users, may (a) transmit or distribute your Content over various public networks and in various media; and (b) make such changes to your Content as are necessary to conform and adapt that Content to the technical requirements of connecting networks, devices, services or media. You agree that this license shall permit Kutumb to take these actions.\n" +
                "\n" +
                "9.4 You confirm and warrant to Kutumb that you have all the rights, power and authority necessary to grant the above license.\n" +
                "\n" +
                "9.5 You are solely responsible for any content that you submit post or transmit via our Software and Services.\n" +
                "\n" +
                "10. Termination:\n" +
                "\n" +
                "10.1 The Terms will continue to apply until terminated by either you or Kutumb as set out below.\n" +
                "\n" +
                "10.2 If you want to terminate your legal agreement with Kutumb, you may do so by sending email to contact@letsKutumb.com. Upon receipt of your email, Kutumb will close your account and same will not be visible to you and other users but Kutumb may continue to keep your data for purposes mentioned in the Terms and/or the Privacy Policy.\n" +
                "\n" +
                "10.3 Kutumb may at any time, terminate its legal agreement with you if:\n" +
                "\n" +
                "You have breached any provision of the Terms (or have acted in manner which clearly shows that you do not intend to, or are unable to comply with the provisions of the Terms); or\n" +
                "Kutumb is required to do so by law (for example, where the provision of the Services to you is, or becomes, unlawful); or\n" +
                "The provision of the Services to you by Kutumb is, in Kutumb's opinion, no longer commercially viable.\n" +
                "10.4 When the Terms come to an end, all of the legal rights, obligations and liabilities that you and Kutumb have benefited from, been subject to (or which have accrued over time whilst the Terms have been in force) or which are expressed to continue indefinitely, shall be unaffected by this cessation, and the provisions of article 9 shall continue to apply to such rights, obligations and liabilities indefinitely.\n" +
                "\n" +
                "11. EXCLUSION OF WARRANTIES\n" +
                "\n" +
                "11.1 NOTHING IN THE TERMS SHALL EXCLUDE OR LIMIT Kutumb'S WARRANTY OR LIABILITY FOR LOSSES WHICH MAY NOT BE LAWFULLY EXCLUDED OR LIMITED BY APPLICABLE LAW. OUR LIABILITY WILL BE EXCLUDED OR LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.\n" +
                "\n" +
                "11.2 YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR USE OF THE SERVICES IS AT YOUR SOLE RISK AND THAT THE SERVICES ARE PROVIDED \"AS IS\" AND \"AS AVAILABLE.\"\n" +
                "\n" +
                "11.3 IN PARTICULAR, Kutumb DO NOT REPRESENT OR WARRANT TO YOU THAT:\n" +
                "\n" +
                "YOUR USE OF THE SERVICES WILL MEET YOUR REQUIREMENTS, \uF0B7 YOUR USE OF THE SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE OR FREE FROM ERROR,\n" +
                "ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE OF THE SERVICES WILL BE ACCURATE OR RELIABLE, AND\n" +
                "THAT DEFECTS IN THE OPERATION OR FUNCTIONALITY OF ANY SOFTWARE PROVIDED TO YOU AS PART OF THE SERVICES WILL BE CORRECTED.\n" +
                "11.4 ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.\n" +
                "\n" +
                "11.5 NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM Kutumb OR THROUGH OR FROM THE SERVICES SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TERMS.\n" +
                "\n" +
                "11.6 Kutumb FURTHER EXPRESSLY DISCLAIMS ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON‐ INFRINGEMENT.\n" +
                "\n" +
                "12. LIMITATION OF LIABILITY\n" +
                "\n" +
                "12.1 SUBJECT TO OVERALL PROVISION IN PARAGRAPH\n" +
                "\n" +
                "12.1 ABOVE, YOU EXPRESSLY UNDERSTAND AND AGREE THAT Kutumb SHALL NOT BE LIABLE TO YOU FOR:\n" +
                "\n" +
                "ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL CONSEQUENTIAL OR EXEMPLARY DAMAGES WHICH MAY BE INCURRED BY YOU, HOWEVER CAUSED AND UNDER ANY THEORY OF LIABILITY. THIS SHALL INCLUDE, BUT NOT BE LIMITED TO, ANY LOSS OF PROFIT (WHETHER INCURRED DIRECTLY OR INDIRECTLY), ANY LOSS OF GOODWILL OR BUSINESS REPUTATION, ANY LOSS OF DATA SUFFERED, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR OTHER INTANGIBLE LOSS;\n" +
                "ANY LOSS OR DAMAGE WHICH MAY BE INCURRED BY YOU, INCLUDING BUT NOT LIMITED TO LOSS OR DAMAGE AS A RESULT OF:\n" +
                "ANY RELIANCE PLACED BY YOU ON THE COMPLETENESS, ACCURACY OR EXISTENCE OF ANY ADVERTISING OR INFORMATION POSTED BY ANY USER, OR AS A RESULT OF ANY RELATIONSHIP OR TRANSACTION BETWEEN YOU AND ANY ADVERTISER OR SPONSOR WHOSE ADVERTISING APPEARS ON THE SERVICES OR ANY USER;\n" +
                "ANY CHANGES WHICH Kutumb MAY MAKE TO THE SERVICES, OR FOR ANY PERMANENT OR TEMPORARY CESSATION IN THE PROVISION OF THE SERVICES (OR ANY FEATURES WITHIN THE SERVICES); \uF0B7 THE DELETION OF, CORRUPTION OF, OR FAILURE TO STORE, ANY CONTENT AND OTHER COMMUNICATIONS DATA MAINTAINED OR TRANSMITTED (INCLUDING DATA SENT VIA SMS) BY OR THROUGH YOUR USE OF THE SERVICES;\n" +
                "YOUR FAILURE TO PROVIDE Kutumb WITH ACCURATE ACCOUNT INFORMATION;\n" +
                "YOUR FAILURE TO KEEP YOUR PASSWORD OR ACCOUNT DETAILS SECURE AND CONFIDENTIAL.\n" +
                "12.2 THE LIMITATIONS ON Kutumb'S LIABILITY TO YOU IN PARAGRAPH\n" +
                "\n" +
                "12.1 ABOVE SHALL APPLY WHETHER OR NOT Kutumb HAS BEEN ADVISED OF OR SHOULD HAVE BEEN AWARE OF THE POSSIBILITY OF ANY SUCH LOSSES ARISING.\n" +
                "\n" +
                "13. Advertisements\n" +
                "\n" +
                "13.1 Some of the Services are supported by advertising revenue and may display advertisements and promotions. These advertisements may be targeted to the content of information stored on the Services, queries made through the Services or other information.\n" +
                "\n" +
                "13.2 The manner, mode and extent of advertising by Kutumb on the Services are subject to change without specific notice to you.\n" +
                "\n" +
                "13.3 In consideration for Kutumb granting you access to and use of the Services, you agree that Kutumb may place such advertising on the Services.\n" +
                "\n" +
                "14. Privacy Policy and your personal information:\n" +
                "\n" +
                "For information about Kutumb's privacy practices, please read Kutumb's privacy policy. This privacy policy explains how Kutumb treats your personal information, and protects your privacy, when you use the Services. You agree to the use of your data in accordance with Kutumb's privacy policies.\n" +
                "\n" +
                "15. Other content\n" +
                "\n" +
                "15.1 The Services may include hyperlinks to other web sites or content or resources. Kutumb may have no control over any web sites or resources which are provided by companies or persons other than Kutumb.\n" +
                "\n" +
                "15.2 You acknowledge and agree that Kutumb is not responsible for the availability of any such external sites or resources, and does not endorse any advertising, products or other materials on or available from such web sites or resources.\n" +
                "\n" +
                "15.3 You acknowledge and agree that Kutumb is not liable for any loss or damage which may be incurred by you as a result of the availability of those external sites or resources, or as a result of any reliance placed by you on the completeness, accuracy or existence of any advertising, products or other materials on, or available from, such web sites or resources.\n" +
                "\n" +
                "16. Changes to the Terms\n" +
                "\n" +
                "16.1 Kutumb may make changes to the Terms from time to time. When these changes are made, Kutumb will make a new copy of the Terms available on the Site.\n" +
                "\n" +
                "16.2 You understand and agree that if you use the Services after the date on which the Terms have changed, Kutumb will treat your use as acceptance of the updated Terms.\n" +
                "\n" +
                "17. Indemnity\n" +
                "\n" +
                "You hereby indemnify and hold Kutumb, and its subsidiaries, affiliates, officers, agents, and employees, harmless from any loss, costs, damages, expenses, and liability caused by your use of the Service or the Content, or your violation of this Agreement, or your violation of any rights of a third party through use of the Service or the Content.\n" +
                "\n" +
                "18. General Legal Terms\n" +
                "\n" +
                "18.1 The Terms constitute the whole legal agreement between you and Kutumb and govern your use of the Services (but excluding any services which Kutumb may provide to you under a separate written agreement), and completely replace any prior agreements between you and Kutumb in relation to the Services.\n" +
                "\n" +
                "18.2 You agree that Kutumb may provide you with notices, including those regarding changes to the Terms, by SMS text message, email, regular mail, or postings on the Services.\n" +
                "\n" +
                "18.3 You agree that if Kutumb does not exercise or enforce any legal right or remedy which is contained in the Terms (or which Kutumb has the benefit of under any applicable law), this will not be taken to be a formal waiver of Kutumb's rights and that those rights or remedies will still be available to Kutumb.\n" +
                "\n" +
                "18.4 If any court of law, having the jurisdiction to decide on this matter, rules that any provision of the Terms is invalid, then that provision will be removed from the Terms without affecting the rest of the Terms. The remaining provisions of the Terms will continue to be valid and enforceable.\n" +
                "\n" +
                "18.5 Law and Jurisdiction\n" +
                "\n" +
                "The Terms, and your relationship with Kutumb under the Terms, shall be governed by the laws of India without regard to its conflict of law’s provisions. Any legal actions against Kutumb must be commenced at Noida within one year after the claim arose. You consent to the exclusive jurisdiction and venue of the state courts of competent jurisdiction located at Noida. You and Kutumb agree to submit to the exclusive jurisdiction of the courts located within the country of India to resolve any legal matter arising from the Terms. Notwithstanding this, you agree that Kutumb shall still be allowed to apply for injunctive remedies (or an equivalent type of urgent legal relief) in any jurisdiction. Contacting Kutumb If you have any questions or concerns about the Terms of Service, please write to contact@letsKutumb.com. In case of any grievances regarding any content on Kutumb, please write to the Grievances Officer at contact@bamfaltech.com or call us on +918527541059.");
        return mView;
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Terms & Conditions";
    }

    @Override
    public String getFragmentTitle() {
        return "TermsAndConditionsFragment";
    }

}
