package bamfaltech.kutumb.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import bamfaltech.kutumb.CoachMarksActivity;
import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.GenericActionBar;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.view.CustomTabLayout;

/**
 * Created by gaurav.bansal1 on 24/12/16.
 */

public class TabContainerFragment extends BaseKutumbFragment {

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private FragmentStatePagerAdapter mAdapter;
    private String[] TAB_NAME = {"Home", "Chat", "Utility"};
    public static final String SHOW_TAB_POSITION = "tab_position";
    public static final int SHOW_TAB_HOME = 0;
    public static final int SHOW_TAB_CHAT = 1;
    public static final int SHOW_TAB_UTILITY = 2;
    //public static final int SHOW_TAB_MYMUSIC = 3;
    private static final int TOTAL_NUM_OF_TABS = 3; // total number of pages to show
    private Bundle mBundle; // need it for supporting deeplinking
    private int selectedPosition = 0; // -1 denotes fragment was not created, positive value will specify selected tab position
    private AdView mAdView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View tabContainerView = inflater.inflate(R.layout.tab_container_fragment_layout, container, false);
        mBundle = getArguments();
        if (mBundle != null && selectedPosition == -1) {
            selectedPosition = mBundle.getInt(SHOW_TAB_POSITION, SHOW_TAB_HOME);
        }

        mContext = getActivity();
        init(tabContainerView);

        //loadAd(tabContainerView);

        return tabContainerView;
    }

    private void loadAd(View view){
        // Initialize the Mobile Ads SDK.
        MobileAds.initialize(getActivity(), "ca-app-pub-3940256099942544~3347511713");

        // Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
        // values/strings.xml.
        mAdView = (AdView) view.findViewById(R.id.ad_view);

        // Create an ad request. Check your logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("060D8B587E3C6D20BC7C93D722939536")
                .build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
            ((KutumbActivity)mContext).setCurrentPosition(mTabLayout.getSelectedTabPosition());
        }
    }

    public FragmentStatePagerAdapter getAdapter() {
        return mAdapter;
    }

    private void init(View view){
        mViewPager = (ViewPager) view.findViewById(R.id.container);
        setActionBar(view, new GenericActionBar(mContext, "Home", false), true);

        mAdapter = new TabPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mTabLayout = (TabLayout) view.findViewById(R.id.sliding_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(selectedPosition).select();

        setupTabIcons(mTabLayout);

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                ((KutumbActivity)mContext).setCurrentPosition(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                selectedPosition = tab.getPosition();
                mTabLayout.getTabAt(selectedPosition).select();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //showCoachmarks();
    }

    private void showCoachmarks() {
        Intent coachMarksActivity = new Intent(getActivity(), CoachMarksActivity.class);
        coachMarksActivity.putExtra(Constants.COACHMARK_VALUE, Constants.COACHMARK_VALUE_SEARCH);
        startActivity(coachMarksActivity);
    }

    @Override
    public String getFragmentTitle() {
        return "tabContainer";
    }

    /**
     * PagerAdapter which provides pages to the viewpager
     */
    class TabPagerAdapter extends FragmentStatePagerAdapter {

        public TabPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case SHOW_TAB_HOME:
                    fragment = new RecentPostsFragment();
                    break;
                case SHOW_TAB_CHAT:
                    fragment = new PersonalChatListFragment();
                    break;
                case SHOW_TAB_UTILITY:
                    fragment = new UtilityFragment();
                    break;
                /*case SHOW_TAB_MYMUSIC:
                    fragment = new MyMusicFragment();
                    break;*/

            }
            if (mBundle != null) {
                fragment.setArguments(mBundle);
            } else {
                fragment.setArguments(new Bundle());
            }
            //mListOfFragment[position] = (BaseGaanaFragment) fragment;
            //((KutumbActivity)mContext).setCurrentFragment(fragment);
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return TOTAL_NUM_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TAB_NAME[position];
        }


        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
            try {
                super.restoreState(state, loader);
            }catch (Exception e){
            }
        }
    }

    private void setupTabIcons(TabLayout tabLayout) {


        TextView tabOne = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabOne.setText(TAB_NAME[0]);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home_black_24dp, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabTwo.setText(TAB_NAME[1]);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_chat_black_24dp, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        tabThree.setText(TAB_NAME[2]);
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_business_black_24dp, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }
}
