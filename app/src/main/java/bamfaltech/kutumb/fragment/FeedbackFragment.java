package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.util.Constants;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class FeedbackFragment extends BaseKutumbFragment {

    private static final String TAG = "FeedbackFragment";
    EditText feedbackBody;
    private static final String REQUIRED = "Required";
    private Activity activity;
    private DatabaseReference mDatabase;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.feedback_fragment, container, false);
        activity = getActivity();
        feedbackBody = (EditText) mView.findViewById(R.id.feedback_body);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        init(mView);
        return mView;
    }

    @Override
    public String getFragmentTitle() {
        return "feedbackfragment";
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Feedback";
    }

    public void submitFeedback() {
        final String body = feedbackBody.getText().toString();

        // Title is required


        // Body is required
        if (TextUtils.isEmpty(body)) {
            feedbackBody.setError(REQUIRED);
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        Toast.makeText(activity, "Submitting your feedback", Toast.LENGTH_SHORT).show();

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        // [START_EXCLUDE]
                        if (user == null) {
                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(activity,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewFeedback(userId, user.getName(), user.getFlatNumber(), user.getPhoto_profile().toString(), body);
                        }

                        // Finish this Activity, back to the stream
                        ((KutumbActivity)activity).removeFragment();
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // [START_EXCLUDE]
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }
                });
        // [END single_value_read]
    }

    private void setEditingEnabled(boolean enabled) {
        //mTitleField.setEnabled(enabled);
        feedbackBody.setEnabled(enabled);
        /*if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }*/
    }

    // [START write_fan_out]
    private void writeNewFeedback(String userId, String username, String flatno, String userPhoto, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("feedback").push().getKey();
        Post post = new Post(userId, username, flatno, body);
        post.setPhoto_profile(userPhoto);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/feedback/" + key, postValues);
        childUpdates.put("/user-feedback/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }
}
