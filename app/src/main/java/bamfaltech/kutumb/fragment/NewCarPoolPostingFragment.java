package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.CarPoolModel;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.CircleTransform;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 07/03/17.
 */

public class NewCarPoolPostingFragment extends BaseKutumbFragment implements View.OnClickListener{

    private static final String TAG = "NewCarPoolPostingFragment";
    private static final String REQUIRED = "Required";

    // [START declare_database_ref]
    private DatabaseReference mDatabase;
    // [END declare_database_ref]

    //private EditText mTitleField;
    private ImageView mAuthorImage;
    private TextView mAuthorName;
    private TextView mAuthorFlatNo;

    private EditText mCarPoolSource;
    private EditText mCardPoolDestination;
    private EditText mSourceLeaveTime;
    private ImageView mLeaveSetTime;
    private EditText mDestinationLeaveTime;
    private ImageView mReturnSetTime;
    private EditText mMobileNumber;

    private FloatingActionButton mSubmitButton;
    private Spinner catagorySpinner;

    private AutoCompleteTextView catagoryAuto;
    public static final String[] Choices = {"Instrumental", "Pop", "HipHop", "Desi", "Bhangra", "Kathhak"};
    private Activity activity;

    private CheckedTextView profileMobileNumberCheck;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.new_car_pool_listing, container, false);
        mContext = getActivity();
        activity = getActivity();

        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        init(mView);

        // [START initialize_database_ref]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        //mTitleField = (EditText) findViewById(R.id.field_title);
        mAuthorImage = (ImageView) mView.findViewById(R.id.post_author_photo);
        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
        Glide.with(this).load(userModel.getPhoto_profile())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(activity))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mAuthorImage);


        mAuthorName = (TextView) mView.findViewById(R.id.post_author);
        mAuthorName.setText(Util.getAllCapitalWordsString(userModel.getName()));

        mAuthorFlatNo = (TextView) mView.findViewById(R.id.post_author_flat_no);
        mAuthorFlatNo.setText(userModel.getFlatNumber());

        mCarPoolSource = (EditText) mView.findViewById(R.id.car_pool_source_input);
        mCardPoolDestination = (EditText) mView.findViewById(R.id.car_pool_destination_input);
        mSourceLeaveTime = (EditText) mView.findViewById(R.id.car_pool_source_leave_time_input);
        mLeaveSetTime = (ImageView) mView.findViewById(R.id.set_time_source);
        mDestinationLeaveTime = (EditText) mView.findViewById(R.id.car_pool_destination_leave_time_input);
        mReturnSetTime = (ImageView) mView.findViewById(R.id.set_time_return);
        mMobileNumber = (EditText) mView.findViewById(R.id.car_pool_owner_phone_input);
        mSubmitButton = (FloatingActionButton) mView.findViewById(R.id.fab_submit_post);

        mMobileNumber.setText(userModel.getMobileNumber());
        mSourceLeaveTime.setOnClickListener(this);
        mDestinationLeaveTime.setOnClickListener(this);
        mLeaveSetTime.setOnClickListener(this);
        mReturnSetTime.setOnClickListener(this);

        profileMobileNumberCheck = (CheckedTextView) mView.findViewById(R.id.profileMobileNumberCheck);
        profileMobileNumberCheck.setOnClickListener(this);
        profileMobileNumberCheck.setChecked(true);

        catagoryAuto = (AutoCompleteTextView) mView.findViewById(R.id.catagory_auto);

        return mView;
    }

    boolean isAutoCompleteCatagory = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Offer A Ride";
    }

    public void submitCarPoolPost() {
        //final String title = catagoryAuto.getText().toString();
        final String source = mCarPoolSource.getText().toString();
        final String destination = mCardPoolDestination.getText().toString();
        final String sourceLeaveTime = mSourceLeaveTime.getText().toString();
        final String destinationLeaveTime = mDestinationLeaveTime.getText().toString();
        final String mobile = mMobileNumber.getText().toString();

        // Title is required


        // Body is required
        if (TextUtils.isEmpty(source)) {
            mCarPoolSource.setError(REQUIRED);
            return;
        } else if (TextUtils.isEmpty(destination)) {
            mCardPoolDestination.setError(REQUIRED);
            return;
        } else if (TextUtils.isEmpty(sourceLeaveTime)) {
            mSourceLeaveTime.setError(REQUIRED);
            return;
        } else if (TextUtils.isEmpty(destinationLeaveTime)) {
            mDestinationLeaveTime.setError(REQUIRED);
            return;
        } else if (TextUtils.isEmpty(mobile)) {
            mMobileNumber.setError(REQUIRED);
            return;
        }
        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        Toast.makeText(activity, "Posting...", Toast.LENGTH_SHORT).show();

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        // [START_EXCLUDE]
                        if (user == null) {
                            // User is null, error out
                            //Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(activity,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewPost(userId, user, source, destination, sourceLeaveTime, destinationLeaveTime, mobile);
                        }

                        // Finish this Activity, back to the stream
                        ((KutumbActivity)activity).removeFragment();
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // [START_EXCLUDE]
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }
                });
        // [END single_value_read]
    }

    @Override
    public String getFragmentTitle() {
        return "newutility";
    }

    private void setEditingEnabled(boolean enabled) {
        //mTitleField.setEnabled(enabled);
        //mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    // [START write_fan_out]
    private void writeNewPost(String userId, UserModel user, String source, String destination, String sourceLeaveTIme, String destinationLeaveTime, String mobile) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("carpool").push().getKey();
        CarPoolModel carPoolModel = new CarPoolModel(userId, user.getName(), user.getFlatNumber(), source, destination, sourceLeaveTIme, destinationLeaveTime, mobile);
        carPoolModel.setPhoto_profile(user.getPhoto_profile());
        RWADetailModel.RWA rwa = user.getRwa();
        if(rwa != null) {
            carPoolModel.setRwaName(rwa.getRwaname());
        }

        Map<String, Object> postValues = carPoolModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/carpool/" + key, postValues);
        childUpdates.put("/society-carpool/" + rwa.getRwaid() + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.set_time_source:
            case R.id.set_time_return:
            case R.id.car_pool_source_leave_time_input:
            case R.id.car_pool_destination_leave_time_input:
                Calendar mcurrentTime = Calendar.getInstance();
                final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                final int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(activity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        DecimalFormat decimalFormat = new DecimalFormat("00");
                        String amOrpm = "am";
                        if(selectedHour >= 12) {
                            amOrpm = "pm";
                            if(selectedHour > 12) {
                                selectedHour = selectedHour - 12;
                            }
                        }
                        if(view.getId() == R.id.set_time_source || view.getId() == R.id.car_pool_source_leave_time_input) {
                            mSourceLeaveTime.setText(decimalFormat.format(selectedHour) + ":" + decimalFormat.format(selectedMinute) + " " + amOrpm);
                        } else {
                            mDestinationLeaveTime.setText(decimalFormat.format(selectedHour) + ":" + decimalFormat.format(selectedMinute) + " " + amOrpm);
                        }
                    }
                }, hour, minute, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
                break;
            case R.id.profileMobileNumberCheck:
                if(profileMobileNumberCheck.isChecked()) {
                    profileMobileNumberCheck.setChecked(false);
                    mMobileNumber.setText("");
                } else {
                    profileMobileNumberCheck.setChecked(true);
                    UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
                    if(userModel != null) {
                        mMobileNumber.setText(userModel.getMobileNumber());
                    }
                }
                break;
        }
    }
}
