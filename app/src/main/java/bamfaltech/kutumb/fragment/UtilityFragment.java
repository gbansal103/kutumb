package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.HttpManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.viewholder.PostViewHolder;
import bamfaltech.kutumb.viewholder.UtilityCatagoryHeaderHolder;

public class UtilityFragment extends BaseKutumbFragment implements AdapterView.OnItemSelectedListener {

    private static final String TAG = "NotificationFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private TextView mNoResultsText;


    private View headerView = null;

    private Activity activity;



    public UtilityFragment() {}

    boolean mViewCreatedFirst = false;

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.utility_fragment_layout, container, false);
        mContext = getActivity();
        activity = getActivity();

        headerView = inflater.inflate(R.layout.utility_catagory_header, container, false);

        mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        /*catagorySpinner = (Spinner) rootView.findViewById(R.id.catagory);
        ArrayAdapter<String> towerListAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_list_item, Constants.catagoryList);
        towerListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        catagorySpinner.setAdapter(towerListAdapter);

        catagorySpinner.setOnItemSelectedListener(this);*/

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        mNoResultsText = (TextView) rootView.findViewById(R.id.no_results);

        View actionBarView = inflater.inflate(R.layout.action_details, container, false);
        mToolbar.addView(actionBarView);


        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        mViewCreatedFirst = true;

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        mProgressBar.setVisibility(View.VISIBLE);
        // Set up FirebaseRecyclerAdapter with the Query
        //loadAllCatagories();
        count = 0;
        Query postsQuery = getQuery(mDatabase);
        populateUtility(postsQuery);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query query = databaseReference.child("utility")
                .limitToFirst(100);
        return query;
    }


    int count = 0;

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if(count == 0) {
            count++;
            return;
        }
        if(position == 0) {
            Query postsQuery = getQuery(mDatabase);
            populateUtility(postsQuery);
            return;
        }
        String selectedCatagory = Constants.catagoryList.get(position);
        Query query = FirebaseManager.getInstance().getUtilitiesSpecificCatagory(selectedCatagory);
        populateUtility(query);
    }



    public void populateUtility(Query postsQuery) {

        // Set up Layout Manager, reverse layout
        /*mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);*/
        // Set up FirebaseRecyclerAdapter with the Query
        mAdapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(Post.class, R.layout.item_post,
                PostViewHolder.class, postsQuery) {

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                if(viewType == Constants.UTILITY_CATAGORY_SELECTION) {
                    return new UtilityCatagoryHeaderHolder(headerView);
                } else {
                    return super.onCreateViewHolder(parent, viewType);
                }
            }

            @Override
            public int getItemCount() {
                int itemCount = super.getItemCount();
                itemCount = itemCount + 1;
                return itemCount;
            }

            @Override
            public Post getItem(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItem(position);
                } else {
                    Post post = new Post();
                    post.setPhoto_profile(FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());
                    post.setAuthor("What you want to share ?");
                    return post;
                }
            }

            @Override
            public DatabaseReference getRef(int position) {
                if(position < getItemCount() - 1) {
                    return super.getRef(position);
                } else {
                    return null;
                }
            }

            @Override
            public int getItemViewType(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemViewType(position);
                } else {
                    return Constants.UTILITY_CATAGORY_SELECTION;
                }
            }

            @Override
            public long getItemId(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemId(position);
                } else {
                    return -1;
                }
            }


            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final Post model, final int position) {
                mProgressBar.setVisibility(View.GONE);
                if(getItemCount() == 1) {
                    mNoResultsText.setVisibility(View.VISIBLE);
                } else {
                    mNoResultsText.setVisibility(View.GONE);
                }
                if (viewHolder instanceof UtilityCatagoryHeaderHolder) {
                    ((UtilityCatagoryHeaderHolder)viewHolder).bindToUtilityHeader(UtilityFragment.this, activity, count, mDatabase);
                } else {
                    final DatabaseReference postRef = getRef(position);

                    // Set click listener for the whole post view
                    final String postKey = postRef.getKey();
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Launch UtilityDetailFragment
                            UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(UtilityDetailFragment.EXTRA_POST_KEY, postKey);
                            utilityDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(utilityDetailFragment);
                        }
                    });

                    // Determine if the current user has liked this post and set UI accordingly
                    if (model.stars.containsKey(getUid())) {
                        viewHolder.mLikeText.setText("Interested");
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                        viewHolder.mLikeImage.setImageResource(R.drawable.heart_red);
                    } else {
                        viewHolder.mLikeText.setText("Interested");
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.black));
                        viewHolder.mLikeImage.setImageResource(R.drawable.ic_favorite_black_24dp);
                    }

                    // Bind Post to ViewHolder, setting OnClickListener for the star button
                    viewHolder.bindToUtility(postRef, model, new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {
                            // Need to write to both places the post is stored
                            DatabaseReference globalPostRef = mDatabase.child("utility").child(postRef.getKey());
                            DatabaseReference userPostRef = mDatabase.child("user-utility").child(model.uid).child(postRef.getKey());

                            // Run two transactions
                            onStarClicked(globalPostRef, true);
                            onStarClicked(userPostRef, false);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TopicFragment topicFragment = new TopicFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("topic", model.title);
                            bundle.putInt("type",Constants.TOPIC_LIST_TYPE_UTILITY);
                            topicFragment.setArguments(bundle);
                            ((KutumbActivity) activity).displayFragment(topicFragment);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(UtilityDetailFragment.EXTRA_POST_KEY, postKey);
                            utilityDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(utilityDetailFragment);
                        }
                    }, UtilityFragment.this);

                }
            }
        };
        mRecycler.setAdapter(mAdapter);
    }


    // [START post_stars_transaction]
    private void onStarClicked(final DatabaseReference postRef, final boolean shouldSendNotif) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);mProgressBar.setVisibility(View.GONE);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                    if(shouldSendNotif) {
                        String frontUserID = p.uid;
                        if (!frontUserID.equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
                            FirebaseManager.getInstance().getFrontUserInfoFromFirebase(frontUserID, new Interfaces.OnUserModelRetrieved() {
                                @Override
                                public void onRetreivalComplete(UserModel userModel) {
                                    UserModel senderUserModel = FirebaseManager.getInstance().getCurrentUser();
                                    sendMessage(senderUserModel.getName() + ", " + senderUserModel.getFlatNumber() + " showed interest in your utility post", userModel.getFCMRegID(), postRef.getKey());
                                }

                                @Override
                                public void onError(String errormsg) {

                                }
                            });
                        }
                    }
                }



                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }
    // [END post_stars_transaction]


    public void sendMessage(final String messageToSend, final String frontUserFCMRegID, String postKey) {

        if (messageToSend.length() > 0) {

            Log.i(TAG, "sendMessage");

            HttpManager.getInstance().sendMessage(messageToSend, frontUserFCMRegID, Constants.NOTIFICATION_PURPOSE_UTILITY_INTEREST, postKey);

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
