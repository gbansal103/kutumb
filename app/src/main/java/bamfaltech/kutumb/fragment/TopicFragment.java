package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.viewholder.PostViewHolder;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class TopicFragment extends BaseKutumbFragment implements View.OnClickListener{

    String topicName = "";
    int type = Constants.TOPIC_LIST_TYPE_POST;
    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private String TAG = "TopicFragment";

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.topic_fragment, container, false);

        activity = getActivity();

        mProgressBar = (ProgressBar) mView.findViewById(R.id.progressbar);
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            topicName = bundle.getString("topic");
            type = bundle.getInt("type");
        }
        init(mView);
        ImageView currenrUserPic = (ImageView) mView.findViewById(R.id.current_user_pic);
        Util.bindCircularImage(currenrUserPic, FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());

        mView.findViewById(R.id.top_layout).setOnClickListener(this);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) mView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return mView;
    }

    @Override
    public String getFragmentTitle() {
        return "TopicFragment";
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, topicName);
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(topicName);
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "TopicFragment";
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        if(type == Constants.TOPIC_LIST_TYPE_POST) {
            populatePostTopicAdapter();
        } else {
            populateUtilityTopicAdapter();
        }

        mProgressBar.setVisibility(View.GONE);
        mRecycler.setAdapter(mAdapter);
    }

    private void populateUtilityTopicAdapter() {
        Query topicQuery = FirebaseManager.getInstance().getUtilitiesSpecificCatagory(topicName);
        mAdapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(Post.class, R.layout.item_post,
                PostViewHolder.class, topicQuery) {


            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final Post model, final int position) {
                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Launch UtilityDetailFragment
                        UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(UtilityDetailFragment.EXTRA_POST_KEY, postKey);
                        utilityDetailFragment.setArguments(bundle);
                        ((KutumbActivity)activity).displayFragment(utilityDetailFragment);
                    }
                });

                // Determine if the current user has liked this post and set UI accordingly
                if (model.stars.containsKey(getUid())) {
                    viewHolder.mLikeText.setText("Interested");
                    viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    viewHolder.mLikeImage.setImageResource(R.drawable.heart_red);
                } else {
                    viewHolder.mLikeText.setText("Interested");
                    viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.black));
                    viewHolder.mLikeImage.setImageResource(R.drawable.ic_favorite_black_24dp);
                }

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToUtility(postRef, model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        // Need to write to both places the post is stored
                        DatabaseReference globalPostRef = mDatabase.child("utility").child(postRef.getKey());
                        DatabaseReference userPostRef = mDatabase.child("user-utility").child(model.uid).child(postRef.getKey());

                        // Run two transactions
                        onStarClicked(globalPostRef);
                        onStarClicked(userPostRef);
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(activity, "You are already in "+topicName+ " catagory", Toast.LENGTH_SHORT).show();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(UtilityDetailFragment.EXTRA_POST_KEY, postKey);
                        utilityDetailFragment.setArguments(bundle);
                        ((KutumbActivity)activity).displayFragment(utilityDetailFragment);
                    }
                }, TopicFragment.this);
            }
        };
    }

    private void populatePostTopicAdapter() {
        Query topicQuery = FirebaseManager.getInstance().getTopicSpecificCatagory(topicName);
        mAdapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(Post.class, R.layout.item_post,
                PostViewHolder.class, topicQuery) {


            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final Post model, final int position) {
                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Launch PostDetailFragment
                        PostDetailFragment postDetailFragment = new PostDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                        postDetailFragment.setArguments(bundle);
                        ((KutumbActivity)activity).displayFragment(postDetailFragment);
                    }
                });

                // Determine if the current user has liked this post and set UI accordingly
                if (model.stars.containsKey(getUid())) {
                    viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    viewHolder.mLikeImage.setImageResource(R.drawable.heart_red);
                } else {
                    viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.black));
                    viewHolder.mLikeImage.setImageResource(R.drawable.ic_favorite_black_24dp);
                }

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToPost(postRef, model, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FirebaseManager.getInstance().getFrontUserInfoFromFirebase(model.uid, new Interfaces.OnUserModelRetrieved() {
                            @Override
                            public void onRetreivalComplete(UserModel userModel) {
                                UserProfileFragment userProfileFragment = new UserProfileFragment();
                                Bundle bundle = new Bundle();
                                bundle.putSerializable("userModel", userModel);
                                userProfileFragment.setArguments(bundle);
                                ((KutumbActivity)activity).displayFragment(userProfileFragment);
                            }

                            @Override
                            public void onError(String errormsg) {

                            }
                        });

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        // Need to write to both places the post is stored
                        DatabaseReference globalPostRef = mDatabase.child("posts").child(postRef.getKey());
                        DatabaseReference userPostRef = mDatabase.child("user-posts").child(model.uid).child(postRef.getKey());

                        // Run two transactions
                        onStarClicked(globalPostRef);
                        onStarClicked(userPostRef);
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PostDetailFragment postDetailFragment = new PostDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                        postDetailFragment.setArguments(bundle);
                        ((KutumbActivity)mContext).displayFragment(postDetailFragment);
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(activity, "You are already in "+topicName+ " post", Toast.LENGTH_SHORT).show();
                    }
                }, TopicFragment.this);
            }
        };
    }

    // [START post_stars_transaction]
    private void onStarClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                }

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }
    // [END post_stars_transaction]


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    @Override
    public void onClick(View view) {

    }
}
