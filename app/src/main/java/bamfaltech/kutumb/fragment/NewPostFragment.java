package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.CircleTransform;
import bamfaltech.kutumb.util.Util;

public class NewPostFragment extends BaseKutumbFragment implements View.OnClickListener {

    private static final String TAG = "NewPostFragment";
    private static final String REQUIRED = "Required";

    // [START declare_database_ref]
    private DatabaseReference mDatabase;
    // [END declare_database_ref]

    private ImageView mAuthorImage;
    private TextView mAuthorName;
    private TextView mAuthorFlatNo;
    private EditText mBodyField;
    private FloatingActionButton mSubmitButton;
    private boolean isTopicPost = false;
    private String postTitle = "";

    private Activity activity;
    private TextView newPostText;
    private AutoCompleteTextView titleAuto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.activity_new_post, container, false);
        mContext = getActivity();
        activity = getActivity();
        init(mView);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        mAuthorImage = (ImageView) mView.findViewById(R.id.post_author_photo);
        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
        Glide.with(this).load(userModel.getPhoto_profile())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(activity))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mAuthorImage);

        mAuthorName = (TextView) mView.findViewById(R.id.post_author);
        mAuthorName.setText(Util.getAllCapitalWordsString(userModel.getName()));

        mAuthorFlatNo = (TextView) mView.findViewById(R.id.post_author_flat_no);
        mAuthorFlatNo.setText(userModel.getFlatNumber());

        newPostText = (TextView) mView.findViewById(R.id.post_topic);
        newPostText.setOnClickListener(this);

        titleAuto = (AutoCompleteTextView) mView.findViewById(R.id.title_auto);
        getTitleList();
        titleAuto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    titleAuto.showDropDown();
                }
            }
        });

        mBodyField = (EditText) mView.findViewById(R.id.field_body);
        mSubmitButton = (FloatingActionButton) mView.findViewById(R.id.fab_submit_post);

        /*mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });*/

        return mView;
    }

    boolean isAutoCompleteCatagory = false;
    public void getTitleList() {
        DatabaseReference ref = mDatabase.child("topics");
        FirebaseManager.getInstance().getTopicList(ref, new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                if(object != null && object instanceof List) {
                    List<String> catagoryStringList = (List)object;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                            android.R.layout.simple_dropdown_item_1line, Util.convertListToStringArray(catagoryStringList));
                    titleAuto.setThreshold(0);
                    titleAuto.setAdapter(adapter);
                    titleAuto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            isAutoCompleteCatagory = true;
                        }
                    });
                    titleAuto.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            isAutoCompleteCatagory = false;
                        }
                    });

                }
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public String getFragmentTitle() {
        return "newpost";
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "New Post";
    }

    /*FirebaseAuth mFirebaseAuth;
    FirebaseUser mFirebaseUser;
    private FirebaseUser getUserIfLogin(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        *//*if(mFirebaseUser != null){
            return mFirebaseUser.getPhotoUrl().toString();
        }*//*
        return mFirebaseUser;
    }*/

    public void submitTopic() {

    }

    public void submitPost() {
        //final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();
        if(isTopicPost) {
            postTitle = titleAuto.getText().toString();
            if(TextUtils.isEmpty(postTitle)) {
                Toast.makeText(mContext, "Topic can not be blank", Toast.LENGTH_LONG).show();
                return;
            }
        }

        // Title is required
        /*if (TextUtils.isEmpty(title)) {
            mTitleField.setError(REQUIRED);
            return;
        }*/

        // Body is required
        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(REQUIRED);
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        Toast.makeText(activity, "Posting...", Toast.LENGTH_SHORT).show();

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        // [START_EXCLUDE]
                        if (user == null) {
                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(activity,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            if(isTopicPost) {
                                writeNewPost(userId, user, postTitle, body);
                                //writeNewPost(userId, user.getName(), user.getFlatNumber(), user.getPhoto_profile(), postTitle, body);
                                if(!isAutoCompleteCatagory) {
                                    writeNewTopic(userId, postTitle);
                                }
                            } else {
                                writeNewPost(userId, user, null, body);
                            }
                        }

                        // Finish this Activity, back to the stream
                        ((KutumbActivity)activity).removeFragment();
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // [START_EXCLUDE]
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }
                });
        // [END single_value_read]
    }

    private void setEditingEnabled(boolean enabled) {
        //mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    // [START write_fan_out]
    private void writeNewPost(String userId, String username, String userFlat, String userPhoto, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(userId, username, userFlat, body);
        post.setPhoto_profile(userPhoto);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewPost(String userId, UserModel user, String title, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post;
        if(TextUtils.isEmpty(title)) {
            post = new Post(userId, user.getName(), user.getFlatNumber(), body);
        } else {
            post = new Post(userId, user.getName(), user.getFlatNumber(), title, body);
        }
        post.setPhoto_profile(user.getPhoto_profile());
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);
        RWADetailModel.RWA userRWA = user.getRwa();
        if(userRWA != null) {
            String societyKey = userRWA.getRwaid();
            childUpdates.put("/society-posts/"+societyKey+"/"+key, postValues);
        }

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewPost(String userId, String username, String userFlat, String userPhoto, String title, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(userId, username, userFlat, title, body);
        post.setPhoto_profile(userPhoto);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewTopic(String userId, String postTitle) {
        String key = mDatabase.child("topics").push().getKey();
        TopicModel topicModel = new TopicModel(userId, postTitle);
        Map<String, Object> topicValues = topicModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/topics/" + key, topicValues);
        //childUpdates.put("/user-topics/" + userId + "/" + key, topicValues);

        mDatabase.updateChildren(childUpdates);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.post_topic:
                if(!isTopicPost) {
                    isTopicPost = true;
                    titleAuto.setVisibility(View.VISIBLE);
                    Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.ic_clear_black_24dp);
                    newPostText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                } else {
                    isTopicPost = false;
                    titleAuto.setVisibility(View.GONE);
                    Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.ic_add_circle_black_24dp);
                    newPostText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
                break;
        }
    }
    // [END write_fan_out]
}
