package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.adapter.CustomListAdapter;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.models.ContactModel;
import bamfaltech.kutumb.models.PeopleObject;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.view.LoginActivity;
import bamfaltech.kutumb.viewholder.ContactViewHolder;
import bamfaltech.kutumb.viewholder.FlatMemberHolder;
import bamfaltech.kutumb.viewholder.PostViewHolder;

/**
 * Created by gaurav.bansal1 on 07/03/17.
 */

public class ImportantContactFragment extends BaseKutumbFragment implements CustomListAdapter.IAddListItemView, View.OnClickListener{

    private static final String TAG = "ImportantContact";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private TextView mNoPeple;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    //CLass Model
    private UserModel userModel;
    private Activity activity;
    private CustomListAdapter mCustomListAdapter;

    List<ContactModel.Contact> contactList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.contact_fragment, container, false);
        activity = getActivity();

        setupActionBar(rootView);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        mNoPeple = (TextView) rootView.findViewById(R.id.no_people);



        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }

    private void setupActionBar(View rootView) {
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle(){
        return "Important Contacts";
    }

    /**
     * Verificar se usuario está logado
     */
    private void verifyUserIfLogin(){
        /*mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();*/
        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
        /*if (mFirebaseUser == null){
            startActivity(new Intent(activity, LoginActivity.class));
            activity.finish();
        }else{
            userModel = new UserModel(mFirebaseUser.getDisplayName(), mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid(), mFirebaseUser.getEmail());*/
            //readFirebaseMessages();
        if(userModel.getRwa() != null) {
            getImportantSocietyContacts(userModel.getRwa().getRwaid());
        } else {
            mNoPeple.setVisibility(View.VISIBLE);
        }
        //}
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);
        //Query chatListQuery = getQuery(mDatabase);
        verifyUserIfLogin();
    }


    void getImportantSocietyContacts(final String rwaid) {
        //Util.showPB(activity, "Getting People to Chat...");

        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("rwaid", rwaid);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(ContactModel.class);
        urlManager.setPriority(Request.Priority.HIGH);
        urlManager.setCachable(true);
        urlManager.setMethod(Request.Method.GET);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.GET_SOCIETY_IMP_CONTACTS);


        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                mProgressBar.setVisibility(View.GONE);
                ContactModel contactModel = (ContactModel) object;
                if(contactModel != null) {
                    contactList = (ArrayList<ContactModel.Contact>) contactModel.getArrListBusinessObj();
                    if (contactList.size() > 0) {
                        mCustomListAdapter = new CustomListAdapter(activity);
                        mCustomListAdapter.setParamaters(contactList.size(), ImportantContactFragment.this);
                        mRecycler.setAdapter(mCustomListAdapter);
                    }
                } else {
                    mNoPeple.setVisibility(View.VISIBLE);
                    Toast.makeText(activity, "No people", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorMsg) {
                mProgressBar.setVisibility(View.GONE);
                mNoPeple.setVisibility(View.VISIBLE);
                Toast.makeText(activity, "Server Error", Toast.LENGTH_SHORT).show();
            }
        }, urlManager);
    }


    /*@Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PeopleObject p = peopleObjList.get(arg2);
        String chattingToName = p.getPersonName();
        String chattingToDeviceID = p.getRegId();

        Intent intent = new Intent(ListPeople.this, ChatActivity.class);
        intent.putExtra("chattingFrom",
                utils.getFromPreferences(Utils.UserName));
        intent.putExtra("chattingToName", chattingToName);
        intent.putExtra("chattingToDeviceID", chattingToDeviceID);
        startActivity(intent);
    }*/

    @Override
    public void addListItemView(final int position, RecyclerView.ViewHolder pConvertView, ViewGroup parentView) {
        ((ContactViewHolder)pConvertView).bindToContactView(contactList.get(position));
    }

    private void updateChatUser(String room, final String status, final PeopleObject.People people, final int position) {
        people.setChatStatus(status);
        mCustomListAdapter.notifyItemChanged(position);
    }

    @Override
    public void showHideEmtpyView(boolean showEmptyView) {

    }

    @Override
    public RecyclerView.ViewHolder createViewHolder(ViewGroup parent, int viewType) {
        View convertView;
        convertView = LayoutInflater.from(activity).inflate(R.layout.contact_item, parent, false);
        return new ContactViewHolder(convertView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onClick(View view) {
        /*switch (view.getId()) {
            case R.id.no_people:
                FlatChooserFragment fragment = new FlatChooserFragment();
                ((KutumbActivity)activity).displayFragment(fragment);
                break;
        }*/
    }
}
