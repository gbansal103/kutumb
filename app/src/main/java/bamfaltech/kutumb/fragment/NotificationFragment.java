package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.NotificationModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.viewholder.NotificationViewHolder;

/**
 * Created by gaurav.bansal1 on 29/11/16.
 */

public class NotificationFragment extends BaseKutumbFragment {


    private List<NotificationModel> notificationList;// = Arrays.asList("Aao Raja","Jao Raja","Bhago Raja");
    private View mNotificationItemView;

    private static final String TAG = "NotificationFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<NotificationModel, NotificationViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private ProgressBar mProgressBar;

    private TextView mNoNotif;

    private Activity activity;

    public NotificationFragment() {}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);
        activity = getActivity();
        mContext = getActivity();
        Toolbar mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);
        mNoNotif = (TextView) rootView.findViewById(R.id.no_people);


        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(mRecycler); //set swipe to recylcerview

        return rootView;
    }

    private String getPageTitle() {
        return "Notification";
    }

    @Override
    public String getFragmentTitle() {
        return "notificationfragment";
    }

    @Override
    public void onResume() {
        super.onResume();
        NotificationManager notificationManager =
                (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        Query postsQuery = getQuery(mDatabase);
        mAdapter = new FirebaseRecyclerAdapter<NotificationModel, NotificationViewHolder>(NotificationModel.class, R.layout.notification_item_view,
                NotificationViewHolder.class, postsQuery) {

            @Override
            public int getItemCount() {
                mProgressBar.setVisibility(View.GONE);
                int count = super.getItemCount();
                if(count == 0) {
                    mNoNotif.setVisibility(View.VISIBLE);
                } else {
                    mNoNotif.setVisibility(View.GONE);
                }
                return count;
            }

            @Override
            protected void populateViewHolder(NotificationViewHolder viewHolder, final NotificationModel model, int position) {
                mProgressBar.setVisibility(View.GONE);

                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();

                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToNotification(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        if(model.getNotificationPurpose() == null) {
                            Toast.makeText(activity, "This is only to notify. No further detail attached here...", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(model.getNotificationPurpose().equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_UTILITY_INTEREST)) {
                            UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, model.getNotificationContentKey());
                            utilityDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(utilityDetailFragment);
                        } else if(model.getNotificationPurpose().equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_POST_LIKE)) {
                            PostDetailFragment postDetailFragment = new PostDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, model.getNotificationContentKey());
                            postDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(postDetailFragment);
                        } else if(model.getNotificationPurpose().equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_NOTICE)) {
                            NoticeFragment noticeFragment = new NoticeFragment();
                            ((KutumbActivity)activity).displayFragment(noticeFragment);
                        } else {
                            Toast.makeText(activity, "This is only to notify. No further detail attached here...", Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query query = databaseReference.child("user-notifications/"+ FirebaseManager.getInstance().getCurrentUser().getId())
                .limitToFirst(100);
        return query;
    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
            final int position = viewHolder.getAdapterPosition(); //get position which is swipe
            final UserModel currentUser = FirebaseManager.getInstance().getCurrentUser();
            final DatabaseReference mNotifRef = mAdapter.getRef(position);

            if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {    //if swipe left

                final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
                builder.setTitle("Delete Notification");
                builder.setMessage("Are you sure, do you want to delete the notification ?");

                String positiveText = "yes";
                builder.setPositiveButton(positiveText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // positive button logic
                                FirebaseManager.getInstance().removeNodeById(mDatabase, "user-notifications", currentUser.getId(), mNotifRef.getKey());
                                Toast.makeText(mContext, "Notification has been deleted !", Toast.LENGTH_LONG).show();
                            }
                        });

                String negativeText = "no";
                builder.setNegativeButton(negativeText,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // negative button logic
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                AlertDialog dialog = builder.create();
                // display dialog
                dialog.show();
            }
        }
    };

}
