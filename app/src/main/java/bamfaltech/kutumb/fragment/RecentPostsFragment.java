package bamfaltech.kutumb.fragment;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.UserModel;

public class RecentPostsFragment extends PostListFragment {

    public RecentPostsFragment() {}

    @Override
    public Query getQuery(DatabaseReference databaseReference) {
        // [START recent_posts_query]
        // Last 100 posts, these are automatically the 100 most recent
        // due to sorting by push() keys
        UserModel currentUser = FirebaseManager.getInstance().getCurrentUser();
        RWADetailModel.RWA rwa = currentUser.getRwa();
        Query recentPostsQuery = databaseReference.child("posts/")
                .limitToFirst(100);
        if(rwa != null) {
            String societyKey = rwa.getRwaid();
            recentPostsQuery = databaseReference.child("society-posts/"+societyKey)
                    .limitToFirst(100);
        }
        /*Query recentPostsQuery = databaseReference.child("society-posts/")
                .limitToFirst(100);*/
        // [END recent_posts_query]

        return recentPostsQuery;
    }
}
