package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.HttpManager;
import bamfaltech.kutumb.models.ChatModel;
import bamfaltech.kutumb.models.FileModel;
import bamfaltech.kutumb.models.NoticeModel;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.CircleTransform;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 01/03/17.
 */

public class NewNoticeFragment extends BaseKutumbFragment implements View.OnClickListener {

    private static final String TAG = "NewNoticeFragment";
    private static final String REQUIRED = "Required";

    // [START declare_database_ref]
    private DatabaseReference mDatabase;
    // [END declare_database_ref]

    private EditText mTitleField;
    private ImageView mAuthorImage;
    private TextView mAuthorName;
    private TextView mAuthorFlatNo;
    private EditText mBodyField;

    private TextView gallery;
    private TextView camera;

    private FloatingActionButton mSubmitButton;
    private boolean isTopicPost = false;
    private String postTitle = "";

    private Activity activity;

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;

    private File filePathImageCamera;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    private ProgressBar mProgressBar;
    private TextView newPostText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.fragment_new_notice, container, false);
        mContext = getActivity();
        activity = getActivity();
        init(mView);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END initialize_database_ref]

        mAuthorImage = (ImageView) mView.findViewById(R.id.post_author_photo);
        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
        Glide.with(this).load(userModel.getPhoto_profile())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(activity))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mAuthorImage);
        mTitleField = (EditText) mView.findViewById(R.id.field_title);

        mAuthorName = (TextView) mView.findViewById(R.id.post_author);
        mAuthorName.setText(Util.getAllCapitalWordsString(userModel.getName()));

        mAuthorFlatNo = (TextView) mView.findViewById(R.id.post_author_flat_no);
        mAuthorFlatNo.setText(userModel.getFlatNumber());

        /*newPostText = (TextView) mView.findViewById(R.id.post_topic);
        newPostText.setOnClickListener(this);*/

        mBodyField = (EditText) mView.findViewById(R.id.field_body);
        mSubmitButton = (FloatingActionButton) mView.findViewById(R.id.fab_submit_post);

        gallery = (TextView) mView.findViewById(R.id.photoGallery);
        camera = (TextView) mView.findViewById(R.id.photoCamera);

        gallery.setOnClickListener(this);
        camera.setOnClickListener(this);

        mProgressBar = (ProgressBar) mView.findViewById(R.id.progressbar);

        /*mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });*/

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getUserVisibleHint()) {
            setCurrentFragment(this);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser) {
            setCurrentFragment(this);
        }
    }

    @Override
    public String getFragmentTitle() {
        return "newpost";
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "New Notice";
    }

    /*FirebaseAuth mFirebaseAuth;
    FirebaseUser mFirebaseUser;
    private FirebaseUser getUserIfLogin(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        *//*if(mFirebaseUser != null){
            return mFirebaseUser.getPhotoUrl().toString();
        }*//*
        return mFirebaseUser;
    }*/

    public void submitTopic() {

    }

    public void submitNotice() {
        //final String title = mTitleField.getText().toString();
        final String body = mBodyField.getText().toString();
        if(isTopicPost) {
            postTitle = mTitleField.getText().toString();
            if(TextUtils.isEmpty(postTitle)) {
                Toast.makeText(mContext, "Topic can not be blank", Toast.LENGTH_LONG).show();
                return;
            }
        }

        // Title is required
        /*if (TextUtils.isEmpty(title)) {
            mTitleField.setError(REQUIRED);
            return;
        }*/

        // Body is required
        if (TextUtils.isEmpty(body)) {
            mBodyField.setError(REQUIRED);
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);
        Toast.makeText(activity, "Posting...", Toast.LENGTH_SHORT).show();

        // [START single_value_read]
        final String userId = getUid();
        mDatabase.child("users").child(userId).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        UserModel user = dataSnapshot.getValue(UserModel.class);

                        // [START_EXCLUDE]
                        if (user == null) {
                            // User is null, error out
                            Log.e(TAG, "User " + userId + " is unexpectedly null");
                            Toast.makeText(activity,
                                    "Error: could not fetch user.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            // Write new post
                            writeNewNotice(postTitle, body);
                            /*if(isTopicPost) {
                                writeNewPost(userId, user.getName(), user.getFlatNumber(), user.getPhoto_profile(), postTitle, body);
                                writeNewTopic(userId, postTitle);
                            } else {
                                writeNewPost(userId, user.getName(), user.getFlatNumber(), user.getPhoto_profile(), body);
                            }*/
                        }

                        // Finish this Activity, back to the stream
                        ((KutumbActivity)activity).removeFragment();
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w(TAG, "getUser:onCancelled", databaseError.toException());
                        // [START_EXCLUDE]
                        setEditingEnabled(true);
                        // [END_EXCLUDE]
                    }
                });
        // [END single_value_read]
    }

    private void setEditingEnabled(boolean enabled) {
        //mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    // [START write_fan_out]
    private void writeNewPost(String userId, String username, String userFlat, String userPhoto, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(userId, username, userFlat, body);
        post.setPhoto_profile(userPhoto);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewPost(String userId, String username, String userFlat, String userPhoto, String title, String body) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("posts").push().getKey();
        Post post = new Post(userId, username, userFlat, title, body);
        post.setPhoto_profile(userPhoto);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void writeNewTopic(String userId, String postTitle) {
        String key = mDatabase.child("topics").push().getKey();
        TopicModel topicModel = new TopicModel(userId, postTitle);
        Map<String, Object> topicValues = topicModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/topics/" + key, topicValues);
        childUpdates.put("/user-topics/" + userId + "/" + key, topicValues);

        mDatabase.updateChildren(childUpdates);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST){
            if (resultCode == activity.RESULT_OK){
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null){
                    sendFileFirebase(storageRef,selectedImageUri);
                }else{
                    //URI IS NULL
                }
            }
        }else if (requestCode == IMAGE_CAMERA_REQUEST){
            if (resultCode == activity.RESULT_OK){
                if (filePathImageCamera != null && filePathImageCamera.exists()){
                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName()+"_camera");
                    sendFileFirebase(imageCameraRef,filePathImageCamera);
                }else{
                    //IS NULL
                }
            }
        }
    }

    private void writeNewNotice(String title, String body) {
        String key = mDatabase.child("notice").push().getKey();
        NoticeModel noticeModel = new NoticeModel(FirebaseManager.getInstance().getCurrentUser(), title, body, mFileModel);
        Map<String, Object> noticeValues = noticeModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        //childUpdates.put("/notice/" + key, noticeValues);

        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();
        RWADetailModel.RWA userRWA = userModel.getRwa();
        if(userRWA != null) {
            String societyKey = userRWA.getRwaid();
            childUpdates.put("/notice/"+societyKey+"/"+key, noticeValues);
        }

        sendMessage("New Circular uploaded", "all", key);
        mDatabase.updateChildren(childUpdates);
    }

    public void sendMessage(final String messageToSend, final String frontUserFCMRegID, String postKey) {
        if (messageToSend.length() > 0) {
            Log.i(TAG, "sendMessage");
            HttpManager.getInstance().sendMessage(messageToSend, frontUserFCMRegID, Constants.NOTIFICATION_PURPOSE_NOTICE, postKey);
        }
    }

    FileModel mFileModel = null;

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file){
        mProgressBar.setVisibility(View.VISIBLE);
        FirebaseManager.getInstance().sendFileFirebase(storageReference, file, new Interfaces.OnUploadFileCompleteListener() {
            @Override
            public void onUploadCompleted(FileModel fileModel) {
                mFileModel = fileModel;
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                Log.e(TAG,"onFailure sendFileFirebase "+error);
                mProgressBar.setVisibility(View.GONE);
            }
        });


            /*mChatFirebaseAdapter.setFileSendingInProgress(true);
            Drawable drawableInProgress = getResources().getDrawable(R.drawable.default_user_image);
            try {
                InputStream inputStream = getContentResolver().openInputStream(file);
                drawableInProgress= Drawable.createFromStream(inputStream, file.toString() );
            } catch (FileNotFoundException e) {
                drawableInProgress = getResources().getDrawable(R.drawable.default_user_image);
            }
            mChatFirebaseAdapter.setFileInProgress(drawableInProgress);
            mChatFirebaseAdapter.notifyDataSetChanged();*/
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final File file){
        mProgressBar.setVisibility(View.VISIBLE);
        FirebaseManager.getInstance().sendFileFirebase(storageReference, file, new Interfaces.OnUploadFileCompleteListener() {
            @Override
            public void onUploadCompleted(FileModel fileModel) {
                mProgressBar.setVisibility(View.GONE);
                mFileModel = fileModel;
            }

            @Override
            public void onError(String error) {
                Log.e(TAG,"onFailure sendFileFirebase "+error);
                mProgressBar.setVisibility(View.GONE);

            }
        });
    }

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent(){
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto+"camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        it.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(filePathImageCamera));
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.post_topic:
                if(!isTopicPost) {
                    isTopicPost = true;
                    mTitleField.setVisibility(View.VISIBLE);
                    Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.ic_clear_black_24dp);
                    newPostText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                    //newPostText.setCompoundDrawables(drawable, drawable, drawable, drawable);
                } else {
                    isTopicPost = false;
                    mTitleField.setVisibility(View.GONE);
                    Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.ic_add_circle_black_24dp);
                    newPostText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
                break;
            case R.id.photoGallery:
                photoGalleryIntent();
                break;
            case R.id.photoCamera:
                photoCameraIntent();
                break;
        }
    }
    // [END write_fan_out]
}
