package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class AboutusFragment extends BaseKutumbFragment {

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.about_us, container, false);
        activity = getActivity();
        init(mView);
        TextView textView = (TextView) mView.findViewById(R.id.about_us_text);
        textView.setText("Introducing Kutumub, to instantly reach the people in your society! Share Photos, Send \n" +
                "Messages and get updates of your Society.\n" +
                "\n" +
                "1. Create and find Groups(In Utility Section) based on interests\n" +
                "2. Instantly search your society-mates\n" +
                "3. Post and Search notices/Circulars issued by Society\n" +
                "4. chat 1-on-1 with Kutumb Family, \n" +
                "5. Express yourself through Text and photos\n" +
                "6. Check out for the upcoming events\n" +
                "7. Get notified when your friends connects with you\n" +
                "8. Create Disucssion forum & provide suggestion");
        return mView;
    }

    @Override
    public String getFragmentTitle() {
        return "aboutusfragment";
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "About Us";
    }
}
