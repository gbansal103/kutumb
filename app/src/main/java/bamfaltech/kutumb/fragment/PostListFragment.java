package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.HttpManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.viewholder.PostViewHolder;

public abstract class PostListFragment extends BaseKutumbFragment implements View.OnClickListener{

    private static final String TAG = "PostListFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private Activity activity;


    private View headerView;

    public PostListFragment() {}

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_all_posts, container, false);
        mContext = getActivity();
        activity = getActivity();
        mToolbar = (Toolbar) rootView.findViewById(R.id.main_toolbar);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);

        View actionBarView = inflater.inflate(R.layout.action_details, container, false);
        mToolbar.addView(actionBarView);


        ImageView currenrUserPic = (ImageView) rootView.findViewById(R.id.current_user_pic);
        Util.bindCircularImage(currenrUserPic, FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());

        rootView.findViewById(R.id.top_layout).setOnClickListener(this);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.top_layout:
                NewPostFragment fragment = new NewPostFragment();
                ((KutumbActivity)activity).displayFragment(fragment);
                break;
        }
    }

    private static final int VIEW_TYPE_HEADER = -1;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        Query postsQuery = getQuery(mDatabase);
        mAdapter = new FirebaseRecyclerAdapter<Post, PostViewHolder>(Post.class, R.layout.item_post,
                PostViewHolder.class, postsQuery) {

            @Override
            public int getItemCount() {
                int itemCount = super.getItemCount();
                itemCount = itemCount + 1;
                return itemCount;
            }

            @Override
            public Post getItem(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItem(position);
                } else {
                    Post post = new Post();
                    post.setPhoto_profile(FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());
                    post.setAuthor("What do you want to share ?");
                    return post;
                }
            }

            @Override
            public DatabaseReference getRef(int position) {
                if(position < getItemCount() - 1) {
                    return super.getRef(position);
                } else {
                    return null;
                }
            }

            @Override
            public int getItemViewType(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemViewType(position);
                } else {
                    return 1;
                }
            }

            @Override
            public long getItemId(int position) {
                if(position < getItemCount() - 1) {
                    return super.getItemId(position);
                } else {
                    return -1;
                }
            }

            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder, final Post model, final int position) {
                mProgressBar.setVisibility(View.GONE);
                if (position == getItemCount() - 1) {
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Launch PostDetailFragment
                            NewPostFragment fragment = new NewPostFragment();
                            ((KutumbActivity)activity).displayFragment(fragment);
                        }
                    });
                    viewHolder.bindToHeaderPost(model, PostListFragment.this);

                } else {
                    final DatabaseReference postRef = getRef(position);

                    // Set click listener for the whole post view
                    final String postKey = postRef.getKey();
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // Launch PostDetailFragment
                            PostDetailFragment postDetailFragment = new PostDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                            postDetailFragment.setArguments(bundle);
                            ((KutumbActivity)activity).displayFragment(postDetailFragment);
                        }
                    });

                    // Determine if the current user has liked this post and set UI accordingly
                    if (model.stars.containsKey(getUid())) {
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                        viewHolder.mLikeImage.setImageResource(R.drawable.heart_red);
                    } else {
                        viewHolder.mLikeText.setTextColor(getResources().getColor(android.R.color.black));
                        viewHolder.mLikeImage.setImageResource(R.drawable.ic_favorite_black_24dp);
                    }

                    // Bind Post to ViewHolder, setting OnClickListener for the star button
                    viewHolder.bindToPost(postRef, model, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            FirebaseManager.getInstance().getFrontUserInfoFromFirebase(model.uid, new Interfaces.OnUserModelRetrieved() {
                                @Override
                                public void onRetreivalComplete(UserModel userModel) {
                                    UserProfileFragment userProfileFragment = new UserProfileFragment();
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("userModel", userModel);
                                    userProfileFragment.setArguments(bundle);
                                    ((KutumbActivity)activity).displayFragment(userProfileFragment);
                                }

                                @Override
                                public void onError(String errormsg) {

                                }
                            });

                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View starView) {
                            // Need to write to both places the post is stored
                            DatabaseReference globalPostRef = mDatabase.child("posts").child(postRef.getKey());
                            DatabaseReference userPostRef = mDatabase.child("user-posts").child(model.uid).child(postRef.getKey());
                            DatabaseReference societyPostRef = mDatabase.child("society-posts").child(KutumbApplication.getInstance().getCurrentRWA().getRwaid()).child(postRef.getKey());

                            // Run two transactions
                            onStarClickedWithExtra(globalPostRef);
                            onStarClicked(userPostRef);
                            onStarClicked(societyPostRef);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            PostDetailFragment postDetailFragment = new PostDetailFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                            postDetailFragment.setArguments(bundle);
                            ((KutumbActivity) activity).displayFragment(postDetailFragment);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TopicFragment topicFragment = new TopicFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("topic", model.title);
                            bundle.putInt("type",Constants.TOPIC_LIST_TYPE_POST);
                            topicFragment.setArguments(bundle);
                            ((KutumbActivity) activity).displayFragment(topicFragment);
                        }
                    }, PostListFragment.this);
                }
            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    // [START post_stars_transaction]
    private void onStarClickedWithExtra(final DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                    String frontUserID = p.uid;
                    if(!frontUserID.equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
                        FirebaseManager.getInstance().getFrontUserInfoFromFirebase(frontUserID, new Interfaces.OnUserModelRetrieved() {
                            @Override
                            public void onRetreivalComplete(UserModel userModel) {
                                UserModel senderUserModel = FirebaseManager.getInstance().getCurrentUser();
                                sendMessage(senderUserModel.getName() + ", " + senderUserModel.getFlatNumber() + " liked your post", userModel.getFCMRegID(), postRef.getKey());
                            }

                            @Override
                            public void onError(String errormsg) {

                            }
                        });
                    }
                }
                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }
    // [END post_stars_transaction]

    // [START post_stars_transaction]
    private void onStarClicked(final DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                    String frontUserID = p.uid;
                }
                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }
    // [END post_stars_transaction]

    public void sendMessage(final String messageToSend, final String frontUserFCMRegID, String postKey) {

        if (messageToSend.length() > 0) {

            Log.i(TAG, "sendMessage");

            HttpManager.getInstance().sendMessage(messageToSend, frontUserFCMRegID, Constants.NOTIFICATION_PURPOSE_POST_LIKE, postKey);

        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public abstract Query getQuery(DatabaseReference databaseReference);

    public static class DetailListingItemHolder extends RecyclerView.ViewHolder {

        public DetailListingItemHolder(View itemView) {
            super(itemView);
        }
    }

}
