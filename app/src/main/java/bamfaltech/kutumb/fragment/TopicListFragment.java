package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.viewholder.TopicViewHolder;

/**
 * Created by gaurav.bansal1 on 01/03/17.
 */

public class TopicListFragment extends BaseKutumbFragment implements View.OnClickListener{
    private static final String TAG = "TopicListFragment";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<TopicModel, TopicViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    //private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.topic_list_fragment, container, false);
        activity = getActivity();


        /*ImageView currenrUserPic = (ImageView) rootView.findViewById(R.id.current_user_pic);
        Util.bindCircularImage(currenrUserPic, FirebaseManager.getInstance().getCurrentUser().getPhoto_profile());*/

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressbar);

        // [START create_database_reference]
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // [END create_database_reference]

        mRecycler = (RecyclerView) rootView.findViewById(R.id.messages_list);
        mRecycler.setHasFixedSize(true);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Set up Layout Manager, reverse layout
        mManager = new LinearLayoutManager(getActivity());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);

        // Set up FirebaseRecyclerAdapter with the Query
        mProgressBar.setVisibility(View.VISIBLE);

        Query postsQuery = getQuery(mDatabase);

        mAdapter = new FirebaseRecyclerAdapter<TopicModel, TopicViewHolder>(TopicModel.class, R.layout.notification_item_view,
                TopicViewHolder.class, postsQuery) {
            @Override
            protected void populateViewHolder(TopicViewHolder viewHolder, final TopicModel model, int position) {
                final DatabaseReference postRef = getRef(position);

                // Set click listener for the whole post view
                final String postKey = postRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Launch PostDetailFragment
                        PostDetailFragment postDetailFragment = new PostDetailFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(PostDetailFragment.EXTRA_POST_KEY, postKey);
                        postDetailFragment.setArguments(bundle);
                        ((KutumbActivity)mContext).displayFragment(postDetailFragment);
                    }
                });


                // Bind Post to ViewHolder, setting OnClickListener for the star button
                viewHolder.bindToTopic(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        TopicFragment topicFragment = new TopicFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("topic", model.getTopic());
                        bundle.putInt("type", Constants.TOPIC_LIST_TYPE_POST);
                        topicFragment.setArguments(bundle);
                        ((KutumbActivity)activity).displayFragment(topicFragment);
                    }
                });
                mProgressBar.setVisibility(View.GONE);
            }
        };
        mRecycler.setAdapter(mAdapter);
    }

    public Query getQuery(DatabaseReference databaseReference) {
        Query query = databaseReference.child("topics")
                .limitToFirst(100);
        return query;
    }

    @Override
    public void onClick(View view) {

    }
}
