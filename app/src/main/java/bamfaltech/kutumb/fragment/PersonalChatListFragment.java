package bamfaltech.kutumb.fragment;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.adapter.CustomListAdapter;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.models.PeopleObject;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.view.LoginActivity;
import bamfaltech.kutumb.viewholder.FlatMemberHolder;
import bamfaltech.kutumb.viewholder.PostViewHolder;

/**
 * Created by gaurav.bansal1 on 06/02/17.
 */

public class PersonalChatListFragment extends Fragment implements CustomListAdapter.IAddListItemView, View.OnClickListener {

    private static final String TAG = "PersonalChatList";

    // [START define_database_reference]
    private DatabaseReference mDatabase;
    // [END define_database_reference]

    private FirebaseRecyclerAdapter<Post, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private TextView mNoPeple;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    //CLass Model
    private UserModel userModel;
    private Activity activity;
    private CustomListAdapter mCustomListAdapter;

    List<PeopleObject.People> peopleObjList;

    View parent = null;
    boolean fragmentCreated = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        if(parent == null) {
            fragmentCreated = true;
            parent = inflater.inflate(R.layout.fragment_personal_chat_list, container, false);


            mToolbar = (Toolbar) parent.findViewById(R.id.main_toolbar);

            mProgressBar = (ProgressBar) parent.findViewById(R.id.progressbar);
            mNoPeple = (TextView) parent.findViewById(R.id.no_people);

            View actionBarView = inflater.inflate(R.layout.action_details, container, false);
            mToolbar.addView(actionBarView);


            // [START create_database_reference]
            mDatabase = FirebaseDatabase.getInstance().getReference();
            // [END create_database_reference]

            mRecycler = (RecyclerView) parent.findViewById(R.id.messages_list);
            mRecycler.setHasFixedSize(true);
        } else {
            fragmentCreated = false;
        }
        registerChatReceiver();

        return parent;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unRegisterReceiver();
    }

    public void update() {
        userModel = FirebaseManager.getInstance().getCurrentUser();//new UserModel(mFirebaseUser.getDisplayName(), mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid(), mFirebaseUser.getEmail());
        //readFirebaseMessages();
        getPeopleList(userModel.getId(), true);
    }

    /**
     * Verificar se usuario está logado
     */
    private void verifyUserIfLogin(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null){
            startActivity(new Intent(activity, LoginActivity.class));
            activity.finish();
        }else{
            userModel = new UserModel(mFirebaseUser.getDisplayName(), mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid(), mFirebaseUser.getEmail());
            //readFirebaseMessages();
            if(fragmentCreated) {
                mProgressBar.setVisibility(View.VISIBLE);
                getPeopleList(userModel.getId(), false);
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(fragmentCreated) {
            // Set up Layout Manager, reverse layout
            mManager = new LinearLayoutManager(getActivity());
            mManager.setReverseLayout(true);
            mManager.setStackFromEnd(true);
            mRecycler.setLayoutManager(mManager);
            // Set up FirebaseRecyclerAdapter with the Query
            verifyUserIfLogin();
        } else {
            update();
        }
    }

    BroadcastReceiver chatUpdateBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            update();
        }
    };

    public void registerChatReceiver(){
        activity.registerReceiver(chatUpdateBroadcastReceiver,
                new IntentFilter("bamfaltech.kutumb.chatUpdateReceiver"));
    }

    @Override
    public void onResume() {
        super.onResume();
        NotificationManager notificationManager =
                (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        update();
    }

    public void unRegisterReceiver() {
        activity.unregisterReceiver(chatUpdateBroadcastReceiver);
    }


    void getPeopleList(final String userid, final boolean isUpdate) {
        //Util.showPB(activity, "Getting People to Chat...");

        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("userid", userid);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(PeopleObject.class);
        urlManager.setPriority(Request.Priority.HIGH);
        urlManager.setCachable(true);
        boolean isChatListRefereshNeeded = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_UPDATE_CHAT_LIST, false, true);
        /*if(isUpdate) {
            urlManager.setDataRefreshStatus(true);
        } else if(isChatListRefereshNeeded) {
            urlManager.setDataRefreshStatus(true);
            DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_UPDATE_CHAT_LIST, false, true);
        }*/
        if(!Util.hasInternetAccess(KutumbApplication.getContext())) {
            urlManager.setDataRefreshStatus(false);
        } else {
            urlManager.setDataRefreshStatus(true);
        }
        urlManager.setMethod(Request.Method.GET);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.GET_PERSONAL_CHAT_RECORD_LIST);


        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                PeopleObject peopleObject = (PeopleObject) object;
                if(peopleObject != null) {
                    mNoPeple.setVisibility(View.GONE);
                    peopleObjList = (ArrayList<PeopleObject.People>) peopleObject.getArrListBusinessObj();
                    if (peopleObjList.size() > 0) {
                        boolean isChatListRefereshNeeded = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_UPDATE_CHAT_LIST, false, true);
                        if (isUpdate || isChatListRefereshNeeded) {
                            mCustomListAdapter.updateAdapterCount(peopleObjList.size());
                            mCustomListAdapter.notifyDataSetChanged();
                            DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_UPDATE_CHAT_LIST, false, true);
                        } else {
                            mProgressBar.setVisibility(View.GONE);

                            mCustomListAdapter = new CustomListAdapter(activity);
                            mCustomListAdapter.setParamaters(peopleObjList.size(), PersonalChatListFragment.this);
                            mRecycler.setAdapter(mCustomListAdapter);

                        }
                    } else {
                        mNoPeple.setVisibility(View.VISIBLE);
                    }
                } else {
                    mNoPeple.setVisibility(View.VISIBLE);
                    //Toast.makeText(activity, "No people", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError(String errorMsg) {
                mProgressBar.setVisibility(View.GONE);
                if(mAdapter != null && mAdapter.getItemCount() <= 0) {
                    mNoPeple.setVisibility(View.VISIBLE);
                }
                //Toast.makeText(activity, "Some Problem has been occured", Toast.LENGTH_SHORT).show();
            }
        }, urlManager);
    }


    /*@Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PeopleObject p = peopleObjList.get(arg2);
        String chattingToName = p.getPersonName();
        String chattingToDeviceID = p.getRegId();

        Intent intent = new Intent(ListPeople.this, ChatActivity.class);
        intent.putExtra("chattingFrom",
                utils.getFromPreferences(Utils.UserName));
        intent.putExtra("chattingToName", chattingToName);
        intent.putExtra("chattingToDeviceID", chattingToDeviceID);
        startActivity(intent);
    }*/

    @Override
    public void addListItemView(final int position, RecyclerView.ViewHolder pConvertView, ViewGroup parentView) {
        ((FlatMemberHolder)pConvertView).bindToChatList(peopleObjList.get(position), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String chatName = peopleObjList.get(position).getChatName();
                String chatReceiver = peopleObjList.get(position).getReceiverId();
                String chatIds[] = chatName.split("_");
                String frontUserID = "";
                if(chatIds[0].equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    frontUserID = chatIds[1];
                } else {
                    frontUserID = chatIds[0];
                }

                if(!TextUtils.isEmpty(chatName) && !TextUtils.isEmpty(chatReceiver) && chatReceiver.equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                    updateChatUser(chatName, "0", peopleObjList.get(position), position);
                }

                Intent intent = new Intent(getActivity(), StartChatActivity.class);
                intent.putExtra("front_user_id", frontUserID);
                intent.putExtra("chat_name", chatName);
                intent.putExtra("chat_receiver", chatReceiver);
                startActivity(intent);
            }
        });
    }

    private void updateChatUser(String room, final String status, final PeopleObject.People people, final int position) {
        people.setChatStatus(status);
        mCustomListAdapter.notifyItemChanged(position);
    }

    @Override
    public void showHideEmtpyView(boolean showEmptyView) {

    }

    @Override
    public RecyclerView.ViewHolder createViewHolder(ViewGroup parent, int viewType) {
        View convertView;
        convertView = LayoutInflater.from(activity).inflate(R.layout.flat_member_item, parent, false);
        return new FlatMemberHolder(convertView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.no_people:
                FlatChooserFragment fragment = new FlatChooserFragment();
                ((KutumbActivity)activity).displayFragment(fragment);
                break;
        }
    }
}
