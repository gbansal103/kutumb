package bamfaltech.kutumb.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class PrivacyPolicyFragment extends BaseKutumbFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View mView = inflater.inflate(R.layout.about_us, container, false);
        init(mView);
        TextView textView = (TextView) mView.findViewById(R.id.about_us_text);
        textView.setText(/*"Privacy Policy\n" +
                "\n" +*/
                        "1. At Kutumb, we recognize that privacy is important. The Policy applies to the Services mentioned in the Terms of Use. Kutumb is a Social Networking site or app (hereinafter referred to as the \"Site\" or “App”).\n" +
                        "\n" +
                        "2. Information we collect and how we use it:\n" +
                        "\n" +
                        "We offer a number of services that do not require you to register for an account or provide any personal information to us, such as browsing public web pages of the Site or App. In order to provide our full range of services, we may collect the following types of information:\n" +
                        "\n" +
                        "2.1 Information you provide‐\n" +
                        "\n" +
                        "2.1.1. When you sign up on Kutumb, we ask you for personal information such as first name, last name, username, time zone, zip/postal code, country, state, city, email, birth date, gender, photo, interests etc. Once you register with Kutumb and sign in to our services, you are not anonymous to us. You will provide with a username and one‐time password to login to the Site or App or you may login through social logins (such as Facebook or Google).\n" +
                        "\n" +
                        "2.1.2. Also during registration, you will be requested to register your email, mobile phone or other communication device to receive messages, text messages, notifications, and other services to your email, mobile phone or other communication device. If you select to register other communication device, you must provide your mobile network operator and your mobile phone number. You must also provide the validation code that Kutumb sends to your email, mobile phone, or other device to ensure that the device you are registering does belong to you.\n" +
                        "\n" +
                        "2.1.3. Some of the information from registration such as country, state, and city of residence, gender or age may be automatically placed in your Profile. You can select to hide this information from other members of Kutumb by editing your Profile. In addition, you can choose to tell other members of Kutumb more about your hobbies, interests, hometown, high school, college, and much more. You can fill out as much or as little as you like. The more you fill out the more likely that you will be able to meet other members based on your common interest.\n" +
                        "\n" +
                        "2.1.4. For certain premium services, we also request credit card or other payment account information which we maintain in encrypted form on secure servers. We may combine the information you submit under your account with information from other Kutumb services or third parties in order to provide you with a better experience and to improve the quality of our services. For certain services, we may give you the opportunity to opt out of combining such information.\n" +
                        "\n" +
                        "2.2 Other Information\n" +
                        "\n" +
                        "2.2.1. Kutumb may also automatically receive and record information on our server logs from your browser including your IP address, Kutumb cookie information and the page you requested.\n" +
                        "\n" +
                        "2.2.2. Your mobile phone number may be used to associate, identify and track content you upload as belonging to you. Kutumb may use the information to manage or improve the Site or App, track usage and to protect the rights or property of our users and of others.\n" +
                        "\n" +
                        "2.2.3. Kutumb cookies ‐ When you visit Kutumb, we send one or more cookies ‐ a small file containing a string of characters ‐ to your computer that uniquely identifies your browser. We use cookies to improve the quality of our service by storing user preferences and tracking user trends, such as how people search. Most browsers are initially set up to accept cookies, but you can reset your browser to refuse all cookies or to indicate when a cookie is being sent. However, some Kutumb features and services may not function properly if your cookies are disabled.\n" +
                        "\n" +
                        "2.2.4. Log information‐ When you use Kutumb services, our servers automatically record information that your browser sends whenever you visit the Site. These server logs may include information such as your web request, Internet Protocol address, browser type, browser language, the date and time of your request and one or more cookies that may uniquely identify your browser.\n" +
                        "\n" +
                        "2.2.5. User communications‐ When you send email or other communication to Kutumb, we may retain those communications in order to process your inquiries, respond to your requests and improve our Services.\n" +
                        "\n" +
                        "2.3 Kutumb collect the above information, which include the sensitive personal data or information, from the users of this Site or App for providing the Services to users and other purpose as mentioned in the Terms of Use. In addition to the above, such purposes include:\n" +
                        "\n" +
                        "2.3.1. Display of the content for advertising;\n" +
                        "\n" +
                        "2.3.2. Auditing, research and analysis in order to maintain, protect and improve our services;\n" +
                        "\n" +
                        "2.3.3. Ensuring the technical functioning of our network; and\n" +
                        "\n" +
                        "2.3.4. Developing new services.\n" +
                        "\n" +
                        "2.4 The information collected shall be used for the purpose for which it has been collected.\n" +
                        "\n" +
                        "2.5 Kutumb will not retain that the information for longer than is required for the purposes for which the information may lawfully be used or is otherwise required under the Terms of Use or any other law for the time being in force.\n" +
                        "\n" +
                        "3. Links ‐ Kutumb may present links in a format that enables us to keep track of whether these links have been followed. We use this information to improve the quality of our search technology, customized content and advertising.\n" +
                        "\n" +
                        "4. Other sites ‐ This Privacy Policy applies to this Site. We do not exercise control over the sites displayed as search results or links from within our various services. These other sites may place their own cookies or other files on your computer, collect data or solicit personal information from you.\n" +
                        "\n" +
                        "5. Choices for personal information\n" +
                        "\n" +
                        "5.1 When you sign up for a particular service that requires registration, we ask you to provide personal information. If we use this information in a manner different than the purpose for which it was collected, then we will ask for your consent prior to such use.\n" +
                        "\n" +
                        "5.2 If we propose to use personal information for any purposes other than those described in this Policy and/or in the specific service notices, we will offer you an effective way to opt out of the use of personal information for those other purposes. We will not collect or use sensitive information for purposes other than those described in this Policy and/or in the specific service notices, unless we have obtained your prior consent.\n" +
                        "\n" +
                        "5.3 You can decline to submit personal information to any of our services, in which case Kutumb may not be able to provide those services to you.\n" +
                        "\n" +
                        "6. Information sharing\n" +
                        "\n" +
                        "6.1 We have your consent. We require opt‐in consent for the sharing of any sensitive personal information.\n" +
                        "\n" +
                        "6.2 We have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to (a) satisfy any applicable law, regulation, legal process or enforceable governmental request, (b) enforce applicable Terms of Service, including investigation of potential violations thereof, (c) detect, prevent, or otherwise address fraud, security or technical issues, or (d) protect against imminent harm to the rights, property or safety of Kutumb, its users or the public as required or permitted by law.\n" +
                        "\n" +
                        "6.3 We have to process a financial transaction to enable your access our paid services that require payment of fees. For example, if you choose to pay us using credit cards, we need to share some information (such as your name and credit card number) with banks and other entities in the financial system that process credit card transactions. In such cases, we will notify you of that sharing before you complete the transaction.\n" +
                        "\n" +
                        "6.4 If Kutumb becomes involved in a merger, acquisition, or any form of sale of some or all of its assets, we will provide notice before personal information is transferred and becomes subject to a different privacy policy.\n" +
                        "\n" +
                        "6.5 We may share with third party certain pieces of aggregated, non‐personal information such as the number of users who searched for a particular term, for example, or how many users clicked on a particular advertisement. Such information does not identify you individually.\n" +
                        "\n" +
                        "6.6 We may disclose the sensitive personal data or information:\n" +
                        "\n" +
                        "6.6.1. to any person, if such disclose is required for a lawful purpose connected with a function or activity of Kutumb.\n" +
                        "\n" +
                        "6.6.2. where the disclosure is necessary for compliance of a legal obligation.\n" +
                        "\n" +
                        "6.7 We have to disclose the sensitive personal data or information with Government agencies mandated under the law to obtain information for the purpose of verification of identity, or for prevention, detection, investigation including cyber incidents, prosecution, and punishment of offences.\n" +
                        "\n" +
                        "7. Information security\n" +
                        "\n" +
                        "7.1 We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, as well as physical security measures to guard against unauthorized access to systems where we store personal data.\n" +
                        "\n" +
                        "7.2 We restrict access to personal information to Kutumb employees, contractors and agents who need to know that information in order to operate, develop or improve our Services. These individuals are bound by confidentiality obligations and may be subject to discipline, including termination and criminal prosecution, if they fail to meet these obligations.\n" +
                        "\n" +
                        "8. Data integrity\n" +
                        "\n" +
                        "Kutumb processes personal information only for the purposes for which it was collected and in accordance with this Policy or any applicable service‐specific privacy notice. We review our data collection, storage and processing practices to ensure that we only collect, store and process the personal information needed to provide or improve our services. We take reasonable steps to ensure that the personal information we process is accurate, complete, and current, but we depend on our users to update or correct their personal information whenever necessary.\n" +
                        "\n" +
                        "9. Accessing and updating personal information\n" +
                        "\n" +
                        "When you use Kutumb services, we make good faith efforts to provide you with access to your personal information and either to correct this data if it is inaccurate or to delete such data at your request if it is not otherwise required to be retained by law or for legitimate business purposes. We ask individual users to identify themselves and the information requested to be accessed, corrected or removed before processing such requests, and we may decline to process requests that are unreasonably repetitive or systematic, require disproportionate technical effort, jeopardize the privacy of others, or would be extremely impractical (for instance, requests concerning information residing on backup tapes), or for which access is not otherwise required. In any case where we provide information access and correction, we perform this service free of charge, except if doing so would require a disproportionate effort.\n" +
                        "\n" +
                        "10. Enforcement\n" +
                        "\n" +
                        "Kutumb regularly reviews its compliance with this Privacy Policy. Please feel free to direct any questions or concerns regarding this Policy or Kutumb's treatment of personal information by contacting us through the Site or App or by writing to us at contact@letsKutumb.com. When we receive formal written complaints at this address, it is Kutumb's policy to contact the complaining user regarding his or her concerns. We will cooperate with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that cannot be resolved between Kutumb and an individual.\n" +
                        "\n" +
                        "11. Changes to this policy\n" +
                        "\n" +
                        "11.1 Please note that this Privacy Policy may change from time to time. We will not reduce your rights under this Policy without your explicit consent, and we expect most such changes will be minor. Regardless, we will post any Policy changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Policy changes). Each version of this Policy will be identified at the top of the page by its effective date, and we will also keep prior version of this Privacy Policy in an archive for your review.\n" +
                        "\n" +
                        "11.2 If you have any additional questions or concerns about this Privacy Policy, please feel free to reach reach out to us at contact@bamfaltech.com or call us on +918527541059.");
        return mView;
    }

    private void init(View view) {
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(getActivity());
        mDetailsMaterialActionBar.setParams(this, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private String getPageTitle() {
        return "Privacy Policy";
    }

    @Override
    public String getFragmentTitle() {
        return "PrivacyPolicyFragment";
    }
}
