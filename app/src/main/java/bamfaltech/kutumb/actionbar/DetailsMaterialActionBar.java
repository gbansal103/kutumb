package bamfaltech.kutumb.actionbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bamfaltech.kutumb.BasicInfoActivity;
import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.MainActivity;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.view.FullScreenImageActivity;
import bamfaltech.kutumb.view.PopupItemView;

public class DetailsMaterialActionBar extends RelativeLayout implements OnClickListener, Toolbar.OnMenuItemClickListener {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Toolbar mToolbar;
    private Fragment mFragment;
    private String actionBarTitle;
    private String actionBarArtwork;

    public DetailsMaterialActionBar(Context context) {
        this(context, null);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mLayoutInflater.inflate(R.layout.action_details, this);
        findViewById(R.id.menu_icon).setOnClickListener(this);
    }

    public DetailsMaterialActionBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mLayoutInflater.inflate(R.layout.action_details, this);
    }

    public DetailsMaterialActionBar(Context context, AttributeSet attributeSet, int defStyle) {
        super(context, attributeSet, defStyle);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        mLayoutInflater.inflate(R.layout.action_details, this);
    }

    public void setParams(Fragment fragment, String title) {
        setParams(fragment, title, null);
    }

    public void setParams(Fragment fragment, String title, String actionBarArtwork) {
        mFragment = fragment;
        actionBarTitle = title;
        this.actionBarArtwork = actionBarArtwork;
        initActionBarViews();
    }


    private void initActionBarViews() {
        TextView textView = (TextView) findViewById(R.id.title);
        textView.setText(actionBarTitle);
        ImageView imageView = (ImageView) findViewById(R.id.actionbar_img);
        if(!TextUtils.isEmpty(actionBarArtwork)) {
            imageView.setVisibility(VISIBLE);
            Util.bindCircularImage(imageView, actionBarArtwork);
        } else {
            imageView.setVisibility(GONE);
        }
        findViewById(R.id.menu_icon).setOnClickListener(this);
        findViewById(R.id.menu_icon).setVisibility(VISIBLE);

    }

    public TextView getTitleTextView() {
        return (TextView) findViewById(R.id.title);
    }

    public void setToolbar(Toolbar toolbar) {
        mToolbar = toolbar;
        toolbar.setOnMenuItemClickListener(this);
        Menu lMenu = toolbar.getMenu();

    }

    public void showContextMenu(boolean isContextmode) {
        Menu lMenu = null;
        if (mToolbar != null) {
            lMenu = mToolbar.getMenu();
        }
        /*if (isContextmode) {
            if (lMenu != null) {
                lMenu.setGroupVisible(R.id.cast_menu_detail, false);
            }
            findViewById(R.id.menu_icon).setVisibility(GONE);
            findViewById(R.id.action_details).setVisibility(GONE);
        } else {
            if (lMenu != null) {
                lMenu.setGroupVisible(R.id.cast_menu_detail, true);
                if (mBusinessObject != null && mBusinessObject.isLocalMedia()) {
                    lMenu.findItem(R.id.menu_favourite).setVisible(false);
                    lMenu.findItem(R.id.menu_download).setVisible(false);
                }
            }

            findViewById(R.id.menu_icon).setVisibility(VISIBLE);
            findViewById(R.id.action_details).setVisibility(VISIBLE);
        }
        super.showContextMenu(isContextmode);*/
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_icon:
                if(mContext instanceof BasicInfoActivity) {
                    boolean isProfilePageCompleted = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_COMPLETE_USER_PROFILE, false, true);
                    if(isProfilePageCompleted) {
                        /*Intent intent = new Intent(mContext, KutumbActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                        mContext.startActivity(intent);*/
                        ((BasicInfoActivity)mContext).finish();
                    } else {
                        ((BasicInfoActivity)mContext).performDoubleExit();
                    }
                } else if(mContext instanceof StartChatActivity) {
                    ((StartChatActivity)mContext).finish();
                } else if(mContext instanceof FullScreenImageActivity) {
                    ((FullScreenImageActivity)mContext).finish();
                } else if(mContext instanceof MainActivity) {
                    ((MainActivity) mContext).onBackPressHandling();
                } else {
                    ((KutumbActivity) mContext).homeIconClick(false);
                }
                break;
        }
    }


    public void onClickMenu(int id) {
        switch (id) {
            case R.id.menu_icon:
                ((KutumbActivity) mContext).homeIconClick(false);
                break;
            case R.id.menu_option:
                PopupItemView popupItemView = new PopupItemView(mContext);
                popupItemView.populateChatPopupMenu();
                popupItemView.show();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        onClickMenu(item.getItemId());
        return false;
    }
}