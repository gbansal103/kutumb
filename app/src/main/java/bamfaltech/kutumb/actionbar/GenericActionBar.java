package bamfaltech.kutumb.actionbar;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.fragment.NotificationFragment;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.fragment.SearchTabFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.view.PopupItemView;

/**
 * Created by gaurav.bansal1 on 15/02/17.
 */

public class GenericActionBar extends LinearLayout implements View.OnClickListener, Toolbar.OnMenuItemClickListener, Interfaces.NotificationChangedListener{

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private ImageView searchView;
    private MenuItem deviceIcon;
    private MenuItem deviceConnected;
    private TextView titleTextView;
    private TextView mNotificationBubble = null;
//    private Drawable list_selector;


    public GenericActionBar(Context context, boolean isHomeFragment) {
        super(context);
        initialiseActionBar(context, null, isHomeFragment);
    }

    public GenericActionBar(Context context, String title, boolean isHomeFragment) {
        super(context);
        mContext = context;
        initialiseActionBar(context, title, isHomeFragment);
    }


    private void initialiseActionBar(Context context, String title, boolean isHomeFragment) {
        mContext = context;

        mLayoutInflater = LayoutInflater.from(context);
        mLayoutInflater.inflate(R.layout.actionbar_generic, this);
        findViewById(R.id.menu_icon).setOnClickListener(this);
        titleTextView = (TextView) findViewById(R.id.action_title);
        FirebaseManager.getInstance().setNotificationListener(this);

        if (TextUtils.isEmpty(title)) {
            titleTextView.setVisibility(View.GONE);
        } else {
            titleTextView.setVisibility(View.INVISIBLE);
        }

    }

    private void backClick() {
        findViewById(R.id.menu_icon).setVisibility(View.VISIBLE);
        ((ImageView) findViewById(R.id.menu_icon)).setImageResource(R.drawable.actionbar_menu);
        setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }


    public void setToolbar(Toolbar toolbar) {
        View view = toolbar.getMenu().findItem(R.id.notif_actionbar).getActionView();
        mNotificationBubble = (TextView) view.findViewById(R.id.notification_bubble);
        view.setOnClickListener(this);
        /*View menuView = toolbar.getMenu().findItem(R.id.menu_option).getActionView();
        menuView.setOnClickListener(this);*/
        toolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_icon:
                ((KutumbActivity) mContext).homeIconClick(true);
                break;
            case R.id.rlParentBubble:
            case R.id.notification_bubble:
            case R.id.notification_image:
                mNotificationBubble.setVisibility(View.GONE);
                NotificationFragment notificationFragment = new NotificationFragment();
                ((KutumbActivity)mContext).displayFragment(notificationFragment);
                break;
            case R.id.menu_option:
                PopupItemView popupItemView = new PopupItemView(mContext);
                popupItemView.populatePopupMenu();
                popupItemView.show();
                break;
            default:
                break;
        }
    }


    public void onClickMenu(int id) {
        switch (id) {
            case R.id.menu_option:
                PopupItemView popupItemView = new PopupItemView(mContext);
                popupItemView.populatePopupMenu();
                popupItemView.show();
                break;
            case R.id.menu_icon:
                ((KutumbActivity) mContext).homeIconClick(true);
                break;

            case R.id.search_actionbar:
                SearchTabFragment searchTabFragment = new SearchTabFragment();
                ((KutumbActivity)mContext).displayFragment(searchTabFragment);
                break;

            case R.id.notif_actionbar:
                mNotificationBubble.setVisibility(View.GONE);
                NotificationFragment notificationFragment = new NotificationFragment();
                ((KutumbActivity)mContext).displayFragment(notificationFragment);
                break;
            default:
                break;
        }
    }


    public boolean onMenuItemClick(MenuItem item) {

        onClickMenu(item.getItemId());
        return false;
    }

    @Override
    public void onNotificationChanged(final int newNotificationCount) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mNotificationBubble != null && newNotificationCount > 0) {
                    mNotificationBubble.setVisibility(View.VISIBLE);
                    //mNotificationBubble.setText(String.valueOf(newNotificationCount));
                }
            }
        });
    }
}
