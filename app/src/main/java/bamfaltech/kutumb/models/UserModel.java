package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alessandro Barreto on 22/06/2016.
 */
@IgnoreExtraProperties
public class UserModel implements Serializable {

    private String id;
    private String name;
    private String photo_profile;
    private String email_id;
    private String flat_number;
    private String mobile_number;
    private String security_code;
    private String fcm_reg_id;
    private RWADetailModel.RWA rwa;

    public UserModel() {
    }

    public UserModel(String name, String photo_profile, String id, String email_id) {
        this.name = name;
        this.photo_profile = photo_profile;
        this.id = id;
        this.email_id = email_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto_profile() {
        return photo_profile;
    }

    public void setPhoto_profile(String photo_profile) {
        this.photo_profile = photo_profile;
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmailId(String email_id){
        this.email_id = email_id;
    }

    public String getEmailId(){
        return email_id;
    }

    public void setFlatNumber(String flatnumber){
        this.flat_number = flatnumber;
    }

    public String getFlatNumber(){
        return flat_number;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobile_number = mobileNumber;
    }

    public String getMobileNumber(){
        return this.mobile_number;
    }

    public void setFCMRegID(String regID) {
        this.fcm_reg_id = regID;
    }

    public String getFCMRegID(){
        return fcm_reg_id;
    }

    public void setRwa(RWADetailModel.RWA rwaDetailModel) {
        this.rwa = rwaDetailModel;
    }

    public RWADetailModel.RWA getRwa() {
        return rwa;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("name", name);
        result.put("photo_profile", photo_profile);
        result.put("email_id", email_id);
        result.put("flat_number", flat_number);
        result.put("fcm_reg_id", fcm_reg_id);
        result.put("rwa",rwa);
        return result;
    }
    // [END post_to_map]
}
