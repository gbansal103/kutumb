package bamfaltech.kutumb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gaurav.bansal1 on 08/03/17.
 */

public class ContactModel implements Serializable {


    private static final long serialVersionUID = 1L;

    @SerializedName("contacts")
    private ArrayList<Contact> arrListContacts;

    public ArrayList<Contact> getArrListBusinessObj() {
        return arrListContacts;
    }

    public class Contact implements Serializable {

        @SerializedName("contact_person")
        String contactPerson;

        @SerializedName("contact_post")
        String contactPost;

        @SerializedName("contact_extn")
        String contactExtn;

        @SerializedName("contact_phone")
        String contactPhone;

        public String getContactPerson() {
            return contactPerson;
        }

        public String getContactPost() {
            return contactPost;
        }

        public String getContactPhone() {
            return contactPhone;
        }

        public String getContactExtn() {
            return contactExtn;
        }
    }
}
