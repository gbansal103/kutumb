package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaurav.bansal1 on 28/02/17.
 */

public class TopicModel {

    public String tid;
    public String topic;

    public TopicModel(){

    }

    public TopicModel(String id, String topic) {
        this.tid = id;
        this.topic = topic;
    }

    public void setTid(String id) {
        this.tid = id;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTid(){
        return tid;
    }

    public String getTopic() {
        return topic;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("tid", tid);
        result.put("topic", topic);

        return result;
    }
}
