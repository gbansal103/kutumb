package bamfaltech.kutumb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.emoji.People;

public class PeopleObject implements Serializable {

	private static final long serialVersionUID = 1L;

	@SerializedName("people")
	private ArrayList<People> arrListPeople;

	public ArrayList<People> getArrListBusinessObj() {
		return arrListPeople;
	}

	public class People implements Serializable {

		@SerializedName("chat_name")
		String chatName;

		@SerializedName("chat_receiver")
		String receiverId;

		@SerializedName("chat_status")
		String chatStatus;

		/*@SerializedName("chatid")
		String regId;*/

		public String getChatName() {
			return chatName;
		}

		public String getChatStatus() {
			return chatStatus;
		}

		public String getReceiverId() {
			return receiverId;
		}

		public void setChatStatus(String chatStatus) {
			this.chatStatus = chatStatus;
		}

		/*public String getPersonName() {
			return personName;
		}

		public void setPersonName(String personName) {
			this.personName = personName;
		}

		public String getRegId() {
			return regId;
		}

		public void setRegId(String regId) {
			this.regId = regId;
		}*/
	}

}
