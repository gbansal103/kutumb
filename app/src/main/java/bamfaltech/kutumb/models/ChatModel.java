package bamfaltech.kutumb.models;


import android.graphics.drawable.Drawable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Alessandro Barreto on 17/06/2016.
 */
@IgnoreExtraProperties
public class ChatModel implements Serializable{

    private String id;
    private UserModel userModel;
    private String message;
    private String timeStamp;
    private FileModel file;
    private MapModel mapModel;
    private Drawable onGoingDrawable;
    private String chatSourceID;

    public ChatModel() {
    }

    public ChatModel(String chatSourceID, UserModel userModel, String message, String timeStamp, FileModel file) {
        this.chatSourceID = chatSourceID;
        this.userModel = userModel;
        this.message = message;
        this.timeStamp = timeStamp;
        this.file = file;
    }

    public ChatModel(UserModel userModel, String message, String timeStamp, FileModel file) {
        this.userModel = userModel;
        this.message = message;
        this.timeStamp = timeStamp;
        this.file = file;
    }

    public ChatModel(String chatSourceID, UserModel userModel, String timeStamp, MapModel mapModel) {
        this.chatSourceID =chatSourceID;
        this.userModel = userModel;
        this.timeStamp = timeStamp;
        this.mapModel = mapModel;
    }

    public ChatModel(UserModel userModel, String timeStamp, MapModel mapModel) {
        this.userModel = userModel;
        this.timeStamp = timeStamp;
        this.mapModel = mapModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public FileModel getFile() {
        return file;
    }

    public void setFile(FileModel file) {
        this.file = file;
    }

    public MapModel getMapModel() {
        return mapModel;
    }

    public void setMapModel(MapModel mapModel) {
        this.mapModel = mapModel;
    }

    public void setOnGoingDrawable(Drawable drawable) {
        this.onGoingDrawable = drawable;
    }

    public Drawable getOnGoingDrawable() {
        return onGoingDrawable;
    }

    public String getChatSourceID(){
        return chatSourceID;
    }

    public void setChatSourceID(String chatSourceID) {
        this.chatSourceID = chatSourceID;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("chatSourceID", chatSourceID);
        result.put("userModel", userModel);
        result.put("file", file);
        result.put("mapModel", mapModel);
        result.put("timeStamp", timeStamp);
        result.put("message", message);
        return result;
    }

    /*@Override
    public String toString() {
        return "ChatModel{" +
                "mapModel=" + mapModel +
                ", file=" + file +
                ", timeStamp='" + timeStamp + '\'' +
                ", message='" + message + '\'' +
                ", userModel=" + userModel +
                '}';
    }*/
}
