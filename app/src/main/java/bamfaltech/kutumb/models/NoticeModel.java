package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaurav.bansal1 on 01/03/17.
 */

public class NoticeModel {

    public String title;
    public String body;
    private UserModel userModel;
    private FileModel fileModel;


    public NoticeModel() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public NoticeModel(UserModel userModel, String title, String body, FileModel file) {
        this.userModel = userModel;
        this.title = title;
        this.body = body;
        this.fileModel = file;
    }

    public FileModel getFileModel(){
        return fileModel;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("userModel", userModel);
        result.put("fileModel", fileModel);
        result.put("title", title);
        result.put("body", body);
        return result;
    }

    /*@Override
    public String toString() {
        return "NoticeModel{" +
                "userModel=" + userModel +
                ", fileModel=" + fileModel +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
    // [END post_to_map]*/

}
