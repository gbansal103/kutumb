package bamfaltech.kutumb.models;

import java.util.List;

/**
 * Created by gaurav.bansal1 on 06/02/17.
 */

public class ChatList {
    private String userID;
    private String chatList;

    public ChatList(String userID, List<String> ids) {
        this.userID = userID;
        for(String id : ids) {
            chatList += id + ",";
        }
    }

    @Override
    public String toString() {
        return "ChatList{" +
                "userID=" + userID +
                ", chatList=" + chatList +
                '}';
    }
}
