package bamfaltech.kutumb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gaurav.bansal1 on 28/02/17.
 */

public class RWAListModel implements Serializable{
    private static final long serialVersionUID = 1L;

    @SerializedName("rwa")
    private ArrayList<RWADetailModel.RWA> arrListRWA;

    public ArrayList<RWADetailModel.RWA> getArrListBusinessObj() {
        return arrListRWA;
    }

    public static class RWA implements Serializable {
        private static final long serialVersionUID = 1L;

        @SerializedName("rwatower")
        private String rwatower;


        public final String getRwaTower() {
            return rwatower;
        }

    }


}
