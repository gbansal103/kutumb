package bamfaltech.kutumb.models;

import java.util.List;

/**
 * Created by gaurav.bansal1 on 24/02/17.
 */

public class RWADetails {

    private String rwa_id;
    private String society_name;
    private List<String> towers;
    private List<String> floors;
    private List<String> flats;

    public void setRWAId(String rwa_id) {
        this.rwa_id = rwa_id;
    }

    public String getRWAId(){
        return this.rwa_id;
    }

    public void setSocietyName(String societyName) {
        this.society_name = societyName;
    }

    public String getSocietyName() {
        return this.society_name;
    }

    public void setTowers(List<String> towers){
        this.towers = towers;
    }

    public void setFloors(List<String> floors){
        this.floors = floors;
    }

    public void setFlats(List<String> flats){
        this.flats = flats;
    }

    public List<String> getTowers(){
        return towers;
    }

    public List<String> getFloors(){
        return floors;
    }

    public List<String> getFlats(){
        return flats;
    }
}
