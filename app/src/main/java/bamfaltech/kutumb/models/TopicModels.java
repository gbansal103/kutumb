package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gaurav.bansal1 on 02/03/17.
 */

public class TopicModels {

    List<TopicModel> topicList;

    public TopicModels(){

    }

    public List<TopicModel> getTopicList() {
        return topicList;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("topicList", topicList);

        return result;
    }
}
