package bamfaltech.kutumb.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by gaurav.bansal1 on 20/02/17.
 */

public class RWADetailModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @SerializedName("rwa")
    private ArrayList<RWA> arrListRWA;

    public ArrayList<RWA> getArrListBusinessObj() {
        return arrListRWA;
    }

    public static class RWA implements Serializable {
        private static final long serialVersionUID = 1L;

        @SerializedName("rwaid")
        private String rwaid;

        @SerializedName("rwaname")
        private String rwaname;

        @SerializedName("rwasector")
        private String rwasector;

        @SerializedName("rwacity")
        private String rwacity;

        @SerializedName("rwacountry")
        private String rwacountry;

        @SerializedName("rwatower")
        private String rwatower;

        @SerializedName("rwafloor")
        private String rwafloor;

        @SerializedName("rwaflat")
        private String rwaflat;

        @SerializedName("thirteenFloorBehaviour")
        private String thirteenFloorBehaviour;

        @SerializedName("rwaCountInitial")
        private String rwaCountInitial;

        private String rwaFlatNumber;

        public final String getRwaTower() {
            return rwatower;
        }

        public final String getRwaFloor() {
            return rwafloor;
        }

        public final String getRwaFlat() {
            return rwaflat;
        }

        public String getRwacity() {
            return rwacity;
        }

        public String getRwacountry() {
            return rwacountry;
        }

        public String getRwaname() {
            return rwaname;
        }

        public String getRwaid() {
            return rwaid;
        }

        public String getRwasector() {
            return rwasector;
        }

        public String getRwaFlatNumber() {
            return rwaFlatNumber;
        }

        public void setRwaid(String rwaid) {
            this.rwaid = rwaid;
        }

        public void setRwaname(String rwaName) {
            this.rwaname = rwaName;
        }

        public void setRwacity(String rwacity) {
            this.rwacity = rwacity;
        }

        public void setRwaFlatNumber(String rwaFlatNumber) {
            this.rwaFlatNumber = rwaFlatNumber;
        }

        public void setThirteenFloorBehaviour(String thirteenFloorBehaviour) {
            this.thirteenFloorBehaviour = thirteenFloorBehaviour;
        }

        public void setRwaCountInitial(String rwaCountInitial) {
            this.rwaCountInitial = rwaCountInitial;
        }

        public String getThirteenFloorBehaviour() {
            return thirteenFloorBehaviour;
        }

        public String getRwaCountInitial() {
            return rwaCountInitial;
        }
    }


}
