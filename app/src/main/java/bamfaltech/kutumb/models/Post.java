package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class Post {

    public String uid;
    public String author;
    public String author_flat_no;
    private String photo_profile;
    private String rwaName;
    public String title;
    public String body;
    public int starCount = 0;
    public int commentCount = 0;
    public Map<String, Boolean> stars = new HashMap<>();

    public Post() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Post(String uid, String author, String userFlat, String body) {
        this.uid = uid;
        this.author = author;
        this.author_flat_no = userFlat;
        this.title = title;
        this.body = body;
    }

    public Post(String uid, String author, String userFlat, String title, String body) {
        this.uid = uid;
        this.author = author;
        this.author_flat_no = userFlat;
        this.title = title;
        this.body = body;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPhoto_profile() {
        return photo_profile;
    }

    public void setPhoto_profile(String photo_profile) {
        this.photo_profile = photo_profile;
    }

    public void setRwaName(String rwaName) {
        this.rwaName = rwaName;
    }

    public String getRwaName() {
        return rwaName;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("photo_profile", photo_profile);
        result.put("author", author);
        result.put("author_flat_no", author_flat_no);
        result.put("rwaName", rwaName);
        result.put("title", title);
        result.put("body", body);
        result.put("starCount", starCount);
        result.put("stars", stars);
        result.put("commentCount", commentCount);

        return result;
    }
    // [END post_to_map]

}
// [END post_class]
