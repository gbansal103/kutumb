package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaurav.bansal1 on 14/04/17.
 */

public class NewSocietyModel {
    public String city;
    public String society;

    public NewSocietyModel(){

    }

    public NewSocietyModel(String city, String society) {
        this.city = city;
        this.society = society;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSociety(String society) {
        this.society = society;
    }

    public String getCity(){
        return city;
    }

    public String getSociety() {
        return society;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("city", city);
        result.put("society", society);

        return result;
    }
}
