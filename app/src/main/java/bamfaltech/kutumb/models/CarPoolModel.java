package bamfaltech.kutumb.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gaurav.bansal1 on 07/03/17.
 */

public class CarPoolModel {
    public String uid;
    public String author;
    public String author_flat_no;
    private String photo_profile;
    private String rwaName;
    public String carPoolSource;
    public String carPoolDestination;
    public String carPoolLeaveTimeSource;
    public String carPoolLeaveTimeDestination;
    public String carPoolOwnerPhone;

    public CarPoolModel() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public CarPoolModel(String uid, String author, String userFlat, String source, String destination, String sourceLeaveTime, String destinationLeaveTime, String phoneNumber) {
        this.uid = uid;
        this.author = author;
        this.author_flat_no = userFlat;
        this.carPoolSource = source;
        this.carPoolDestination = destination;
        this.carPoolLeaveTimeSource = sourceLeaveTime;
        this.carPoolLeaveTimeDestination = destinationLeaveTime;
        this.carPoolOwnerPhone = phoneNumber;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPhoto_profile() {
        return photo_profile;
    }

    public void setPhoto_profile(String photo_profile) {
        this.photo_profile = photo_profile;
    }

    public void setRwaName(String rwaName) {
        this.rwaName = rwaName;
    }

    public String getRwaName() {
        return rwaName;
    }

    public String getUid() {
        return uid;
    }

    public String getCarPoolSource() {
        return carPoolSource;
    }

    public String getCarPoolDestination() {
        return carPoolDestination;
    }

    public String getCarPoolLeaveTimeSource() {
        return carPoolLeaveTimeSource;
    }

    public String getCarPoolLeaveTimeDestination() {
        return carPoolLeaveTimeDestination;
    }

    public String getCarPoolOwnerPhone() {
        return carPoolOwnerPhone;
    }


    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("photo_profile", photo_profile);
        result.put("author", author);
        result.put("author_flat_no", author_flat_no);
        result.put("rwaName", rwaName);
        result.put("carPoolSource", carPoolSource);
        result.put("carPoolDestination", carPoolDestination);
        result.put("carPoolLeaveTimeSource", carPoolLeaveTimeSource);
        result.put("carPoolLeaveTimeDestination", carPoolLeaveTimeDestination);
        result.put("carPoolOwnerPhone", carPoolOwnerPhone);

        return result;
    }
}
