package bamfaltech.kutumb.models;

import android.widget.TextView;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import static bamfaltech.kutumb.util.Constants.NOTIFICATION_PURPOSE_NODETAIL;

/**
 * Created by gaurav.bansal1 on 29/11/16.
 */

public class NotificationModel {
    private String id;
    private String notificationMsg;
    private String notificationArtwork;
    private String notificationPurpose;
    private String notificationContentKey;

    public void NotificationModel(){

    }

    public void NotificationModel(String msg, String artwork){
        this.notificationMsg = msg;
        this.notificationArtwork = artwork;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setNotificationMsg(String notificationMsg) {
        this.notificationMsg = notificationMsg;
    }

    public void setNotificationArtwork(String notificationArtwork) {
        this.notificationArtwork = notificationArtwork;
    }

    public String getNotificationArtwork() {
        return notificationArtwork;
    }

    public String getNotificationMsg() {
        return notificationMsg;
    }

    public String getId() {
        return id;
    }

    public void setNotificationContentKey(String notificationContentKey) {
        this.notificationContentKey = notificationContentKey;
    }

    public String getNotificationContentKey() {
        return notificationContentKey;
    }

    public void setNotificationPurpose(String notificationPurpose) {
        this.notificationPurpose = notificationPurpose;
    }

    public String getNotificationPurpose() {
        return notificationPurpose;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("notificationMsg", notificationMsg);
        result.put("notificationArtwork", notificationArtwork);
        result.put("notificationPurpose", notificationPurpose);
        result.put("notificationContentKey", notificationContentKey);
        return result;
    }
    // [END post_to_map]
}


