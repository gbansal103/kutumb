package bamfaltech.kutumb;

import android.app.Activity;
import android.content.Intent;
import android.hardware.camera2.params.Face;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.hockeyapp.android.metrics.model.Base;

import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.fragment.LoginFragment;
import bamfaltech.kutumb.fragment.PrivacyPolicyFragment;
import bamfaltech.kutumb.fragment.TermsAndConditionsFragment;
import bamfaltech.kutumb.managers.FacebookLoginManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.GoogleLoginManager;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.util.Constants;

/**
 * Created by gaurav.bansal1 on 17/11/16.
 */

public class MainActivity extends FragmentActivity{

    LoginButton loginButton;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CallbackManager mCallbackManager;
    private String TAG = "FacebookLogin";

    private FacebookLoginManager mFacebookLoginManager = null;
    private GoogleLoginManager mGoogleLoginManager = null;
    private FirebaseManager mFirebaseManager = null;

    private KutumbApplication mApp;
    private TextView termsTextView;

    private BaseKutumbFragment mCurrentFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        mApp = KutumbApplication.getInstance();

        if(mFirebaseManager == null){
            mFirebaseManager = FirebaseManager.getInstance();
        }
        mFirebaseManager.initializeFirebaseAuth(this);

        LoginFragment loginFragment = new LoginFragment();
        displayFragment(loginFragment);

        /*loginButton = (LoginButton) findViewById(R.id.btnSignInFacebook);
        if(mFacebookLoginManager == null) {
            mFacebookLoginManager = FacebookLoginManager.getInstance();
        }
        mFacebookLoginManager.initialize(this);
        mFacebookLoginManager.loginWithFacebook(loginButton);

        SignInButton signInButton = (SignInButton) findViewById(R.id.btnSignInGoogle);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        if(mGoogleLoginManager == null) {
            mGoogleLoginManager = GoogleLoginManager.getInstance();
        }
        mGoogleLoginManager.initialize(this);
        mGoogleLoginManager.setupGoogleSignin(signInButton);*/
    }

    public void displayFragment(BaseKutumbFragment fragment) {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            setCurrentFragment(fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    public void setCurrentFragment(BaseKutumbFragment fragment) {
        this.mCurrentFragment = fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        mFirebaseManager.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        mFirebaseManager.stop();
    }

    @Override
    public void onBackPressed() {
        onBackPressHandling();
    }

    public void onBackPressHandling() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if(count == 1) {
            this.finish();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case Constants.GOOGLE_REQUEST_SIGNIN:
                mGoogleLoginManager.onActivityResult(requestCode, resultCode, data);
                break;
            default:
                mFacebookLoginManager.onActivityResult(requestCode, resultCode, data);
        }
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(mCurrentFragment instanceof LoginFragment) {
            mCurrentFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
