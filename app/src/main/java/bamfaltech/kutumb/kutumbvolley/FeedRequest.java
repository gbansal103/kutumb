package bamfaltech.kutumb.kutumbvolley;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Map;
import java.util.zip.GZIPInputStream;

/**
 * Created by naveenmishra on 23/06/15.
 */
public class FeedRequest extends Request<Object> {
    private Class<?> className;
    private Response.Listener<Object> listener;
    private Priority priority = Priority.NORMAL;
    private String title;
    private int cacheExpiryTime = 0; // minutes, 4 hours
    private int completeCacheExpiryTime = 1440; // minutes, 24 hours
    private String mCacheKey;
    //private URLManager urlManager;
    private boolean parseItemsIntoTrack = false;

    public boolean isParseItemsIntoTrack() {
        return parseItemsIntoTrack;
    }

    public boolean isTranslationRequired = true;
    private String url;

    public void setParseItemsIntoTrack(boolean parseItemsIntoTrack) {
        this.parseItemsIntoTrack = parseItemsIntoTrack;
    }

    /*public void setUrlManager(URLManager urlManager) {
        this.urlManager = urlManager;
    }*/

    public int getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }

    private int maxRetry = 0;

    public FeedRequest(int method, String url, Class<?> className, Response.Listener<Object> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.url = url;
        this.className = className;
        this.listener = listener;
    }

    public void setCacheExpiryTime(int cacheExpiryTime) {
        this.cacheExpiryTime = cacheExpiryTime;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        // return new CustomRetryPolicy();
        return super.getRetryPolicy();
    }

    public void setCacheKey(String cacheKey) {
        this.mCacheKey = cacheKey;
    }

    @Override
    public String getCacheKey() {
        return mCacheKey;
    }

    @Override
    protected Response<Object> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = getData(response);

            if (className != null && className != String.class) {
                Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED).create();
                Object object = gson.fromJson(json, className);
                return Response.success(setConfigs(object), parseIgnoreCacheHeaders(response));
              /*  if (!parseItemsIntoTrack)
                    return Response.success(setConfigs(object), parseIgnoreCacheHeaders(response));
                else
                    return Response.success(setConfigsAndParseTracks(object), parseIgnoreCacheHeaders(response));*/

            } else
                return Response.success((Object) json, parseIgnoreCacheHeaders(response));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (IOException e) {
            return Response.error(new ParseError(e));
        }
    }

    private Object setConfigs(Object businessObject) {
        /*BusinessObject mBusinessObject = null;
        if (urlManager != null && businessObject != null && businessObject instanceof BusinessObject && ((BusinessObject) businessObject).getVolleyError() == null) {
            mBusinessObject = (BusinessObject) businessObject;
            mBusinessObject.setBusinessObjType(urlManager.getBusinessObjectType());
            GaanaApplication appState = (GaanaApplication) GaanaApplication.getContext();

            if (mBusinessObject.getArrListBusinessObj() != null) {
                ArrayList<BusinessObject> arrListBusinessObj = (ArrayList<BusinessObject>) mBusinessObject.getArrListBusinessObj();
                for (Object businessObj : arrListBusinessObj) {
                    ((BusinessObject) businessObj).setBusinessObjType(urlManager.getBusinessObjectType());
                    if (urlManager.getParentBusinessObjectType() != null) {
                        ((BusinessObject) businessObj).setParentBusinessObjType(urlManager.getParentBusinessObjectType());
                    }
                }
            }

            urlManager.setDataRefreshStatus(false);
            mBusinessObject.setUrlManager(urlManager);
        }

        if (mBusinessObject != null)
            return mBusinessObject;*/
        return businessObject;
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return super.parseNetworkError(volleyError);
    }

    public Cache.Entry parseIgnoreCacheHeaders(NetworkResponse response) {
        if (shouldCache()) {
            long now = System.currentTimeMillis();

            Map<String, String> headers = response.headers;
            long serverDate = 0;
            String serverEtag = null;
            String headerValue;

            headerValue = headers.get("Date");
            if (headerValue != null) {
                serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
            }

            serverEtag = headers.get("ETag");

            final long cacheHitButRefreshed = cacheExpiryTime * 60 * 1000; // in 4 hours cache will be hit, but also refreshed on background
            final long cacheExpired = completeCacheExpiryTime * 60 * 1000; // in 24 hours this cache entry expires completely
            final long softExpire = now + cacheHitButRefreshed;
            final long ttl = now + cacheExpired;

            Cache.Entry entry = new Cache.Entry();
            entry.data = response.data;
            entry.etag = serverEtag;
            entry.softTtl = softExpire;
            entry.ttl = ttl;
            entry.serverDate = serverDate;
            entry.responseHeaders = headers;
            return entry;

        } else {
            return HttpHeaderParser.parseCacheHeaders(response);
        }
    }


    private String getData(NetworkResponse response) throws JsonSyntaxException, IOException {
        String output = "";
        String contentEncoding = response.headers.get("Content-Encoding");
        if (contentEncoding != null && contentEncoding.equalsIgnoreCase("gzip")) {
            GZIPInputStream gStream = new GZIPInputStream(new ByteArrayInputStream(response.data));
            InputStreamReader reader = new InputStreamReader(gStream);
            BufferedReader in = new BufferedReader(reader);
            String read;
            while ((read = in.readLine()) != null) {
                output += read;
            }
            reader.close();
            in.close();
            gStream.close();
        } else
            output = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

        return output;
    }

   /* @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return VolleyUtils.getInstance().getHeaderParams(this);
    }*/

    @Override
    protected void deliverResponse(Object response) {
        if (listener != null)
            listener.onResponse(response);
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }



    public boolean isTranslationRequired() {
        return isTranslationRequired;
    }

    public void setIsTranslationRequired(boolean isTranslationRequired) {
        this.isTranslationRequired = isTranslationRequired;
    }
}
