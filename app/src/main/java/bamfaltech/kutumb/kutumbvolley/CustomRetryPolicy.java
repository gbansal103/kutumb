package bamfaltech.kutumb.kutumbvolley;

import com.android.volley.DefaultRetryPolicy;

/**
 * Created by naveenmishra on 15/09/15.
 */
public class CustomRetryPolicy extends DefaultRetryPolicy {
    private static int DEFAULT_TIMEOUT_MS = 60000;

    public CustomRetryPolicy() {
        super(DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
    }

    public CustomRetryPolicy(int maxRetry) {
        super(DEFAULT_TIMEOUT_MS, maxRetry, DEFAULT_BACKOFF_MULT);
    }
}
