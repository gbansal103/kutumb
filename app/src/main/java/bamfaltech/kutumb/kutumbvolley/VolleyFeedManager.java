package bamfaltech.kutumb.kutumbvolley;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.services.*;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by naveenmishra on 23/06/15.
 */
public class VolleyFeedManager {
    private static final String TAG = "FeedManager";
    private static VolleyFeedManager mFeedManager;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    public static VolleyFeedManager getInstance() {
        if (mFeedManager == null) {
            synchronized (VolleyFeedManager.class) {
                if (mFeedManager == null)
                    mFeedManager = new VolleyFeedManager();
            }
        }
        return mFeedManager;
    }

    public void queueJob(final FeedParams feedParams) {
        if (!feedParams.isCacheOnly()) {
            String cacheKey = getUrl(feedParams);
            String url = cacheKey;

            FeedRequest jsonRequest = new FeedRequest(feedParams.getMethod(), url, feedParams.getClassName(), new Response.Listener<Object>() {
                @Override
                public void onResponse(Object response) {
                    feedParams.getListener().onDataRetrieved(response);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    /*BusinessObject businessObject = new BusinessObject();
                    businessObject.setVolleyError(error);*/
                    feedParams.getListener().onErrorResponse(error.getMessage());
                }
            });

            jsonRequest.setShouldCache(feedParams.shouldCache());
            jsonRequest.setTag(feedParams.getTag());
            jsonRequest.setPriority(feedParams.getPriority());
            jsonRequest.setTitle(feedParams.getTitle());
            if (feedParams.getCachingDurationInMinutes() != -1) {
                jsonRequest.setCacheExpiryTime(feedParams.getCachingDurationInMinutes());
            }

            jsonRequest.setRetryPolicy(new CustomRetryPolicy(feedParams.getMaxRetry()));
            jsonRequest.setCacheKey(cacheKey);
            jsonRequest.setParseItemsIntoTrack(feedParams.isParseItemsIntoTrack());
            jsonRequest.setIsTranslationRequired(feedParams.isTranslationRequired());

            if (feedParams.isDataToBeRefreshed()/* && Util.hasInternetAccess(KutumbApplication.getContext()) && !GaanaApplication.getInstance().isAppInOfflineMode()*/) {
                VolleyUtils.getInstance().getRequestQueue().getCache().invalidate(cacheKey, true);
                VolleyUtils.getInstance().getRequestQueue().getCache().remove(cacheKey);
            }

            VolleyUtils.getInstance().addToRequestQueue(jsonRequest);
        } else {
            //getDataFromCache(feedParams);
        }
    }

    private String getUrl(FeedParams feedParams) {
        String url = feedParams.getUrl();

        if (feedParams.getMethod() == Request.Method.GET) {
            Map<String, String> hmpKeyValue = feedParams.getParams();
            if (hmpKeyValue != null && hmpKeyValue.size() > 0) {
                Object[] keys = hmpKeyValue.keySet().toArray();
                for (int i = 0; i < hmpKeyValue.size(); i++) {
                    String value = hmpKeyValue.get(keys[i].toString());
                    if (value != null) {
                        if (i == (hmpKeyValue.size() - 1)) {
                            url = url + keys[i] + "=" + URLEncoder.encode(value);
                        } else {
                            url = url + keys[i] + "=" + URLEncoder.encode(value) + "&";
                        }
                    }
                }
            }
        }
        url = url.replace(" ", "%20");

        return url;
    }

    /*public void getDataFromCache(FeedParams feedParams) {
        Cache cache = VolleyUtils.getInstance().getRequestQueue().getCache();
        Cache.Entry entry = cache.get(feedParams.getUrl());
        if (entry != null) {
            try {
                String data = new String(entry.data, "UTF-8");
                // handle data parsing
                try {
                    if (feedParams.getClassName() != null && feedParams.getClassName() != String.class) {
                        Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.STATIC, Modifier.PROTECTED).create();
                        feedParams.getListener().onDataRetrieved((BusinessObject) gson.fromJson(data, feedParams.getClassName()));
                        return;
                    }
                } catch (Exception e) {
                    BusinessObject businessObject = new BusinessObject();
                    //  businessObject.setVolleyError(new ParseError(e));
                    feedParams.getListener().onDataRetrieved(businessObject);
                    return;
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        feedParams.getListener().onDataRetrieved(null);
    }

    public void bindImage(NetworkImageView imgNetWorkView, String url) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        imgNetWorkView.setImageUrl(url, imageLoader);
    }

    //call this method only when we require to maintain no cache
    public void bindImageFromNetwork(NetworkImageView imgNetWorkView, String url, int maxWidth, int maxHeight, ImageView.ScaleType scaleType) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        //clear LRU Cache
        ((CustomImageLoader) imageLoader).clearMemoryCache(url, maxWidth, maxHeight, scaleType);
        //clear Disk Cache
        VolleyUtils.getInstance().getImageRequestQueue().getCache().invalidate(url, true);
        VolleyUtils.getInstance().getImageRequestQueue().getCache().remove(url);
        imgNetWorkView.setImageUrl(url, imageLoader);
    }

    public void getBitmap(String url, final com.services.Interfaces.OnBitmapRetrieved onBitmapRetrieved) {
        getBitmap(url, onBitmapRetrieved, true, false);
    }

    public void getBitmap(String url, final com.services.Interfaces.OnBitmapRetrieved onBitmapRetrieved, boolean shouldConsiderCacheFirst) {
        getBitmap(url, onBitmapRetrieved, shouldConsiderCacheFirst, false);
    }

    public void getLargeBitmap(String url, final com.services.Interfaces.OnBitmapRetrieved onBitmapRetrieved) {
        Enums.ConnectionType[] conTypesAllowed = Util.getConnectionType(GaanaApplication.getContext());
        Enums.ConnectionType userConType = ConnectionUtil.getConnectionType(GaanaApplication.getContext());
        boolean isCacheOnly = true;
        for (Enums.ConnectionType conType : conTypesAllowed) {
            if (conType == userConType) {
                isCacheOnly = false;
                break;
            }
        }

        getBitmap(url, onBitmapRetrieved, true, isCacheOnly);
    }

    public void getBitmap(String url, final com.services.Interfaces.OnBitmapRetrieved onBitmapRetrieved, boolean shouldConsiderCacheFirst, boolean isCacheOnly) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        final Bitmap bitmap;

        isCacheOnly = (isCacheOnly || GaanaApplication.getInstance().isAppInDataSaveMode());
        imageLoader.get(url, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Image Load Error: " + error.getMessage());
                        if (onBitmapRetrieved != null)
                            onBitmapRetrieved.onErrorResponse(error);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            // load image into imageview
                            if (onBitmapRetrieved != null)
                                onBitmapRetrieved.onSuccessfulResponse(response.getBitmap());
                        }
                    }
                }
                , 0, 0, ImageView.ScaleType.CENTER_INSIDE, isCacheOnly, shouldConsiderCacheFirst);
    }*/

    public void queueJobMultipart(final FeedParams feedParams) {
        String url = feedParams.getUrl();
        url = url.replace(" ", "%20");

        if (!feedParams.isCacheOnly()) {

            MultipartFeedRequest jsonRequest = new MultipartFeedRequest(feedParams.getMethod(), url, feedParams.getClassName(), new Response.Listener<Object>() {
                @Override
                public void onResponse(Object response) {
                    feedParams.getListener().onDataRetrieved(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    /*BusinessObject businessObject = new BusinessObject();
                    businessObject.setVolleyError(error);*/
                    feedParams.getListener().onErrorResponse(error.getMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return feedParams.getParams();
                }
            };
            jsonRequest.setShouldCache(false);//For any of the POST request we do not want to cache it
            jsonRequest.setTag(feedParams.getTag());
            jsonRequest.setPriority(feedParams.getPriority());
            jsonRequest.setTitle(feedParams.getTitle());
            jsonRequest.setIsTranslationRequired(feedParams.isTranslationRequired());
            if (feedParams.getCachingDurationInMinutes() != -1) {
                jsonRequest.setCacheExpiryTime(feedParams.getCachingDurationInMinutes());
            }
            jsonRequest.setRetryPolicy(new CustomRetryPolicy());
            VolleyUtils.getInstance().addToRequestQueue(jsonRequest);
        } /*else {
            getDataFromCache(feedParams);
        }*/
    }

    /*public void startFeedRetreival(final com.services.Interfaces.OnBusinessObjectRetrieved onBusinessObjectRetrieved, final URLManager urlManager) {
        this.startFeedRetreival(onBusinessObjectRetrieved, urlManager, true);
    }*/

    public void startFeedRetreival(final Interfaces.OnRetrievalCompleteListener onRetrievalCompleteListener, final URLManager urlManager){

        if (urlManager == null) {
            return;
        }

        String url = URLConstants.BASE_KUTUMB_URL;

        if (!TextUtils.isEmpty(urlManager.getFinalUrl()))
            url = urlManager.getFinalUrl();
        else if (urlManager.getUserType() == URLManager.UserType.PRIVATE)
            url = URLConstants.BASE_KUTUMB_URL;

        /*Class<?> className = FeedManager.getInstance().getModelClass(urlManager.getBusinessObjectType());
        if (urlManager.getClassName() != null) {
            className = urlManager.getClassName();
        }*/

        FeedParams feedParams = new FeedParams(url, urlManager.getClassName(), new bamfaltech.kutumb.kutumbvolley.Interfaces.IDataRetrievalListener() {
            @Override
            public void onDataRetrieved(Object businessObject) {
                if(onRetrievalCompleteListener != null) {
                    onRetrievalCompleteListener.onRetrievalCompleted(businessObject);
                }
            }

            @Override
            public void onErrorResponse(String error) {
                if(onRetrievalCompleteListener != null) {
                    onRetrievalCompleteListener.onError(error);
                }
            }

        });

        feedParams.setParams(urlManager.getParams());
        feedParams.setMethod(urlManager.getMethod());
        feedParams.setShouldCache(urlManager.isCacheble());
        feedParams.setDataRefreshStatus(urlManager.isDataToBeRefreshed());
        feedParams.setCachingDurationInMinutes(urlManager.getCachingDurationInMinutes());
        feedParams.setMaxRetry(urlManager.getMaxRetry());
        feedParams.setParseItemsIntoTrack(urlManager.isParseItemsIntoTrack());
        feedParams.setIsTranslationRequired(urlManager.isTranslationRequired());
        if (urlManager.getMethod() == Request.Method.POST) {
            queueJobMultipart(feedParams);
        } else {
            queueJob(feedParams);
        }
    }

    /*public void startFeedRetrieval(final com.services.Interfaces.OnObjectRetrieved onObjectRetrieved, final URLManager urlManager) {

        if (urlManager == null) {
            return;
        }

        String url = UrlConstants.BASE_URL_INDEX;

        if (!TextUtils.isEmpty(urlManager.getFinalUrl()))
            url = urlManager.getFinalUrl();
        else if (urlManager.getUserType() == URLManager.UserType.PRIVATE)
            url = UrlConstants.BASE_URL_PRIVATE;

        Class<?> className = FeedManager.getInstance().getModelClass(urlManager.getBusinessObjectType());
        if (urlManager.getClassName() != null) {
            className = urlManager.getClassName();
        }

        FeedParams feedParams = new FeedParams(url, className, new com.volley.Interfaces.IDataRetrievalListener() {
            @Override
            public void onErrorResponse(BusinessObject errorResponse) {
                onObjectRetrieved.onErrorResponse(errorResponse);
            }

            @Override
            public void onDataRetrieved(Object object) {
                onObjectRetrieved.onRetreivalComplete(object);
            }
        });

        feedParams.setParams(urlManager.getParams());
        feedParams.setMethod(urlManager.getMethod());
        feedParams.setShouldCache(urlManager.isCacheble());
        feedParams.setPriority(urlManager.getPriority());
        feedParams.setIsTranslationRequired(urlManager.isTranslationRequired());
        if (urlManager.getMethod() == Request.Method.POST) {
            queueJobMultipart(feedParams);
        } else {
            queueJob(feedParams);
        }

    }*/

    public String getFeedDataUrl(String baseUrl, HashMap<String, String> hmpKeyValue) {
        String url = baseUrl;
        Object[] keys = hmpKeyValue.keySet().toArray();
        for (int i = 0; i < hmpKeyValue.size(); i++) {
            String value = hmpKeyValue.get(keys[i].toString());
            if (value != null) {
                if (i == (hmpKeyValue.size() - 1)) {
                    url = url + keys[i] + "=" + URLEncoder.encode(value);
                } else {
                    url = url + keys[i] + "=" + URLEncoder.encode(value) + "&";
                }
            }
        }
        return url;
    }

    /*public void startLocalFavoriteRetrievalFromDb(final com.services.Interfaces.OnBusinessObjectRetrieved onBusinessObjectRetrieved, final URLManager urlManager, final String searchParam, final int start, final int end, final String sortColumn, final String orderType) {
        GaanaTaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            com.services.Interfaces.OnBusinessObjectRetrieved newOnbusinessObjectReterived = onBusinessObjectRetrieved;
            BusinessObject mBusinessObj = null;
            boolean hasDataReturned = false;

            @Override
            public void onBackGroundTaskCompleted() {
                try {
                    if (newOnbusinessObjectReterived != null && !hasDataReturned) {
                        newOnbusinessObjectReterived.onRetreivalComplete(mBusinessObj);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void doBackGroundTask() {
                try {
                    mBusinessObj = FavoriteDbHelper.getInstance().getFavoriteListByType(urlManager.getBusinessObjectType(), searchParam, start, end, sortColumn, orderType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, -1);
    }

    public void startFeedRetreivalWithLocalCachePriority(final com.services.Interfaces.OnBusinessObjectRetrieved onBusinessObjectRetrieved, final URLManager urlManager) {
        GaanaTaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            com.services.Interfaces.OnBusinessObjectRetrieved newOnbusinessObjectReterived = onBusinessObjectRetrieved;
            BusinessObject mBusinessObj = null;
            URLManager newUrlManager = urlManager;
            boolean hasDataReturned = false;

            @Override
            public void onBackGroundTaskCompleted() {
                try {
                    if (newOnbusinessObjectReterived != null && !hasDataReturned) {
                        newOnbusinessObjectReterived.onRetreivalComplete(mBusinessObj);
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void doBackGroundTask() {
                try {
                    if (urlManager.isCacheble() && !urlManager.isDataToBeRefreshed()) {
                        mBusinessObj = FeedManager.getInstance().getDataFromCache(urlManager);
                        if (mBusinessObj != null && newOnbusinessObjectReterived != null) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    hasDataReturned = true;
                                    newOnbusinessObjectReterived.onRetreivalComplete(mBusinessObj);
                                }
                            });
                        }
                    }

                    newUrlManager.setIsTranslationRequired(true);
                    BusinessObject businessObj = FeedManager.getInstance().getFeedData(newUrlManager);
                    if (businessObj != null && businessObj.getVolleyError() == null) {
                        mBusinessObj = businessObj;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, -1);
    }

    public void localMediaQueueJob(final String url, final CrossFadeImageView imageView, final LocalMediaImageLoader localMediaImageLoader) {
        final BitmapResponse bitmapResponse = new BitmapResponse();
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        imageView.setImageUrl("", imageLoader);
        Bitmap bmp = getBitmapFromCache(url);
        if (bmp != null) {
            bitmapResponse.setBmp(bmp);
            imageView.setBitmapToImageView(bitmapResponse.getBmp(), true);
            return;
        }
        GaanaTaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            @Override
            public void doBackGroundTask() {
                Bitmap bmp = null;
                if (localMediaImageLoader != null) {
                    bmp = localMediaImageLoader.getBitmapFromDisk(url, imageView);
                    updateImageCache(url, bmp); //null check is inside function
                } else if (!(url.startsWith("http://") || url.startsWith("https://"))) {
                    bmp = getBitmapFromDisk(getAlbumArtwork(url, imageView.getContext()));
                    updateImageCache(url, bmp); //null check is inside function
                }
                bitmapResponse.setBmp(bmp);
            }

            @Override
            public void onBackGroundTaskCompleted() {
                if (bitmapResponse.getBmp() != null) {
                    if (AppConstants.DBG_LEVEL) {
                        Log.i(TAG, "Call for " + url + "ImgView " + imageView.getId());
                    }
                    imageView.setBitmapToImageView(bitmapResponse.getBmp(), true);
                } else {
                    if (AppConstants.DBG_LEVEL) {
                        Log.e(TAG, "Bitmap null for " + url);
                    }
                }
            }
        }, -1, false);
    }

    *//**
     * Will update existing cache.So bitmapManager will fetch image directly from cache.
     *
     * @param url
     * @param bitmap
     *//*
    public void updateImageCache(String url, Bitmap bitmap) {
        //cache.put(url, new SoftReference<Bitmap>(bitmap));
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        //clear LRU Cache
        if (bitmap != null && ((CustomImageLoader) imageLoader).getBitmapFromCache(url) == null) {
            ((CustomImageLoader) imageLoader).updateMemoryCache(url, bitmap);
        }
    }

    public static String getAlbumArtwork(String albumId, Context context) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID + "=" + albumId, null,
                    null);
            if (cursor.moveToFirst()) {
                do {
                    String albumArtwork = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                    return albumArtwork;
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e.toString());
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static Bitmap getBitmapFromDisk(String url) {
        try {
            if (!new File(url).exists())
                return null;

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap bitmap = BitmapFactory.decodeFile(url, options);
            if (bitmap == null)
                Log.w(TAG, "Fetching failed from Disc.Url is " + url);
            return bitmap;
        } catch (OutOfMemoryError error) {//TODO:Not a very good idea but for some devices its throwing for high resolution(Tested for 768*576)
            return null;
        } catch (Exception ex) {
            Log.w(TAG, "EXCEPTION:Error : " + ex.getMessage());
            return null;
        }
    }

    class BitmapResponse {
        private Bitmap bmp;

        private Bitmap getBmp() {
            return bmp;
        }

        private void setBmp(Bitmap bmp) {
            this.bmp = bmp;
        }
    }

    public Bitmap getBitmapFromCache(String url) {
        ImageLoader imageLoader = VolleyUtils.getInstance().getImageLoader();
        return ((CustomImageLoader) imageLoader).getBitmapFromCache(url);
    }

    public void queueJob(URLManager urlManager, String tag, Response.Listener<Object> listener, Response.ErrorListener errorListener) {
        if (urlManager == null) {
            return;
        }

        Class<?> className = FeedManager.getInstance().getModelClass(urlManager.getBusinessObjectType());
        if (urlManager.getClassName() != null) {
            className = urlManager.getClassName();
        }

        String cacheKey = getUrl(urlManager);
        String url = cacheKey;

        FeedRequest jsonRequest = new FeedRequest(urlManager.getMethod(), url, className, listener, errorListener);
        jsonRequest.setUrlManager(urlManager);
        jsonRequest.setShouldCache(urlManager.isCacheble());
        jsonRequest.setIsTranslationRequired(urlManager.isTranslationRequired());
        jsonRequest.setTag(tag);
        jsonRequest.setPriority(urlManager.getPriority());
        jsonRequest.setTitle(tag);
        jsonRequest.setParseItemsIntoTrack(urlManager.isParseItemsIntoTrack());
        if (urlManager.getCachingDurationInMinutes() != -1) {
            jsonRequest.setCacheExpiryTime(urlManager.getCachingDurationInMinutes());
        }

        jsonRequest.setRetryPolicy(new CustomRetryPolicy(urlManager.getMaxRetry()));
        jsonRequest.setCacheKey(cacheKey);

        if (urlManager.isDataToBeRefreshed() && Util.hasInternetAccess(GaanaApplication.getContext()) && !GaanaApplication.getInstance().isAppInOfflineMode()) {
            VolleyUtils.getInstance().getRequestQueue().getCache().invalidate(cacheKey, true);
            VolleyUtils.getInstance().getRequestQueue().getCache().remove(cacheKey);
        }

        VolleyUtils.getInstance().addToRequestQueue(jsonRequest);
    }

    private String getUrl(URLManager urlManager) {
        String url = UrlConstants.BASE_URL_INDEX;

        if (!TextUtils.isEmpty(urlManager.getFinalUrl()))
            url = urlManager.getFinalUrl();
        else if (urlManager.getUserType() == URLManager.UserType.PRIVATE)
            url = UrlConstants.BASE_URL_PRIVATE;

        if (urlManager.getMethod() == Request.Method.GET) {
            Map<String, String> hmpKeyValue = urlManager.getParams();
            if (hmpKeyValue != null && hmpKeyValue.size() > 0) {
                Object[] keys = hmpKeyValue.keySet().toArray();
                for (int i = 0; i < hmpKeyValue.size(); i++) {
                    String value = hmpKeyValue.get(keys[i].toString());
                    if (value != null) {
                        if (i == (hmpKeyValue.size() - 1)) {
                            url = url + keys[i] + "=" + URLEncoder.encode(value);
                        } else {
                            url = url + keys[i] + "=" + URLEncoder.encode(value) + "&";
                        }
                    }
                }
            }
        }
        url = url.replace(" ", "%20");

        return url;
    }
*/

}
