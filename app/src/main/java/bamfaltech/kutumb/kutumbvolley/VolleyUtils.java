package bamfaltech.kutumb.kutumbvolley;

import android.os.Build;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;


import java.util.HashMap;
import java.util.Map;

import bamfaltech.kutumb.KutumbApplication;

/**
 * Created by gaurav.bansal1 on 20/08/15.
 */
public class VolleyUtils {
    private static VolleyUtils instance;
    private RequestQueue mRequestQueue;
    private RequestQueue mImageRequestQueue;
    private ImageLoader mImageLoader;
    //private LruBitmapCache mLruBitmapCache = null;

    public static VolleyUtils getInstance(){
        if(instance == null)
            instance = new VolleyUtils();
        return instance;
    }

    private VolleyUtils(){
        init();
    }

    public void init() {
    }

    /*public LruBitmapCache getmLruBitmapCache(){
        return mLruBitmapCache;
    }*/

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(KutumbApplication.getContext()/*, new HurlStack(null , NoSSLv3SocketFactory.getSocketFactory())*/);
        }

        return mRequestQueue;
    }

    public boolean isFromCache(){
        /*if(mLruBitmapCache != null){
            return mLruBitmapCache.isFromCache();
        } else {*/
            return false;
        //}
    }

    /*public RequestQueue getImageRequestQueue() {
        if (mImageRequestQueue == null) {
            mImageRequestQueue = Volley.newImageRequestQueue(GaanaApplication.getContext(), new HurlStack(null , NoSSLv3SocketFactory.getSocketFactory()));
        }

        return mImageRequestQueue;
    }*/

    /*public ImageLoader getImageLoader() {
        mLruBitmapCache = new LruBitmapCache();
        if (mImageLoader == null) {
            mImageLoader = new CustomImageLoader(getImageRequestQueue(), mLruBitmapCache);
        }
        return this.mImageLoader;
    }*/

    public <T> void addToRequestQueue(Request<T> req) {
        if(req.getPriority() == Request.Priority.HIGH){
            //We need to call getHighPriorityRequestQueue() once we will handle all the aspects
            getRequestQueue().add(req);
        } else {
            getRequestQueue().add(req);
        }
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void cancelPendingImageRequests(Object tag) {
        if (mImageRequestQueue != null) {
            mImageRequestQueue.cancelAll(tag);
        }
    }

    public void resetCache(){
        if (mRequestQueue != null) {
            mRequestQueue.getCache().clear();
        }

        if (mImageRequestQueue != null) {
            mImageRequestQueue.getCache().clear();
        }
    }

    public void removeCache(String url){
        if (mRequestQueue != null) {
            mRequestQueue.getCache().remove(url);
        }
    }

    public void invalidateCache(String url){
        if (mRequestQueue != null) {
            mRequestQueue.getCache().invalidate(url, true);
        }
    }

    /*public Map<String, String> getHeaderParams(FeedRequest feedRequest) {
        Map<String, String> headerParams = new HashMap<String, String>();
        if (TextUtils.isEmpty(Constants.API_HEADER_COUNTRY_CODE)) {
            Constants.API_HEADER_COUNTRY_CODE = "IN";
        }
        headerParams.put("Accept-Encoding", "gzip");
        headerParams.put(Constants.API_HEADER_NAME_APP_ID, Constants.API_HEADER_APP_ID);
        headerParams.put(Constants.API_HEADER_NAME_COUNTRY, Constants.API_HEADER_COUNTRY_CODE);
        headerParams.put(Constants.API_HEADER_NAME_GPS_CITY, Constants.API_HEADER_CITY_NAME);
        headerParams.put(Constants.API_HEADER_NAME_GPS_STATE, Constants.API_HEADER_STATE_NAME);
        headerParams.put(Constants.API_HEADER_NAME_GPS_ENABLE, Constants.API_HEADER_GPS_ENABLED);
        headerParams.put(Constants.API_HEADER_NAME_DEVICE_TYPE, Constants.API_HEADER_VALUE_DEVICE_TYPE);
        headerParams.put(Constants.API_HEADER_NAME_APP_VERSION, Constants.API_HEADER_VALUE_APP_VERSION);
        headerParams.put(Constants.API_HEADER_NAME_DEVICE_TIME_SECONDS, String.valueOf(System.currentTimeMillis() / 1000));
        headerParams.put(Constants.API_HEADER_NAME_DEVICE_TIME, Util.getTime());
        headerParams.put(Constants.API_HEADER_COOKIES, "PHPSESSID=" + GaanaApplication.getCurrentSessionId());
        headerParams.put(Constants.API_HEADER_NAME_DEVICE_ID, Util.getDeviceId(GaanaApplication.getContext()));
        headerParams.put(Constants.API_HEADER_NAME_OS_VERSION, Build.VERSION.RELEASE);
        headerParams.put(Constants.API_HEADER_NAME_GAANA_APP_VERSION, "gaanaAndroid-" + Constants.API_HEADER_VALUE_GAANA_APP_VERSION);
        if((!Constants.API_HEADER_VALUE_DISPLAY_LANGUAGE.equalsIgnoreCase("English"))
                && feedRequest.isTranslationRequired()){
            headerParams.put(Constants.API_HEADER_NAME_DISPLAY_LANGUAGE, Constants.API_HEADER_VALUE_DISPLAY_LANGUAGE);
        }
        return headerParams;
    }*/
}
