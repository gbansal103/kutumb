package bamfaltech.kutumb.kutumbvolley;



/**
 * Created by gaurav.bansal1 on 14/10/16.
 */
public class Interfaces {

    public interface IDataRetrievalListener{
        public void onDataRetrieved(Object businessObject);
        public void onErrorResponse(String error);
    }
}
