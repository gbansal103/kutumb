package bamfaltech.kutumb.kutumbvolley;

import com.android.volley.Request;

import java.util.Map;


/**
 * Created by gaurav.bansal1 on 14/10/16.
 */
public class FeedParams {
    private int method = Request.Method.GET;
    private Class<?> className;
    private String url;
    private String tag;
    private String title;

    private boolean isCacheOnly;
    private boolean shouldCache;
    private Map<String, String> params;
    private Interfaces.IDataRetrievalListener listener;
    private Request.Priority priority = Request.Priority.NORMAL;
    private Boolean isToBeRefresh = false;

    private int cachingDurationInMinutes = -1;
    private int maxRetry = 0;

    private boolean isTranslationRequired = true;

    public int getMaxRetry() {
        return maxRetry;
    }

    public void setMaxRetry(int maxRetry) {
        this.maxRetry = maxRetry;
    }

    public FeedParams (String url, Class<?> className, Interfaces.IDataRetrievalListener listener){
        this.url = url;
        this.className = className;
        this.listener = listener;
    }

    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public Class<?> getClassName() {
        return className;
    }

    public String getUrl() {
        return url;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public boolean isCacheOnly() {
        return isCacheOnly;
    }

    public void setIsCacheOnly(boolean isCacheOnly) {
        this.isCacheOnly = isCacheOnly;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public Interfaces.IDataRetrievalListener getListener() {
        return listener;
    }

    public boolean shouldCache() {
        return shouldCache;
    }

    public void setShouldCache(boolean shouldCache) {
        this.shouldCache = shouldCache;
    }

    public Request.Priority getPriority() {
        return priority;
    }

    public void setPriority(Request.Priority priority) {
        this.priority = priority;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return title;
    }

    public Boolean isDataToBeRefreshed() {
        return this.isToBeRefresh;
    }

    public void setDataRefreshStatus(Boolean isToBeRefresh) {
        this.isToBeRefresh = isToBeRefresh;
    }

    public int getCachingDurationInMinutes() {
        return cachingDurationInMinutes;
    }

    public void setCachingDurationInMinutes(int minutes) {
        this.cachingDurationInMinutes = minutes;
    }

    private boolean parseItemsIntoTrack = false;
    public boolean isParseItemsIntoTrack() {
        return parseItemsIntoTrack;
    }

    public void setParseItemsIntoTrack(boolean parseItemsIntoTrack) {
        this.parseItemsIntoTrack = parseItemsIntoTrack;
    }

    public boolean isTranslationRequired(){
        return isTranslationRequired;
    }

    public void setIsTranslationRequired(boolean isTranslationRequired){
        this.isTranslationRequired = isTranslationRequired;
    }

}
