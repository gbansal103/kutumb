package bamfaltech.kutumb;

import android.app.Application;
import android.content.Context;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.FirebaseDatabase;

import bamfaltech.kutumb.managers.ApplicationLifecycleManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Serializer;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;

/**
 * Created by gaurav.bansal1 on 17/11/16.
 */

public class KutumbApplication extends Application {

    private static KutumbApplication mInstance = null;
    UserModel mCurrentuserModel = null;
    FirebaseManager mFirebaseManager;
    public static int sessionCount = -1;
    RWADetailModel.RWA currentRWA;

    public KutumbApplication(){
        mInstance = this;
    }

    public static KutumbApplication getInstance(){
        return mInstance;
    }

    public static Context getContext() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        registerActivityLifecycleCallbacks(new ApplicationLifecycleManager());
        sessionCount = DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_APPLICATION_SESSION_COUNT, 0, false);
    }

    public void setCurrentUserModel(UserModel userModel){
        mCurrentuserModel = userModel;
    }

    public UserModel getCurrentUserModel(){
        return mCurrentuserModel;
    }

    public void setFirebaseManagerInstance(FirebaseManager firebaseManager) {
        mFirebaseManager = firebaseManager;
    }

    public FirebaseManager getFirebaseManager(){
        return mFirebaseManager;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public void setCurrentRWA(RWADetailModel.RWA rwa) {
        this.currentRWA = rwa;
    }

    public RWADetailModel.RWA getCurrentRWA(){
        return currentRWA;
    }
}
