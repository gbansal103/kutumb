package bamfaltech.kutumb;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import org.w3c.dom.Text;

import bamfaltech.kutumb.adapter.ImageViewPagerAdapter;
import bamfaltech.kutumb.fragment.TabContainerFragment;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;

/**
 * Created by gaurav.bansal1 on 24/02/17.
 */

public class BoardScreenActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener{

    private ViewPager mViewPager;
    private ImageViewPagerAdapter mAdapter;
    private TextView mSkipText;
    private TextView mNextText;
    private CirclePageIndicator mCirclePageIndicator;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.board_screen_activity);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mAdapter = new ImageViewPagerAdapter(this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);

        mCirclePageIndicator = (CirclePageIndicator) findViewById(R.id.circlepageindicator);
        mCirclePageIndicator.setSnap(true);
        mCirclePageIndicator.setViewPager(mViewPager);
        mCirclePageIndicator.setOnPageChangeListener(this);

        mSkipText = (TextView) findViewById(R.id.skip_button);
        mSkipText.setTextColor(getResources().getColor(R.color.com_facebook_blue));
        mNextText = (TextView) findViewById(R.id.next_button);
        mSkipText.setOnClickListener(this);
        mNextText.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.skip_button:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                finish();
                DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_BOARD_SCREEN_COMPLETE, true, false);
                break;
            case R.id.next_button:
                if(mViewPager.getCurrentItem() == 2) {
                    Intent nextIntent = new Intent(this, MainActivity.class);
                    startActivity(nextIntent);
                    finish();
                    DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_BOARD_SCREEN_COMPLETE, true, false);
                    break;
                }
                mViewPager.setCurrentItem(getItem(+1), true);
                break;
        }
    }

    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(mViewPager.getCurrentItem() == 0) {
            mSkipText.setTextColor(getResources().getColor(R.color.com_facebook_blue));
        } else {
            mSkipText.setTextColor(getResources().getColor(R.color.white));
        }

        if(mViewPager.getCurrentItem() == 2) {
            mNextText.setText("Finish");
        } else {
            mNextText.setText("Next");
        }
        mViewPager.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
