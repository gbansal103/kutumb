package bamfaltech.kutumb;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.util.Constants;


public class CoachMarksActivity extends Activity {
    private LayoutInflater mLayoutInflater;
    private View contentView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String coachMarkValue = getIntent().getExtras().getString(Constants.COACHMARK_VALUE);
        mLayoutInflater = LayoutInflater.from(this);

        if (coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_SEARCH)) {
            contentView = mLayoutInflater.inflate(R.layout.coachmark_search_notification, null);
            ImageView notifView = (ImageView) contentView.findViewById(R.id.notif_coachmark);
            notifView.setVisibility(View.INVISIBLE);
        } else if (coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_NOTIFICATION)) {
            contentView = mLayoutInflater.inflate(R.layout.coachmark_search_notification, null);
            ImageView notifView = (ImageView) contentView.findViewById(R.id.search_coachmark);
            notifView.setVisibility(View.INVISIBLE);
        } else if(coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_NEW_POST)){
            //contentView = mLayoutInflater.inflate(R.layout.coachmark_search, null);
        } else if(coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_NEW_UTILITY)) {
            //contentView = mLayoutInflater.inflate(R.layout.coachmark_share, null);
        } else if(coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_NEW_NOTICE)) {
            //contentView = mLayoutInflater.inflate(R.layout.coachmark_cast, null);
        } else if(coachMarkValue.equalsIgnoreCase(Constants.COACHMARK_VALUE_NEW_CARPOOL)) {
            //contentView = mLayoutInflater.inflate(R.layout.coachmark_app_display_language, null);
        }

       // Util.setFonts(this, contentView, Util.FontFamily.PROXIMANOVA_REGULAR);

        setContentView(contentView);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onStart() {
        //GaanaMusicService.isAppInForeground = true;
//        SeventyNineAdManager.getInstance(this).setIsAppInForeground(true);
//        ColombiaVideoAdManager.getInstance().setIsAppInForeground(true);
        super.onStart();
    }

    @Override
    protected void onStop() {
        //GaanaMusicService.isAppInForeground = false;
//        SeventyNineAdManager.getInstance(this).setIsAppInForeground(false);
//        ColombiaVideoAdManager.getInstance().setIsAppInForeground(false);
        super.onStop();
    }
}
