package bamfaltech.kutumb.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import bamfaltech.kutumb.R;

/**
 * Created by gaurav.bansal1 on 5/15/16.
 */
public class ImageViewPagerAdapter extends PagerAdapter {

    Context mContext;

    public ImageViewPagerAdapter(Context context){
        super();
        mContext = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        View mView = mInflater.inflate(R.layout.simple_image_text_layout, container, false);
        ImageView imageView = (ImageView) mView.findViewById(R.id.screen_image);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        TextView textView = (TextView) mView.findViewById(R.id.screen_description);

        switch (position) {
            case 0:
                imageView.setImageResource(R.drawable.connect_board);
                textView.setTextColor(mContext.getResources().getColor(R.color.white));
                textView.setText("Share your thoughts");
                break;
            case 1:
                imageView.setImageResource(R.drawable.board_second);
                textView.setText("Connect with your neighbours");
                break;
            case 2:
                imageView.setImageResource(R.drawable.utility_board);
                textView.setTextColor(mContext.getResources().getColor(R.color.white));
                textView.setText("Promote and Earn");
                break;
        }
        ((ViewPager)container).addView(mView);
        return mView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
