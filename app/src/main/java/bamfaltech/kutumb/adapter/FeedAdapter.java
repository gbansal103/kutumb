package bamfaltech.kutumb.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.FeedModel;

/**
 * Created by gaurav.bansal1 on 15/10/16.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {
    private List<FeedModel> mDataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mNameTextView;
        public TextView mStatusTextView;
        public ImageView mFeedImage;
        public ImageView mProfilePic;
        public TextView mTimestamp;

        public ViewHolder(View v) {
            super(v);
            mNameTextView = (TextView)v.findViewById(R.id.name);
            mTimestamp = (TextView)v.findViewById(R.id.timestamp);
            mStatusTextView = (TextView)v.findViewById(R.id.txtStatusMsg);
            mProfilePic = (ImageView)v.findViewById(R.id.profilePic);
            mFeedImage = (ImageView)v.findViewById(R.id.feedImage1);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public FeedAdapter(List<FeedModel> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_item, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        FeedModel feedModel = mDataset.get(position);
        holder.mNameTextView.setText(feedModel.getName());
        holder.mTimestamp.setText(feedModel.getTimeStamp());
        holder.mStatusTextView.setText(feedModel.getStatus());
        //holder.mProfilePic.setImageDrawable(feedModel.getProfilePic());
        //holder.mFeedImage.setImageDrawable(feedModel.getImge());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}



