package bamfaltech.kutumb.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by gaurav.bansal1 on 07/02/17.
 */

public class CustomListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private IAddListItemView mIAddListItemView;
    private int mCount;

    public CustomListAdapter(Context context) {

    }

    public void setParamaters(int count, IAddListItemView iAddListItemView) {
        mCount = count;
        this.mIAddListItemView = iAddListItemView;
    }

    public void updateAdapterCount(int count) {
        mCount = count;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return mIAddListItemView.createViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mIAddListItemView.addListItemView(position, holder, null);
    }

    @Override
    public int getItemCount() {
        return mCount;
    }

    @Override
    public int getItemViewType(int position) {
        return mIAddListItemView.getItemViewType(position - 1);
    }

    public interface IAddListItemView {
        public void addListItemView(final int position, RecyclerView.ViewHolder pConvertView, ViewGroup parentView);

        public void showHideEmtpyView(boolean showEmptyView);

        public RecyclerView.ViewHolder createViewHolder(ViewGroup parent, int viewType);

        public int getItemViewType(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
    }
}
