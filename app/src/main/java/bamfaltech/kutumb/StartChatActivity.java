package bamfaltech.kutumb;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.adapter.ChatFirebaseAdapter;
import bamfaltech.kutumb.adapter.ClickListenerChatFirebase;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.HttpManager;
import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.models.ChatModel;
import bamfaltech.kutumb.models.FileModel;
import bamfaltech.kutumb.models.MapModel;
import bamfaltech.kutumb.models.PeopleObject;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.util.Util;
import bamfaltech.kutumb.view.FullScreenImageActivity;
import bamfaltech.kutumb.view.LoginActivity;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class StartChatActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, ClickListenerChatFirebase {

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;

    static final String TAG = StartChatActivity.class.getSimpleName();
    static final String CHAT_REFERENCE = "chatmodel";

    //Firebase and GoogleApiClient
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;
    private DatabaseReference mFirebaseDatabaseReference;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    //CLass Model
    private UserModel userModel;

    //Views UI
    private RecyclerView rvListMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private ImageView btSendMessage,btEmoji;
    private EmojiconEditText edMessage;
    private View contentRoot;
    private EmojIconActions emojIcon;
    private UserModel frontUserModel;

    //File
    private File filePathImageCamera;

    private String frontUserID;
    private String frontUserName;
    private String frontUserFCMRegID;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_chat_activity);

        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);

        frontUserID = getIntent().getStringExtra("front_user_id");
        frontUserName = getIntent().getStringExtra("front_user_name");
        frontUserFCMRegID = getIntent().getStringExtra("front_user_reg_id");

        String chatName = getIntent().getStringExtra("chat_name");
        String chatReceiver = getIntent().getStringExtra("chat_receiver");
        if(!TextUtils.isEmpty(chatName) && !TextUtils.isEmpty(chatReceiver) && chatReceiver.equalsIgnoreCase(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            updateChatUser(chatName, "0");
        }

        //frontUserModel = FirebaseManager.getInstance().getFrontUserName(frontUserID, mToolbar);
        // mToolbar.setTitle(frontUserName);

        FirebaseManager.getInstance().getFrontUserInfoFromFirebase(frontUserID, new Interfaces.OnUserModelRetrieved() {
            @Override
            public void onRetreivalComplete(UserModel userModel) {
                frontUserName = userModel.getName();
                frontUserFCMRegID = userModel.getFCMRegID();
                String frontUserArtwork = userModel.getPhoto_profile();
                //mToolbar.setTitle(frontUserName);
                setupToolbar(frontUserName, frontUserArtwork);
            }

            @Override
            public void onError(String errormsg) {

            }
        });

        if (!Util.verifyConnection(this)) {
            Util.initToast(this, "internet connection is not available.");
            finish();
        } else {
            bindViews();
            verifyUserIfLogin();
        }

        saveNewChatUser(frontUserID, frontUserName);
    }

    private void setupToolbar(String toolbarTitle, String frontUserArtwork) {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(this);
        mDetailsMaterialActionBar.setParams(null, toolbarTitle, frontUserArtwork);
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(toolbarTitle);
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mToolbar.inflateMenu(R.menu.chat_menu);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }


    private void saveNewChatUser(final String regid, final String username) {
        if(userModel == null) {
            return;
        }
        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("userid", userModel.getId());
        hmpCredentials.put("username", userModel.getName());
        hmpCredentials.put("reg_id", regid);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(String.class);
        urlManager.setPriority(Request.Priority.NORMAL);
        urlManager.setCachable(false);
        urlManager.setMethod(Request.Method.POST);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.INSERT_NEW_CHAT_USER);

        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                String response = (String) object;
                if (response != null) {
                    if (response.equalsIgnoreCase("Chat already registered")) {
                        //nothing required to to
                    } else {
                        if (response.equalsIgnoreCase("New Chat Item added successfully")) {
                            //nothing required to do
                        }
                    }

                }

            }

            @Override
            public void onError(String errorMsg) {

            }
        }, urlManager);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_IMG);

        if (requestCode == IMAGE_GALLERY_REQUEST){
            if (resultCode == RESULT_OK){
                Uri selectedImageUri = data.getData();
                if (selectedImageUri != null){
                    sendFileFirebase(storageRef,selectedImageUri);
                }else{
                    //URI IS NULL
                }
            }
        }else if (requestCode == IMAGE_CAMERA_REQUEST){
            if (resultCode == RESULT_OK){
                if (filePathImageCamera != null && filePathImageCamera.exists()){
                    StorageReference imageCameraRef = storageRef.child(filePathImageCamera.getName()+"_camera");
                    sendFileFirebase(imageCameraRef,filePathImageCamera);
                }else{
                    //IS NULL
                }
            }
        }else if (requestCode == PLACE_PICKER_REQUEST){
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place!=null){
                    LatLng latLng = place.getLatLng();
                    MapModel mapModel = new MapModel(latLng.latitude+"",latLng.longitude+"");
                    sendMapToFirebase(mapModel);
                }else{
                    //PLACE IS NULL
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.sendPhoto:
                photoCameraIntent();
                break;
            case R.id.sendPhotoGallery:
                photoGalleryIntent();
                break;
            case R.id.sendLocation:
                locationPlacesIntent();
                break;
            case R.id.sign_out:
                signOut();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onOptionItemSelected(int id) {
        switch (id){
            case R.id.sendPhoto:
                photoCameraIntent();
                break;
            case R.id.sendPhotoGallery:
                photoGalleryIntent();
                break;
            case R.id.sendLocation:
                locationPlacesIntent();
                break;
            case R.id.sign_out:
                signOut();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Util.initToast(this,"Google Play Services error.");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonMessage:
                if(!TextUtils.isEmpty(edMessage.getText().toString())) {
                    sendMessageFirebase();
                }
                break;
        }
    }

    HttpPost httppost;
    StringBuffer buffer;
    HttpResponse response;
    HttpClient httpclient;
    List<NameValuePair> nameValuePairs;
    String user_name = "";
    String messageToSend = "";
    public void sendMessage(String messageToSend) {
        HttpManager.getInstance().sendMessage(messageToSend, frontUserFCMRegID, Constants.NOTIFICATION_PURPOSE_CHAT);
    }

    @Override
    public void clickImageChat(View view, int position,String nameUser,String urlPhotoUser,String urlPhotoClick) {
        Intent intent = new Intent(this,FullScreenImageActivity.class);
        intent.putExtra("nameUser",nameUser);
        intent.putExtra("urlPhotoUser",urlPhotoUser);
        intent.putExtra("urlPhotoClick",urlPhotoClick);
        startActivity(intent);
    }

    @Override
    public void clickImageMapChat(View view, int position,String latitude,String longitude) {
        String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude,longitude,latitude,longitude);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }


    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final Uri file){
        mProgressBar.setVisibility(View.VISIBLE);
        FirebaseManager.getInstance().sendFileFirebase(storageReference, file, new Interfaces.OnUploadFileCompleteListener() {
            @Override
            public void onUploadCompleted(FileModel fileModel) {
                mProgressBar.setVisibility(View.GONE);
                final ChatModel chatModel = new ChatModel(userModel.getId(), userModel,"",Calendar.getInstance().getTime().getTime()+"",fileModel);

                final String room_type_1 = userModel.getId() + "_" + frontUserID;
                final String room_type_2 = frontUserID + "_" + userModel.getId();
                mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                        .getRef()
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(room_type_1)) {
                                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                                    room = room_type_1;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_1)
                                            .push()
                                            .setValue(chatModel);
                                } else if (dataSnapshot.hasChild(room_type_2)) {
                                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                                    room = room_type_2;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_2)
                                            .push()
                                            .setValue(chatModel);
                                } else {
                                    Log.e(TAG, "sendMessageToFirebaseUser: success");
                                    room = room_type_1;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_1)
                                            .push()
                                            .setValue(chatModel);
                                }
                                edMessage.setText(null);
                                saveOrUpdateChatUser(room, frontUserID, "1");
                                sendMessage("Image");
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // Unable to send message.
                            }
                        });
            }

            @Override
            public void onError(String error) {
                Log.e(TAG,"onFailure sendFileFirebase "+error);
                mProgressBar.setVisibility(View.GONE);
            }
        });


            /*mChatFirebaseAdapter.setFileSendingInProgress(true);
            Drawable drawableInProgress = getResources().getDrawable(R.drawable.default_user_image);
            try {
                InputStream inputStream = getContentResolver().openInputStream(file);
                drawableInProgress= Drawable.createFromStream(inputStream, file.toString() );
            } catch (FileNotFoundException e) {
                drawableInProgress = getResources().getDrawable(R.drawable.default_user_image);
            }
            mChatFirebaseAdapter.setFileInProgress(drawableInProgress);
            mChatFirebaseAdapter.notifyDataSetChanged();*/
    }

    /**
     * Envia o arvquivo para o firebase
     */
    private void sendFileFirebase(StorageReference storageReference, final File file){
        mProgressBar.setVisibility(View.VISIBLE);
        FirebaseManager.getInstance().sendFileFirebase(storageReference, file, new Interfaces.OnUploadFileCompleteListener() {
            @Override
            public void onUploadCompleted(FileModel fileModel) {
                mProgressBar.setVisibility(View.GONE);
                final ChatModel chatModel = new ChatModel(userModel.getId(), userModel,"",Calendar.getInstance().getTime().getTime()+"",fileModel);


                final String room_type_1 = userModel.getId() + "_" + frontUserID;
                final String room_type_2 = frontUserID + "_" + userModel.getId();
                mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                        .getRef()
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(room_type_1)) {
                                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                                    room = room_type_1;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_1)
                                            .push()
                                            .setValue(chatModel);
                                } else if (dataSnapshot.hasChild(room_type_2)) {
                                    Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                                    room = room_type_2;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_2)
                                            .push()
                                            .setValue(chatModel);
                                } else {
                                    Log.e(TAG, "sendMessageToFirebaseUser: success");
                                    room = room_type_1;
                                    //ChatModel chatModel = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                                    mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                            .child(room_type_1)
                                            .push()
                                            .setValue(chatModel);
                                }
                                sendMessage("Image");
                                saveOrUpdateChatUser(room, frontUserID, "1");
                                edMessage.setText(null);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // Unable to send message.
                            }
                        });
                //mFirebaseDatabaseReference.child(CHAT_REFERENCE).push().setValue(chatModel);
            }

            @Override
            public void onError(String error) {
                Log.e(TAG,"onFailure sendFileFirebase "+error);
                mProgressBar.setVisibility(View.GONE);

            }
        });
    }

    private void sendMapToFirebase(final MapModel mapModel) {
        final String room_type_1 = userModel.getId() + "_" + frontUserID;
        final String room_type_2 = frontUserID + "_" + userModel.getId();

        /*final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference();*/


        FirebaseMessaging fm = FirebaseMessaging.getInstance();
        fm.send(new RemoteMessage.Builder("722586070974" + "@gcm.googleapis.com")
                .setMessageId(Integer.toString(count++/*msgId.incrementAndGet()*/))
                .addData("my_message", edMessage.getText().toString())
                .build());

        mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(room_type_1)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                            room = room_type_1;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel, Calendar.getInstance().getTime().getTime()+"", mapModel);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_1)
                                    .push()
                                    .setValue(chatModel);
                        } else if (dataSnapshot.hasChild(room_type_2)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                            room = room_type_2;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel, Calendar.getInstance().getTime().getTime()+"", mapModel);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_2)
                                    .push()
                                    .setValue(chatModel);
                        } else {
                            Log.e(TAG, "sendMessageToFirebaseUser: success");
                            room = room_type_1;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel, Calendar.getInstance().getTime().getTime()+"", mapModel);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_1)
                                    .push()
                                    .setValue(chatModel);
                        }
                        edMessage.setText(null);
                        saveOrUpdateChatUser(room, frontUserID, "1");
                        sendMessage("Location");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Unable to send message.
                    }
                });
    }

    /**
     * Obter local do usuario
     */
    private void locationPlacesIntent(){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    /**
     * Enviar foto tirada pela camera
     */
    private void photoCameraIntent(){
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto+"camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        it.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(filePathImageCamera));
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);
    }

    /**
     * Enviar foto pela galeria
     */
    private void photoGalleryIntent(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
    }

    private static int count = 1;
    private String room = "";
    public void sendMessageFirebase() {
        final String room_type_1 = userModel.getId() + "_" + frontUserID;
        final String room_type_2 = frontUserID + "_" + userModel.getId();

        /*final DatabaseReference databaseReference = FirebaseDatabase.getInstance()
                .getReference();*/


        FirebaseMessaging fm = FirebaseMessaging.getInstance();
        fm.send(new RemoteMessage.Builder("722586070974" + "@gcm.googleapis.com")
                .setMessageId(Integer.toString(count++/*msgId.incrementAndGet()*/))
                .addData("my_message", edMessage.getText().toString())
                .build());


        mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                .getRef()
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(room_type_1)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_1 + " exists");
                            room = room_type_1;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_1)
                                    .push()
                                    .setValue(chatModel);
                        } else if (dataSnapshot.hasChild(room_type_2)) {
                            Log.e(TAG, "sendMessageToFirebaseUser: " + room_type_2 + " exists");
                            room = room_type_2;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_2)
                                    .push()
                                    .setValue(chatModel);
                        } else {
                            Log.e(TAG, "sendMessageToFirebaseUser: success");
                            room = room_type_1;
                            ChatModel chatModel = new ChatModel(userModel.getId(), userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
                            mFirebaseDatabaseReference.child(CHAT_REFERENCE)
                                    .child(room_type_1)
                                    .push()
                                    .setValue(chatModel);
                        }
                        saveOrUpdateChatUser(room, frontUserID, "1");
                        sendMessage(edMessage.getText().toString());
                        edMessage.setText(null);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // Unable to send message.
                    }
                });
    }

    private void saveOrUpdateChatUser(String room, String frontUserID, String status) {
        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("chat_name", room);
        hmpCredentials.put("chat_receiver", frontUserID);
        hmpCredentials.put("chat_status", status);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(String.class);
        urlManager.setPriority(Request.Priority.NORMAL);
        urlManager.setCachable(false);
        urlManager.setMethod(Request.Method.POST);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.INSERT_CHAT_RECORD);

        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {

            }

            @Override
            public void onError(String errorMsg) {

            }
        }, urlManager);
    }

    private void updateChatUser(String room, String status) {
        HashMap<String, String> hmpCredentials;
        hmpCredentials = new HashMap<String, String>();
        hmpCredentials.put("chat_name", room);
        hmpCredentials.put("chat_status", status);

        URLManager urlManager = new URLManager();
        urlManager.setClassName(String.class);
        urlManager.setPriority(Request.Priority.NORMAL);
        urlManager.setCachable(false);
        urlManager.setMethod(Request.Method.POST);
        urlManager.setParams(hmpCredentials);
        urlManager.setFinalUrl(URLConstants.UPDATE_CHAT_RECORD);

        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {

            }

            @Override
            public void onError(String errorMsg) {

            }
        }, urlManager);
    }

    /**
     * Enviar msg de texto simples para chat
     */
   /*private void sendMessageFirebase(){
        ChatModel model = new ChatModel(userModel,edMessage.getText().toString(), Calendar.getInstance().getTime().getTime()+"",null);
        mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(userModel.getId()+"_"+frontUserID).push().setValue(model);
        edMessage.setText(null);
    }*/

    DatabaseReference mRef;
    ChatFirebaseAdapter mChatFirebaseAdapter;

    /**
     * Ler collections chatmodel Firebase
     */
    private void readFirebaseMessages(){
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();//.child(CHAT_REFERENCE);

        mFirebaseDatabaseReference.child(CHAT_REFERENCE).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.hasChild(frontUserID+"_"+userModel.getId())){
                    mRef = mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(frontUserID+"_"+userModel.getId());
                } else if(dataSnapshot.hasChild(userModel.getId() + "_" +frontUserID)) {
                    mRef = mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(userModel.getId() + "_" +frontUserID);
                } else {
                    mRef = mFirebaseDatabaseReference.child(CHAT_REFERENCE).child(userModel.getId() + "_" +frontUserID);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mChatFirebaseAdapter = new ChatFirebaseAdapter(mRef, userModel.getId(), StartChatActivity.this);
                        mChatFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onItemRangeInserted(int positionStart, int itemCount) {
                                super.onItemRangeInserted(positionStart, itemCount);
                                int friendlyMessageCount = mChatFirebaseAdapter.getItemCount();
                                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                                if (lastVisiblePosition == -1 ||
                                        (positionStart >= (friendlyMessageCount - 1) &&
                                                lastVisiblePosition == (positionStart - 1))) {
                                    rvListMessage.scrollToPosition(positionStart);
                                }
                            }
                        });
                        rvListMessage.setLayoutManager(mLinearLayoutManager);
                        rvListMessage.setAdapter(mChatFirebaseAdapter);
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    private void showImageSendProgress() {

    }

    /**
     * Verificar se usuario está logado
     */
    private void verifyUserIfLogin(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }else{
            userModel = new UserModel(mFirebaseUser.getDisplayName(), mFirebaseUser.getPhotoUrl().toString(), mFirebaseUser.getUid(), mFirebaseUser.getEmail());
            readFirebaseMessages();
        }
    }

    /**
     * bind views com Java API
     */
    private void bindViews(){
        contentRoot = findViewById(R.id.contentRoot);
        edMessage = (EmojiconEditText)findViewById(R.id.editTextMessage);
        btSendMessage = (ImageView)findViewById(R.id.buttonMessage);
        btSendMessage.setOnClickListener(this);
        btEmoji = (ImageView)findViewById(R.id.buttonEmoji);
        emojIcon = new EmojIconActions(this,contentRoot,edMessage,btEmoji);
        emojIcon.ShowEmojIcon();
        rvListMessage = (RecyclerView)findViewById(R.id.messageRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
    }

    /**
     * Sign Out no login
     */
    private void signOut(){
        mFirebaseAuth.signOut();
        Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
