package bamfaltech.kutumb;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.actionbar.DetailsMaterialActionBar;
import bamfaltech.kutumb.adapter.NothingSelectedSpinnerAdapter;
import bamfaltech.kutumb.kutumbvolley.VolleyFeedManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.managers.HttpManager;
import bamfaltech.kutumb.managers.URLManager;
import bamfaltech.kutumb.models.NewSocietyModel;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.RWAListModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.services.Serializer;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;
import bamfaltech.kutumb.util.URLConstants;
import bamfaltech.kutumb.util.Util;


/**
 * Created by gaurav.bansal1 on 14/10/16.
 */
public class BasicInfoActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private View containerView;
    private EditText mDisplayName;
    private EditText mMobileNumber;
    private EditText mSecurityCode;
    private Bundle profileExtra;
    private Spinner towerSpinner;
    private Spinner floorSpinner;
    private Spinner flatSpinner;

    private AutoCompleteTextView mCityname;
    private AutoCompleteTextView mSocietyName;

    private TextView societyNotFound;

    private LinearLayout flatLayoutContainer;


    private ImageView mProfilePicture;
    private UserModel currentUser;
    private boolean fromStart = false;
    private String TAG = "BasicInfoActivity";
    private static final String REQUIRED = "Required";


    HttpPost httppost;
    StringBuffer buffer;
    HttpClient httpclient;
    List<NameValuePair> nameValuePairs;
    String user_name = "";

    public static String[] Societies;
    private ArrayAdapter<String> societyAdapter;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basic_info_activity);

        setupToolbar();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        containerView = (View) findViewById(R.id.main_layout);
        profileExtra = getIntent().getExtras();
        if(profileExtra != null) {
            fromStart = profileExtra.getBoolean("fromStart");
        }
        currentUser = FirebaseManager.getInstance().getCurrentUser();

        mCityname = (AutoCompleteTextView) findViewById(R.id.city);
        mSocietyName = (AutoCompleteTextView) findViewById(R.id.society);

        flatLayoutContainer = (LinearLayout) findViewById(R.id.flat_container);
        societyNotFound = (TextView) findViewById(R.id.society_not_found);
        societyNotFound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        /*societyAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, Societies);
        mSocietyName.setThreshold(1);
        mSocietyName.setAdapter(societyAdapter);
        mSocietyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });*/
        //getSocietyNames();

        initUI();
        TextView submitButton = (TextView) findViewById(R.id.submit_button);
        submitButton.setOnClickListener(this);
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(this);

        dialog.setTitle("Add your society");

        dialog.setContentView(R.layout.add_new_society);

        final EditText city = (EditText) dialog.findViewById(R.id.city_name_text);
        final EditText society = (EditText) dialog.findViewById(R.id.society_name_text);
        final EditText mobile = (EditText) dialog.findViewById(R.id.your_phone_number_text);


        dialog.findViewById(R.id.submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cityName = city.getText().toString();
                String societyName = society.getText().toString();
                String mobileNumber = mobile.getText().toString();

                if (TextUtils.isEmpty(cityName)) {
                    //city.setError(REQUIRED);
                    Toast.makeText(BasicInfoActivity.this, "Enter city Name.", Toast.LENGTH_LONG).show();
                    return;
                } else if (TextUtils.isEmpty(societyName)) {
                    //society.setError(REQUIRED);
                    Toast.makeText(BasicInfoActivity.this, "Enter Society Name.", Toast.LENGTH_LONG).show();
                    return;
                } else if(TextUtils.isEmpty(mobileNumber)) {
                    Toast.makeText(BasicInfoActivity.this, "Enter Phone Number.", Toast.LENGTH_LONG).show();
                    return;
                }

                writeNewSociety(cityName, societyName);

                Toast.makeText(BasicInfoActivity.this, "New society request has been added. We will verify and add the same in 24 hours", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void writeNewSociety(String cityName, String societyName) {
        // Create new post at /user-posts/$userid/$postid and at
        // /posts/$postid simultaneously
        String key = mDatabase.child("carpool").push().getKey();
        NewSocietyModel carPoolModel = new NewSocietyModel(cityName, societyName);

        Map<String, Object> postValues = carPoolModel.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/new-society-requests/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
    }

    private void setupToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mToolbar.setContentInsetsAbsolute(0, 0);
        mToolbar.removeAllViews();
        DetailsMaterialActionBar mDetailsMaterialActionBar = new DetailsMaterialActionBar(this);
        mDetailsMaterialActionBar.setParams(null, getPageTitle());
        mToolbar.addView(mDetailsMaterialActionBar);
        /*activity.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        ((TextView) mDetailsMaterialActionBar.findViewById(R.id.title)).setText(getPageTitle());
        ((ImageView) mDetailsMaterialActionBar.findViewById(R.id.menu_icon)).setImageResource(R.drawable.ic_actionbar_back);

        mDetailsMaterialActionBar.setToolbar(mToolbar);
    }

    private boolean doubleBackToExitPressedOnce = false;
    public void performDoubleExit(){

        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        showMySnackbar(this, "Press back again to exit", false);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;

            }
        }, 5000);

    }

    private void showMySnackbar(Context context, String message, boolean isLongDuration) {
        Snackbar snackbar = null;
        if(!isLongDuration) {
            snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), "" + message, Snackbar.LENGTH_SHORT);
        } else {
            snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), "" + message, Snackbar.LENGTH_LONG);
        }
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.snack_bar_background_color));
        snackbar.show();
    }

    private String getPageTitle() {
        return "Edit Profile";
    }

    private void initUI(){

        //getTowerList("1200001");
        getRwaInfo();

        RelativeLayout towerLayout = (RelativeLayout) containerView.findViewById(R.id.tower);
        RelativeLayout floorLayout = (RelativeLayout) containerView.findViewById(R.id.floor);
        RelativeLayout flatLayout = (RelativeLayout) containerView.findViewById(R.id.flatno);

        towerSpinner = (Spinner)towerLayout.findViewById(R.id.spinnerView);

        floorSpinner = (Spinner) floorLayout.findViewById(R.id.spinnerView);

        /*ArrayAdapter<String> towerListAdapter = new ArrayAdapter<String>(this, R.layout.spinner_list_item, Constants.towerList);
        towerListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        towerSpinner.setAdapter(towerListAdapter);*/
//        towerSpinner.setOnItemClickListener(this);

        flatSpinner = (Spinner)flatLayout.findViewById(R.id.spinnerView);

        /*ArrayAdapter<String> flatListAdapter = new ArrayAdapter<String>(this, R.layout.spinner_list_item, Constants.flatList);
        towerListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        flatSpinner.setAdapter(flatListAdapter);*/
        //      flatSpinner.setOnItemClickListener(this);
        mDisplayName = (EditText) containerView.findViewById(R.id.flat_owner_name);
        mDisplayName.setText(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
        mDisplayName.clearFocus();

        String urlProfileImg = currentUser.getPhoto_profile();
        /*mProfilePicture = (ImageView) containerView.findViewById(R.id.select_img);
        Util.bindCircularImage(mProfilePicture, urlProfileImg);*/

        mMobileNumber = (EditText) containerView.findViewById(R.id.mobile_number);
        mSecurityCode = (EditText) containerView.findViewById(R.id.security_code);

        if(!fromStart) {
            mMobileNumber.setText(currentUser.getMobileNumber());
            mSecurityCode.setVisibility(View.GONE);
        }
    }

    List<String> flatNumber;
    List<String> towerNumbers;

    List<String> societyList;
    List<String> societyIDList;
    List<RWADetailModel.RWA> filteredRWAList;

    RWADetailModel.RWA userRWAModel;

    private void getRwaInfo() {

        Util.showPB(this, "Loading...");
        URLManager urlManager = new URLManager();
        urlManager.setClassName(RWADetailModel.class);
        urlManager.setPriority(Request.Priority.HIGH);
        urlManager.setCachable(true);
        urlManager.setMethod(Request.Method.GET);
        urlManager.setFinalUrl(URLConstants.GET_RWA_DETAILS);

        VolleyFeedManager.getInstance().startFeedRetreival(new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                Util.hidePB();
                userRWAModel = new RWADetailModel.RWA();
                final RWADetailModel rwaDetailModel = (RWADetailModel) object;
                final ArrayList<RWADetailModel.RWA> RWAList = rwaDetailModel.getArrListBusinessObj();
                societyList = new ArrayList<String>();
                List<String> cityList = new ArrayList<String>();

                for(RWADetailModel.RWA rwa : RWAList) {
                    cityList.add(rwa.getRwacity());
                    societyList.add(rwa.getRwaname());
                }
                LinkedHashSet<String> hashSet = new LinkedHashSet<String>();
                hashSet.addAll(cityList);
                cityList.clear();
                cityList.addAll(hashSet);
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(BasicInfoActivity.this,
                        android.R.layout.simple_dropdown_item_1line, Util.convertListToStringArray(cityList));
                mCityname.setThreshold(0);
                mCityname.setAdapter(adapter);
                mCityname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                        societyList = new ArrayList<String>();
                        societyIDList = new ArrayList<String>();
                        filteredRWAList = new ArrayList<RWADetailModel.RWA>();
                        userRWAModel.setRwacity(adapter.getItem(position));
                        for(RWADetailModel.RWA rwa : RWAList) {
                            if(rwa.getRwacity().equalsIgnoreCase(adapter.getItem(position))) {
                                filteredRWAList.add(rwa);
                                societyList.add(rwa.getRwaname());
                                societyIDList.add(rwa.getRwaid());
                            }
                        }
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(BasicInfoActivity.this,
                                android.R.layout.simple_dropdown_item_1line, Util.convertListToStringArray(societyList));
                        mSocietyName.setThreshold(0);
                        mSocietyName.setAdapter(adapter);
                        mSocietyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                userRWAModel.setRwaname(adapter.getItem(position));
                                int index = societyList.indexOf(adapter.getItem(position));
                                userRWAModel.setRwaid(societyIDList.get(index));
                                userRWAModel.setThirteenFloorBehaviour(filteredRWAList.get(index).getThirteenFloorBehaviour());
                                userRWAModel.setRwaCountInitial(filteredRWAList.get(index).getRwaCountInitial());
                                RWADetailModel.RWA rwa = filteredRWAList.get(index);
                                //RWADetailModel.RWA rwa = RWAList.get(0);
                                KutumbApplication.getInstance().setCurrentRWA(rwa);
                                DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_CURRENT_RWA_DETAILS, Serializer.serialize(rwa), true);

                                flatLayoutContainer.setVisibility(View.VISIBLE);
                                String towerList = rwa.getRwaTower();
                                String towers[] = towerList.split(",");
                                towerNumbers = new ArrayList<String>(Arrays.asList(towers));

                                ArrayAdapter<String> towerListAdapter = new ArrayAdapter<String>(BasicInfoActivity.this, R.layout.spinner_list_item, towerNumbers);
                                towerListAdapter.setDropDownViewResource(R.layout.simple_centered_dropdown_item);
                                towerSpinner.setPrompt("Select Tower");
                                towerSpinner.setAdapter(new NothingSelectedSpinnerAdapter(towerListAdapter, R.layout.nothing_selected_spinner_text, BasicInfoActivity.this, "Tower"));
                                //towerSpinner.setAdapter(towerListAdapter);

                                int towerFloor = Integer.parseInt(rwa.getRwaFloor());
                                final int towerFlat = Integer.parseInt(rwa.getRwaFlat());

                                final List<String> floorNumber = new ArrayList<String>();
                                for(int i=0; i<=towerFloor; i++) {
                                    if(i ==0) {
                                        floorNumber.add("G");
                                    } else if( i == 13) {
                                        if(userRWAModel.getThirteenFloorBehaviour().equalsIgnoreCase("absent")) {
                                            //add nothing
                                        } else if(userRWAModel.getThirteenFloorBehaviour().equalsIgnoreCase("present_with_A")) {
                                            floorNumber.add("12A");
                                        }
                                    } else {
                                        floorNumber.add((i) + "");
                                    }
                                }
                                ArrayAdapter<String> floorListAdapter = new ArrayAdapter<String>(BasicInfoActivity.this, R.layout.spinner_list_item, floorNumber);
                                floorListAdapter.setDropDownViewResource(R.layout.simple_centered_dropdown_item);
                                //floorSpinner.setAdapter(floorListAdapter);
                                floorSpinner.setAdapter(new NothingSelectedSpinnerAdapter(floorListAdapter, R.layout.nothing_selected_spinner_text, BasicInfoActivity.this, "Floor"));
                                floorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        int floor = i -1;//Integer.parseInt(floorNumber.get(i));
                                        flatNumber = new ArrayList<String>();
                                        for (int j = 1; j <= towerFlat; j++) {
                                            if(floor < 0) {
                                                return;
                                            }
                                            if(floor == 0) {
                                                flatNumber.add("00"+j);
                                            } else if(floor >= 13) {
                                                if(userRWAModel.getThirteenFloorBehaviour().equalsIgnoreCase("absent")) {
                                                    //add nothing
                                                    flatNumber.add(((floor+1) * 100 + Integer.parseInt(userRWAModel.getRwaCountInitial()) + j) + "");
                                                } else if(userRWAModel.getThirteenFloorBehaviour().equalsIgnoreCase("present_with_A")) {
                                                    if(floor == 13) {
                                                        flatNumber.add(((floor-1) * 100 + Integer.parseInt(userRWAModel.getRwaCountInitial()) + j) + "A");
                                                    } else {
                                                        flatNumber.add((floor * 100 + Integer.parseInt(userRWAModel.getRwaCountInitial()) + j) + "");
                                                    }

                                                }
                                            } else {
                                                flatNumber.add((floor * 100 + Integer.parseInt(userRWAModel.getRwaCountInitial()) + j) + "");
                                            }
                                            ArrayAdapter<String> flatListAdapter = new ArrayAdapter<String>(BasicInfoActivity.this, R.layout.spinner_list_item, flatNumber);
                                            flatListAdapter.setDropDownViewResource(R.layout.simple_centered_dropdown_item);
                                            flatSpinner.setAdapter(new NothingSelectedSpinnerAdapter(flatListAdapter, R.layout.nothing_selected_spinner_text, BasicInfoActivity.this, "Flat"));
                                            //flatSpinner.setAdapter(flatListAdapter);
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                                towerSpinner.setSelection(0);
                                floorSpinner.setSelection(0);
                            }
                        });
                        mSocietyName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                            @Override
                            public void onFocusChange(View view, boolean b) {
                                if(b) {
                                    mSocietyName.showDropDown();
                                }
                            }
                        });
                    }
                });
                mCityname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        if(b) {
                            mCityname.showDropDown();
                        }
                    }
                });
                mCityname.clearFocus();
            }

            @Override
            public void onError(String errorMsg) {
                Util.hidePB();
                Toast.makeText(BasicInfoActivity.this, "Server error, Please try again !", Toast.LENGTH_LONG).show();
            }
        }, urlManager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_button:
                if(TextUtils.isEmpty(mCityname.getText())) {
                    mCityname.setError("Please enter city Name");
                    return;
                } else if(TextUtils.isEmpty(mSocietyName.getText())) {
                    mSocietyName.setError("Please enter Society Name");
                    return;
                } else if(towerSpinner.getSelectedItemPosition()  <= 0 || flatSpinner.getSelectedItemPosition() <= 0) {
                    Toast.makeText(this, "Please select tower or flat", Toast.LENGTH_SHORT).show();
                    return;
                }
                UserModel userModel = new UserModel();
                userModel.setId(FirebaseAuth.getInstance().getCurrentUser().getUid());
                userModel.setName(mDisplayName.getText().toString());
                userModel.setEmailId(FirebaseAuth.getInstance().getCurrentUser().getEmail());
                String photoProfile = null;
                if(Constants.LOGIN_TYPE == Constants.LOGIN_TYPES.FACEBOOK.ordinal()) {
                    photoProfile = DeviceResourceManager.getInstance().getDataFromSharedPref("facebook_profile", null, false);

                }
                if(!TextUtils.isEmpty(photoProfile)) {
                    userModel.setPhoto_profile(photoProfile);
                } else {
                    userModel.setPhoto_profile(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString());
                }

                if(towerNumbers != null && flatNumber != null) {
                    if(towerSpinner.getSelectedItemPosition()  > 0 && flatSpinner.getSelectedItemPosition() > 0) {
                        userRWAModel.setRwaFlatNumber(towerNumbers.get(towerSpinner.getSelectedItemPosition() - 1) + flatNumber.get(flatSpinner.getSelectedItemPosition() - 1));
                        userModel.setRwa(userRWAModel);
                        userModel.setFlatNumber(towerNumbers.get(towerSpinner.getSelectedItemPosition() - 1) + flatNumber.get(flatSpinner.getSelectedItemPosition() - 1));
                    } else {
                        Toast.makeText(this, "Please select tower or flat", Toast.LENGTH_LONG).show();
                    }
                }
                userModel.setFCMRegID(Util.getRegistrationId(Constants.PREFERENCE_REGISTRATION_ID));
                if(Util.isEmpty(mMobileNumber.getText().toString())) {
                    Toast.makeText(this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }
                userModel.setMobileNumber(mMobileNumber.getText().toString());

                /*if(fromStart && Util.isEmpty(mSecurityCode.getText().toString())) {
                    Toast.makeText(this, "Please enter security code of your flat", Toast.LENGTH_SHORT).show();
                    return;
                }*/


                KutumbApplication.getInstance().setCurrentUserModel(userModel);
                FirebaseManager manager = FirebaseManager.getInstance();
                //manager.initializeFirebaseAuth(this);
                manager.completeProfile(userModel);

                if(fromStart) {
                    Intent intent = new Intent(this, KutumbActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                    startActivity(intent);
                }

                this.finish();

                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
