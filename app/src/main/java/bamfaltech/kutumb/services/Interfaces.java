package bamfaltech.kutumb.services;

import bamfaltech.kutumb.models.FileModel;
import bamfaltech.kutumb.models.UserModel;

/**
 * Created by gaurav.bansal1 on 08/02/17.
 */

public class Interfaces {

    public interface OnUserModelRetrieved {
        public void onRetreivalComplete(UserModel userModel);

        public void onError(String errormsg);
    }

    public interface OnRetrievalCompleteListener {
        public void onRetrievalCompleted(Object object);
        public void onError(String errorMsg);
    }

    public interface OnUploadFileCompleteListener {
        public void onUploadCompleted(FileModel fileModel);
        public void onError(String error);
    }

    public interface NotificationChangedListener {
        public void onNotificationChanged(int newNotificationCount);
    }
}
