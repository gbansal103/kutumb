/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package bamfaltech.kutumb.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

//import com.applozic.mobicomkit.api.notification.MobiComPushReceiver;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Map;

import bamfaltech.kutumb.BasicInfoActivity;
import bamfaltech.kutumb.KutumbActivity;
import bamfaltech.kutumb.KutumbApplication;
import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.fragment.PersonalChatListFragment;
import bamfaltech.kutumb.fragment.TabContainerFragment;
import bamfaltech.kutumb.managers.ApplicationLifecycleManager;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.NotificationModel;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;

import static bamfaltech.kutumb.util.Constants.NOTIFICATION_PURPOSE_NODETAIL;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        /*if (MobiComPushReceiver.isMobiComPushNotification(remoteMessage.getData())) {
            MobiComPushReceiver.processMessageAsync(this, remoteMessage.getData());
            return;
        }*/

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            String from = data.get("senderID");
            String fromName = data.get("senderName");
            String message = data.get("message");
            String sendPurpose = data.get("sendPurpose");
            String contentKey = data.get("contentKey");
            if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_CHAT)) {
                if(ApplicationLifecycleManager.isAppInForeground()) {
                    Activity activity = ApplicationLifecycleManager.getCurrentActivity();
                    if(activity instanceof KutumbActivity) {
                        sendNotification(from, fromName, message, sendPurpose, contentKey);
                        final KutumbActivity kutumbActivity = (KutumbActivity) activity;
                        final Fragment currentFragment = kutumbActivity.getCurrentFragment();
                        if(currentFragment != null && currentFragment instanceof TabContainerFragment) {
                            kutumbActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //kutumbActivity.recreate();
                                    Intent intnet = new Intent("bamfaltech.kutumb.chatUpdateReceiver");
                                    sendBroadcast(intnet);
                                    //((PersonalChatListFragment)((TabContainerFragment)currentFragment).getAdapter().getItem(1)).update();
                                }
                            });

                        }
                    } else {
                        //do not send Notification
                        DeviceResourceManager.getInstance().addToSharedPref(Constants.PREFERENCE_UPDATE_CHAT_LIST, true, true);
                    }
                } else {
                    sendNotification(from, fromName, message, sendPurpose, contentKey);
                }
                //FirebaseManager.getInstance().saveNotification(getNotificiationModel(message, sendPurpose, contentKey));
            } else {
                sendNotification(from, fromName, message, sendPurpose, contentKey);
                FirebaseManager.getInstance().saveNotification(getNotificiationModel(message, sendPurpose, contentKey));
            }
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            sendNotification("", "" ,remoteMessage.getNotification().getBody(), NOTIFICATION_PURPOSE_NODETAIL, null);
            FirebaseManager.getInstance().saveNotification(getNotificiationModel(remoteMessage));
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    private NotificationModel getNotificiationModel(String message, String notificationPurpose, String notificationContentkey) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setNotificationMsg(message);
        notificationModel.setNotificationPurpose(notificationPurpose);
        notificationModel.setNotificationContentKey(notificationContentkey);
        return notificationModel;
    }

    private NotificationModel getNotificiationModel(RemoteMessage remoteMessage) {
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.setNotificationMsg(remoteMessage.getNotification().getBody());
        return notificationModel;
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String from, String fromName, String messageBody, String sendPurpose, String contentKey) {
        Intent intent;
        if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_CHAT)) {
            intent = new Intent(this, StartChatActivity.class);
            intent.putExtra("front_user_id", from);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_POST_LIKE)) {
            intent = new Intent(this, KutumbActivity.class);
            intent.putExtra("fragmentLaunch","postDetail");
            intent.putExtra("contentKey", contentKey);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_UTILITY_INTEREST)) {
            intent = new Intent(this, KutumbActivity.class);
            intent.putExtra("fragmentLaunch","utilityDetail");
            intent.putExtra("contentKey", contentKey);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else {
            intent = new Intent(this, KutumbActivity.class);
            intent.putExtra("fragmentLaunch","notificationFragment");
            intent.putExtra("contentKey", contentKey);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(fromName)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_CHAT)) {
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_POST_LIKE)) {
            notificationManager.notify(1 /* ID of notification */, notificationBuilder.build());
        } else if(sendPurpose.equalsIgnoreCase(Constants.NOTIFICATION_PURPOSE_UTILITY_INTEREST)) {
            notificationManager.notify(2 /* ID of notification */, notificationBuilder.build());
        } else {
            notificationManager.notify(3 /* ID of notification */, notificationBuilder.build());
        }
        //notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
