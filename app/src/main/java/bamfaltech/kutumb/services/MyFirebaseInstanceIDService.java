package bamfaltech.kutumb.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;

/*import com.applozic.mobicomkit.api.account.register.RegisterUserClientService;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;*/

/**
 * Created by gaurav.bansal1 on 27/12/16.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private String TAG = "FirebaseInstanceService";
    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        Log.i(TAG, "storeRegIDInSharedPrefs");
        Util.storeRegistrationId(Constants.PREFERENCE_REGISTRATION_ID, refreshedToken);
    }

}
