package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.NotificationModel;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 14/02/17.
 */

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    private TextView notificationText;
    private ImageView notificationImage;
    private LinearLayout notificationContainer;


    public NotificationViewHolder(View itemView) {
        super(itemView);
        notificationText = (TextView)itemView.findViewById(R.id.notif_text);
        notificationImage = (ImageView)itemView.findViewById(R.id.notif_image);
        notificationContainer = (LinearLayout) itemView.findViewById(R.id.container);
    }

    public void bindToNotification(NotificationModel notificationModel, View.OnClickListener onClickListener) {
        notificationText.setText(notificationModel.getNotificationMsg());
        if(!TextUtils.isEmpty(notificationModel.getNotificationArtwork())) {
            Util.bindCircularImage(notificationImage, notificationModel.getNotificationArtwork());
        } else {
            notificationImage.setImageDrawable(notificationImage.getResources().getDrawable(R.mipmap.ic_launcher));
        }
        notificationContainer.setOnClickListener(onClickListener);
    }
}
