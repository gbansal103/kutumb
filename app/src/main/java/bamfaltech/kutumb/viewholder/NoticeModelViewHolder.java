package bamfaltech.kutumb.viewholder;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.FileModel;
import bamfaltech.kutumb.models.NoticeModel;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 01/03/17.
 */

public class NoticeModelViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public TextView bodyView;
    public ImageView noticePic;


    public NoticeModelViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        bodyView = (TextView) itemView.findViewById(R.id.post_body);
        noticePic = (ImageView) itemView.findViewById(R.id.notice_pic);
    }

    public void bindToNotice(NoticeModel noticeModel, View.OnClickListener noticePicClickListener, Fragment fragment) {
        titleView.setVisibility(View.VISIBLE);
        if(!TextUtils.isEmpty(noticeModel.title)) {
            titleView.setText(noticeModel.title);
        } else {
            titleView.setText("General");
        }
        bodyView.setText(noticeModel.body);
        noticePic.setOnClickListener(noticePicClickListener);

        FileModel file = noticeModel.getFileModel();
        if(file != null) {
            Util.bindImage(noticePic, file.getUrl_file());
        } else {
            noticePic.setVisibility(View.GONE);
        }

    }
}
