package bamfaltech.kutumb.viewholder;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.fragment.TopicFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.CarPoolModel;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 07/03/17.
 */

public class CarPoolModelViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public ImageView authorPic;
    public TextView authorView;
    public TextView authorFlatNo;
    public TextView carPoolSource;
    public TextView carPoolDestination;
    public TextView sourceLeaveTime;
    public TextView destinationLeaveTime;
    public TextView mobileNumber;
    public ImageView chatCarOwner;

    public LinearLayout likeCommentContainer;

    public CarPoolModelViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        authorPic = (ImageView) itemView.findViewById(R.id.post_author_photo);
        authorView = (TextView) itemView.findViewById(R.id.post_author);
        authorFlatNo = (TextView) itemView.findViewById(R.id.post_author_flat_no);
        carPoolSource = (TextView) itemView.findViewById(R.id.car_pool_source);
        carPoolDestination = (TextView) itemView.findViewById(R.id.car_pool_destination);
        sourceLeaveTime = (TextView) itemView.findViewById(R.id.car_pool_leaving_time);
        destinationLeaveTime = (TextView) itemView.findViewById(R.id.car_pool_exit_time);
        mobileNumber = (TextView) itemView.findViewById(R.id.car_pool_owner_mobile);
        chatCarOwner = (ImageView) itemView.findViewById(R.id.chat_pool_owner);

    }

    public void bindToCarPoolModel(CarPoolModel post, View.OnClickListener chatClickListener, View.OnClickListener removeClickListener, Fragment fragment) {

        authorFlatNo.setText(post.author_flat_no);
        Glide.with(authorPic.getContext()).load(post.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(authorPic.getContext())).override(40,40).into(authorPic);

        authorView.setText(Util.getAllCapitalWordsString(post.author));

        carPoolSource.setText(post.getCarPoolSource());
        carPoolDestination.setText(post.getCarPoolDestination());
        sourceLeaveTime.setText(post.getCarPoolLeaveTimeSource());
        destinationLeaveTime.setText(post.getCarPoolLeaveTimeDestination());
        mobileNumber.setText(post.getCarPoolOwnerPhone());

        if(post.getUid().equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
            chatCarOwner.setImageDrawable(chatCarOwner.getContext().getResources().getDrawable(R.drawable.ic_delete_black_24dp));
            chatCarOwner.setOnClickListener(removeClickListener);
        } else {
            chatCarOwner.setImageDrawable(chatCarOwner.getContext().getResources().getDrawable(R.drawable.ic_chat_black_24dp));
            chatCarOwner.setOnClickListener(chatClickListener);
        }

    }

}
