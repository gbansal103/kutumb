package bamfaltech.kutumb.viewholder;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.StartChatActivity;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.fragment.TopicFragment;
import bamfaltech.kutumb.fragment.UtilityFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.util.Util;

public class PostViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public ImageView authorPic;
    public TextView authorView;
    public TextView authorFlatNo;
    public ImageView starView;
    public TextView numStarsView;
    public TextView bodyView;
    public LinearLayout mBottomLayout;
    public LinearLayout mLikeLayout;
    public LinearLayout mCommentLayout;
    public TextView mLikeText;
    public TextView mCommentText;
    public ImageView mLikeImage;
    public ImageView mCommentImage;
    public View mSeperator;
    public View mSeperator1;
    public TextView mLikeCount;
    public TextView mCommentCount;
    public LinearLayout likeCommentContainer;
    public ImageView chatPostOwner;


    public PostViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        authorPic = (ImageView) itemView.findViewById(R.id.post_author_photo);
        authorView = (TextView) itemView.findViewById(R.id.post_author);
        authorFlatNo = (TextView) itemView.findViewById(R.id.post_author_flat_no);
        starView = (ImageView) itemView.findViewById(R.id.star);
        numStarsView = (TextView) itemView.findViewById(R.id.post_num_stars);
        bodyView = (TextView) itemView.findViewById(R.id.post_body);
        mLikeCount = (TextView) itemView.findViewById(R.id.likes_count);
        mBottomLayout = (LinearLayout) itemView.findViewById(R.id.bottom_layout);
        mLikeLayout = (LinearLayout) itemView.findViewById(R.id.like_layout);
        mCommentLayout = (LinearLayout) itemView.findViewById(R.id.comment_layout);
        mLikeText = (TextView) itemView.findViewById(R.id.like_text);
        mCommentText = (TextView) itemView.findViewById(R.id.comment_text);
        mLikeImage = (ImageView) itemView.findViewById(R.id.crown_favorite);
        mCommentImage = (ImageView) itemView.findViewById(R.id.crown_comment);
        mSeperator = (View) itemView.findViewById(R.id.seperator_line);
        //mSeperator1 = (View) itemView.findViewById(R.id.seperator_line_up);
        mCommentCount = (TextView) itemView.findViewById(R.id.comments_count);
        likeCommentContainer = (LinearLayout) itemView.findViewById(R.id.likeCommentContainer);
        chatPostOwner = (ImageView) itemView.findViewById(R.id.chat_post_owner);
    }

    public void bindToPost(final DatabaseReference postRef, final Post post, View.OnClickListener userImageClickListener, View.OnClickListener starClickListener, View.OnClickListener commentClickListener, View.OnClickListener topicClickListener, final Fragment fragment) {
        if(!TextUtils.isEmpty(post.title) && !(fragment instanceof TopicFragment)) {
            titleView.setVisibility(View.VISIBLE);
            titleView.setText(post.title);
            titleView.setOnClickListener(topicClickListener);
        } else {
            titleView.setVisibility(View.GONE);
        }
        authorFlatNo.setText(post.author_flat_no);
        Glide.with(authorPic.getContext()).load(post.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(authorPic.getContext())).override(40,40).into(authorPic);

        authorPic.setOnClickListener(userImageClickListener);

        authorView.setText(Util.getAllCapitalWordsString(post.author));

        int likeCount = post.starCount;
        if(likeCount <= 1) {
            mLikeCount.setText(String.valueOf(likeCount) + " Like");
        } else {
            mLikeCount.setText(String.valueOf(likeCount) + " Likes");
        }

        int commentCount = post.commentCount;
        if(commentCount <= 1) {
            mCommentCount.setText(String.valueOf(commentCount) + " Comment");
        } else {
            mCommentCount.setText(String.valueOf(commentCount) + " Comments");
        }

        bodyView.setText(post.body);

        //starView.setOnClickListener(starClickListener);
        mLikeLayout.setOnClickListener(starClickListener);
        mCommentLayout.setOnClickListener(commentClickListener);

        final Context context = fragment.getActivity();
        final UserModel currentUser = FirebaseManager.getInstance().getCurrentUser();
        if(post.uid.equalsIgnoreCase(currentUser.getId())) {
            chatPostOwner.setImageDrawable(chatPostOwner.getContext().getResources().getDrawable(R.drawable.ic_delete_black_24dp));
            chatPostOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showConfirmationDialog(postRef, currentUser.getId(), currentUser.getRwa().getRwaid(), context);
                }
            });
        } else {
            chatPostOwner.setImageDrawable(chatPostOwner.getContext().getResources().getDrawable(R.drawable.ic_chat_black_24dp));
            chatPostOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, StartChatActivity.class);
                    intent.putExtra("front_user_id", post.uid);
                    context.startActivity(intent);
                }
            });
        }
    }

    private void showConfirmationDialog(final DatabaseReference postRef, final String userID, final String societyID, final Context mContext){
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
        builder.setTitle("Delete Post");
        builder.setMessage("Are you sure, do you want to delete the post ?");

        String positiveText = "yes";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "posts", postRef.getKey());
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "user-posts", userID, postRef.getKey());
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "society-posts", societyID, postRef.getKey());
                        Toast.makeText(mContext, "post has been deleted !", Toast.LENGTH_LONG).show();
                    }
                });

        String negativeText = "no";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    private void showUtilityConfirmationDialog(final DatabaseReference postRef, final String userID, final String societyID, final Context mContext){
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.MyDialogTheme);
        builder.setTitle("Delete Utility");
        builder.setMessage("Are you sure, do you want to delete the utility ?");

        String positiveText = "yes";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "utility", postRef.getKey());
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "user-utility", userID, postRef.getKey());
                        FirebaseManager.getInstance().removeNodeById(mDatabase, "society-utility", societyID, postRef.getKey());
                        Toast.makeText(mContext, "utility has been deleted !", Toast.LENGTH_LONG).show();
                    }
                });

        String negativeText = "no";
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // negative button logic
                    }
                });
        AlertDialog dialog = builder.create();
        // display dialog
        dialog.show();
    }

    public void bindToUtility(final DatabaseReference postRef, final Post post, View.OnClickListener starClickListener, View.OnClickListener catagoryClickListener,  View.OnClickListener commentClickListener, Fragment fragment) {
        if(!(fragment instanceof TopicFragment)) {
            titleView.setVisibility(View.VISIBLE);
            titleView.setText(post.title);
            titleView.setOnClickListener(catagoryClickListener);
        } else {
            titleView.setVisibility(View.GONE);
        }

        authorFlatNo.setText(post.author_flat_no + " | " + post.getRwaName());
        Glide.with(authorPic.getContext()).load(post.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(authorPic.getContext())).override(40,40).into(authorPic);
        authorView.setText(Util.getAllCapitalWordsString(post.author));
        int likeCount = post.starCount;
        mLikeCount.setText(String.valueOf(likeCount) + " Interested");

        int commentCount = post.commentCount;
        if(commentCount <= 1) {
            mCommentCount.setText(String.valueOf(commentCount) + " Comment");
        } else {
            mCommentCount.setText(String.valueOf(commentCount) + " Comments");
        }

        bodyView.setText(post.body);

        //starView.setOnClickListener(starClickListener);
        mLikeLayout.setOnClickListener(starClickListener);
        mCommentLayout.setOnClickListener(commentClickListener);

        final Context context = fragment.getActivity();
        final UserModel currentUser = FirebaseManager.getInstance().getCurrentUser();
        if(post.uid.equalsIgnoreCase(currentUser.getId())) {
            chatPostOwner.setImageDrawable(chatPostOwner.getContext().getResources().getDrawable(R.drawable.ic_delete_black_24dp));
            chatPostOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showUtilityConfirmationDialog(postRef, currentUser.getId(), currentUser.getRwa().getRwaid(), context);
                }
            });
        } else {
            chatPostOwner.setImageDrawable(chatPostOwner.getContext().getResources().getDrawable(R.drawable.ic_chat_black_24dp));
            chatPostOwner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, StartChatActivity.class);
                    intent.putExtra("front_user_id", post.uid);
                    context.startActivity(intent);
                }
            });
        }
    }

    public void bindToHeaderPost(Post post, Fragment fragment) {
        if(fragment instanceof UtilityFragment) {
            titleView.setVisibility(View.VISIBLE);
            titleView.setText(post.title);
        }
        authorFlatNo.setText(post.author_flat_no);
        Glide.with(authorPic.getContext()).load(post.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(authorPic.getContext())).override(40,40).into(authorPic);
        authorView.setText(post.author);
        authorFlatNo.setVisibility(View.GONE);
        mSeperator.setVisibility(View.GONE);
//        mSeperator1.setVisibility(View.GONE);
        mLikeLayout.setVisibility(View.GONE);
        starView.setVisibility(View.GONE);
        numStarsView.setVisibility(View.GONE);
        bodyView.setVisibility(View.GONE);
        mBottomLayout.setVisibility(View.GONE);
        likeCommentContainer.setVisibility(View.GONE);
        chatPostOwner.setVisibility(View.GONE);

    }
}
