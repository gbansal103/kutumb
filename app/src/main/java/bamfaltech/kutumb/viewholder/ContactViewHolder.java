package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.ContactModel;

/**
 * Created by gaurav.bansal1 on 08/03/17.
 */

public class ContactViewHolder extends RecyclerView.ViewHolder {

    public TextView mContactPerson;
    public TextView mContactPost;
    public TextView mContactExtn;
    public TextView mContactPhone;

    public ContactViewHolder(View itemView) {
        super(itemView);
        mContactPerson = (TextView) itemView.findViewById(R.id.contact_person);
        mContactPost = (TextView) itemView.findViewById(R.id.contact_post);
        mContactExtn = (TextView) itemView.findViewById(R.id.contact_extn);
        mContactPhone = (TextView) itemView.findViewById(R.id.contact_phone);
    }

    public void bindToContactView(ContactModel.Contact contact) {
        if(!TextUtils.isEmpty(contact.getContactPerson())) {
            mContactPerson.setVisibility(View.VISIBLE);
            mContactPerson.setText(contact.getContactPerson());
        } else {
            mContactPerson.setVisibility(View.GONE);
        }

        mContactPost.setText(contact.getContactPost());

        if(!TextUtils.isEmpty(contact.getContactPhone())) {
            mContactPhone.setText(contact.getContactPhone());
        } else {
            mContactPerson.setText("NA");
        }

        if(!TextUtils.isEmpty(contact.getContactExtn())) {
            mContactExtn.setText(contact.getContactExtn());
        } else {
            mContactExtn.setText("NA");
        }
    }
}
