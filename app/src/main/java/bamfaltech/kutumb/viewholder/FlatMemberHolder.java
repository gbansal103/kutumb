package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import org.w3c.dom.Text;

import java.util.List;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.PeopleObject;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 24/12/16.
 */

public class FlatMemberHolder extends RecyclerView.ViewHolder{

    public RelativeLayout flatMemberContainer;
    public ImageView flatMemberPic;
    public TextView flatMemberName;
    public TextView flatMemberFlat;
    public TextView newChatNotify;
    public TextView flatMemberOwnerFlagText;

    public FlatMemberHolder(View itemView) {
        super(itemView);
        flatMemberContainer = (RelativeLayout) itemView.findViewById(R.id.flat_member_container);
        flatMemberPic = (ImageView) itemView.findViewById(R.id.memebr_pic);
        flatMemberName = (TextView) itemView.findViewById(R.id.flat_member_name);
        flatMemberFlat = (TextView) itemView.findViewById(R.id.flat_member_flatno);
        newChatNotify = (TextView) itemView.findViewById(R.id.new_message_notif);
        flatMemberOwnerFlagText = (TextView) itemView.findViewById(R.id.owner_notify);
    }

    public void bindToFlat(String userID, UserModel userModel, View.OnClickListener starClickListener, BaseKutumbFragment fragment) {
        flatMemberName.setText(userModel.getName());
        Util.bindCircularImage(flatMemberPic, userModel.getPhoto_profile());
        flatMemberFlat.setText(userModel.getFlatNumber());
        if(userID.equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
            flatMemberOwnerFlagText.setVisibility(View.VISIBLE);
        } else {
            flatMemberOwnerFlagText.setVisibility(View.GONE);
        }
        flatMemberContainer.setOnClickListener(starClickListener);
    }

    public void bindToChatList(PeopleObject.People peopleObj, final View.OnClickListener chatItemClickListener) {
        String chatName = peopleObj.getChatName();
        String chatReceiver = peopleObj.getReceiverId();
        String status = peopleObj.getChatStatus();
        String currentUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        String chatIds[] = chatName.split("_");
        String frontUserID = "";
        if(chatIds[0].equalsIgnoreCase(currentUserID)) {
            frontUserID = chatIds[1];
        } else {
            frontUserID = chatIds[0];
        }
        if(chatReceiver.equalsIgnoreCase(currentUserID) && status.equalsIgnoreCase("1")) {
            newChatNotify.setVisibility(View.VISIBLE);
        } else {
            newChatNotify.setVisibility(View.GONE);
        }
        FirebaseManager.getInstance().getFrontUserInfoFromFirebase(frontUserID, new Interfaces.OnUserModelRetrieved() {
            @Override
            public void onRetreivalComplete(UserModel userModel) {
                flatMemberName.setText(userModel.getName());
                flatMemberContainer.setOnClickListener(chatItemClickListener);
                flatMemberFlat.setText(userModel.getFlatNumber());
                Util.bindCircularImage(flatMemberPic, userModel.getPhoto_profile());
            }

            @Override
            public void onError(String errormsg) {

            }
        });
    }

}
