package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.ChatModel;

/**
 * Created by gaurav.bansal1 on 06/02/17.
 */

public class ChatModelViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public ImageView authorPic;
    public TextView authorView;
    public ImageView starView;
    public TextView numStarsView;
    public TextView bodyView;

    public ChatModelViewHolder(View itemView) {
        super(itemView);

        titleView = (TextView) itemView.findViewById(R.id.post_title);
        authorPic = (ImageView) itemView.findViewById(R.id.post_author_photo);
        authorView = (TextView) itemView.findViewById(R.id.post_author);
        starView = (ImageView) itemView.findViewById(R.id.star);
        numStarsView = (TextView) itemView.findViewById(R.id.post_num_stars);
        bodyView = (TextView) itemView.findViewById(R.id.post_body);
    }

    /*public void bindToChatModel(ChatModel chatModel, View.OnClickListener starClickListener) {
        titleView.setText(chatModel.title);
        Glide.with(authorPic.getContext()).load(chatModel.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(authorPic.getContext())).override(40,40).into(authorPic);
        authorView.setText(chatModel.author);
        numStarsView.setText(String.valueOf(chatModel.starCount));
        bodyView.setText(chatModel.body);
    }*/
}
