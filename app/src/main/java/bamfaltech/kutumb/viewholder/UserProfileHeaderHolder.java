package bamfaltech.kutumb.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 02/03/17.
 */

public class UserProfileHeaderHolder extends PostViewHolder {

    private ImageView profilePic;
    private TextView userName;
    private TextView userFlat;
    private TextView userEdit;
    private ImageView newPost;

    public UserProfileHeaderHolder(View itemView) {
        super(itemView);
        profilePic = (ImageView) itemView.findViewById(R.id.profile_pic);
        userName = (TextView) itemView.findViewById(R.id.user_name);
        userFlat = (TextView) itemView.findViewById(R.id.user_flat);
        userEdit = (TextView) itemView.findViewById(R.id.edit_profile);
        newPost = (ImageView) itemView.findViewById(R.id.new_post);
    }

    public void bindToUserProfileHeader(UserModel userModel, View.OnClickListener userEditClickListener, View.OnClickListener newPostClickListener) {
        Glide.with(profilePic.getContext()).load(userModel.getPhoto_profile()).centerCrop().transform(new bamfaltech.kutumb.adapter.CircleTransform(profilePic.getContext())).override(100,100).into(profilePic);
        userName.setText(userModel.getName());
        userFlat.setText(userModel.getFlatNumber());
        if(userModel.getId().equalsIgnoreCase(FirebaseManager.getInstance().getCurrentUser().getId())) {
            userEdit.setVisibility(View.VISIBLE);
            userEdit.setOnClickListener(userEditClickListener);
            newPost.setVisibility(View.VISIBLE);
            newPost.setOnClickListener(newPostClickListener);
        } else {
            userEdit.setVisibility(View.GONE);
            newPost.setVisibility(View.GONE);
        }

    }
}
