package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.NotificationModel;
import bamfaltech.kutumb.models.TopicModel;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 14/02/17.
 */

public class TopicViewHolder extends RecyclerView.ViewHolder {

    private TextView notificationText;
    private ImageView notificationImage;
    private LinearLayout topicContainer;


    public TopicViewHolder(View itemView) {
        super(itemView);
        topicContainer = (LinearLayout) itemView.findViewById(R.id.container);
        notificationText = (TextView)itemView.findViewById(R.id.notif_text);
        notificationImage = (ImageView)itemView.findViewById(R.id.notif_image);
    }

    public void bindToTopic(TopicModel topicModel, View.OnClickListener onClickListener) {
        topicContainer.setOnClickListener(onClickListener);
        notificationText.setText(topicModel.getTopic());
        notificationImage.setVisibility(View.GONE);
    }
}
