package bamfaltech.kutumb.viewholder;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.fragment.UtilityFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.services.Interfaces;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 22/02/17.
 */

public class UtilityCatagoryHeaderHolder extends PostViewHolder{

    private Spinner catagorySpinner;

    public UtilityCatagoryHeaderHolder(View itemView) {
        super(itemView);
        catagorySpinner = (Spinner) itemView.findViewById(R.id.catagory);
    }

    public void bindToUtilityHeader(Fragment fragment, Context activity, int count, DatabaseReference databaseReference) {
        if(count == 0) {
            getCatagoryList(databaseReference, fragment, activity);

            /*ArrayAdapter<String> towerListAdapter = new ArrayAdapter<String>(fragment.getActivity(), R.layout.spinner_list_item, Constants.catagoryList);
            towerListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            catagorySpinner.setAdapter(towerListAdapter);

            catagorySpinner.setOnItemSelectedListener((UtilityFragment) fragment);*/
        }
    }

    boolean isAutoCompleteCatagory = false;

    public void getCatagoryList(DatabaseReference databaseReference, final Fragment fragment, final Context activity) {
        DatabaseReference ref = databaseReference.child("catagory");
        FirebaseManager.getInstance().getTopicList(ref, new Interfaces.OnRetrievalCompleteListener() {
            @Override
            public void onRetrievalCompleted(Object object) {
                if(object != null && object instanceof List) {
                    List<String> catagoryStringList = (List)object;
                    catagoryStringList.add(0, "General/All");
                    Constants.catagoryList = catagoryStringList;
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                            R.layout.simple_spinner_item, Util.convertListToStringArray(catagoryStringList));
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    catagorySpinner.setAdapter(adapter);
                    catagorySpinner.setOnItemSelectedListener((UtilityFragment) fragment);

                }
            }

            @Override
            public void onError(String errorMsg) {

            }
        });
    }
}
