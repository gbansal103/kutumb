package bamfaltech.kutumb.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bamfaltech.kutumb.R;
import bamfaltech.kutumb.models.Post;
import bamfaltech.kutumb.util.Util;

/**
 * Created by gaurav.bansal1 on 16/02/17.
 */

public class ListHeaderViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public ImageView authorPic;

    public ListHeaderViewHolder(View itemView) {
        super(itemView);
        titleView = (TextView) itemView.findViewById(R.id.header_msg);
        authorPic = (ImageView) itemView.findViewById(R.id.memebr_pic);
    }

    public void bindToHeader(Post model) {
        titleView.setText(model.author);
        Util.bindCircularImage(authorPic, model.getPhoto_profile());
    }
}
