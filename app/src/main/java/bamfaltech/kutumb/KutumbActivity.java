package bamfaltech.kutumb;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import bamfaltech.kutumb.fragment.AboutusFragment;
import bamfaltech.kutumb.fragment.BaseKutumbFragment;
import bamfaltech.kutumb.fragment.CarPoolTabFragment;
import bamfaltech.kutumb.fragment.FeedbackFragment;
import bamfaltech.kutumb.fragment.FlatChooserFragment;
import bamfaltech.kutumb.fragment.ImportantContactFragment;
import bamfaltech.kutumb.fragment.NewCarPoolPostingFragment;
import bamfaltech.kutumb.fragment.NewNoticeFragment;
import bamfaltech.kutumb.fragment.NewPostFragment;
import bamfaltech.kutumb.fragment.NewUtilityPostFragment;
import bamfaltech.kutumb.fragment.NoticeFragment;
import bamfaltech.kutumb.fragment.NotificationFragment;
import bamfaltech.kutumb.fragment.PostDetailFragment;
import bamfaltech.kutumb.fragment.PrivacyPolicyFragment;
import bamfaltech.kutumb.fragment.SearchTabFragment;
import bamfaltech.kutumb.fragment.TabContainerFragment;
import bamfaltech.kutumb.fragment.UserProfileFragment;
import bamfaltech.kutumb.fragment.UtilityDetailFragment;
import bamfaltech.kutumb.managers.FirebaseManager;
import bamfaltech.kutumb.models.RWADetailModel;
import bamfaltech.kutumb.models.UserModel;
import bamfaltech.kutumb.services.Serializer;
import bamfaltech.kutumb.util.CircleTransform;
import bamfaltech.kutumb.util.Constants;
import bamfaltech.kutumb.util.DeviceResourceManager;

import static bamfaltech.kutumb.fragment.TabContainerFragment.SHOW_TAB_POSITION;

//import bamfaltech.kutumb.fragment.HomeFragment;

/**
 * Created by gaurav.bansal1 on 14/10/16.
 */
public class KutumbActivity extends AppCompatActivity implements View.OnClickListener{

    private Fragment mCurrentFragment;
    private static final String TAG = "KutumbActivity";

    private FragmentPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    private boolean isDrawerOpen = false;

    //new
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtFlat, txtRWAName;
    private Toolbar toolbar;
    private FloatingActionButton fab;

    // urls to load navigation header background image
    // and profile image
    private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    private static String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments
    private static final String TAG_HOME = "home";
    private static final String TAG_PERSONAL_CHAT = "personal_chat";
    private static final String TAG_NEW_CHAT = "new_chat";
    private static final String TAG_UTILITY = "utility";
    private static final String TAG_NOTIFICATIONS = "notifications";
    private static final String TAG_PROFILE = "profile";
    private static final String TAG_SEARCH = "search";
    private static final String TAG_IMP_CONTACTS = "contacts";
    private static final String TAG_CARPOOL = "carpool";
    private static final String TAG_FEEDBACK = "feedback";
    private static final String TAG_NOTICE = "notice";
    private static final String TAG_ABOUTUS = "aboutus";
    private static final String TAG_PRIVACY = "privacy";


    public static String CURRENT_TAG = TAG_HOME;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.kutumb_activity_new);
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.removeAllViews();
        *//*if (actionBar.getParent() != null) {
            ((ViewGroup) actionBar.getParent()).removeAllViews();
        }*//*
        DetailsMaterialActionBar materialActionBar = new DetailsMaterialActionBar(this);
        materialActionBar.setParams(mCurrentFragment, getPageTitle());
        toolbar.addView(materialActionBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.name);
        txtFlat = (TextView) navHeader.findViewById(R.id.flatno);
        txtRWAName = (TextView) navHeader.findViewById(R.id.rwaname);
        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        fab.setOnClickListener(this);

        urlProfileImg = getIntent().getStringExtra("userPhotoLink");
        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();


        //checkForUpdates();
        RWADetailModel.RWA currentRWA = (RWADetailModel.RWA) Serializer.deserialize(DeviceResourceManager.getInstance().getDataFromSharedPref(Constants.PREFERENCE_CURRENT_RWA_DETAILS, true));
        KutumbApplication.getInstance().setCurrentRWA(currentRWA);

        //handleTabContainerFragment(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            String fragmentLaunch = bundle.getString("fragmentLaunch");
            String contentKey = bundle.getString("contentKey");
            if(fragmentLaunch != null) {
                if (fragmentLaunch.equalsIgnoreCase("postDetail")) {
                    PostDetailFragment postDetailFragment = new PostDetailFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString(PostDetailFragment.EXTRA_POST_KEY, contentKey);
                    bundle1.putBoolean("handleDeeplink", true);
                    postDetailFragment.setArguments(bundle1);
                    displayFragment(postDetailFragment);
                } else if (fragmentLaunch.equalsIgnoreCase("utilityDetail")) {
                    UtilityDetailFragment utilityDetailFragment = new UtilityDetailFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString(UtilityDetailFragment.EXTRA_POST_KEY, contentKey);
                    bundle1.putBoolean("handleDeeplink", true);
                    utilityDetailFragment.setArguments(bundle1);
                    displayFragment(utilityDetailFragment);
                } else if (fragmentLaunch.equalsIgnoreCase("notificationFragment")) {
                    NotificationFragment notificationFragment = new NotificationFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putBoolean("handleDeeplink", true);
                    displayFragment(notificationFragment);
                }
            } else {
                handleTabContainerFragment(savedInstanceState);
            }
        } else {
            handleTabContainerFragment(savedInstanceState);
        }
    }


    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void handleTabContainerFragment(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            loadHomeFragment(0);
        } else {
            navItemIndex = 0;
            CURRENT_TAG = TAG_HOME;
            int tab = getIntent().getIntExtra("show_tab", 0);
        }
    }



    private String getPageTitle() {
        String title = "Prateek Laurel";
        return title;
    }

    public void setFabIcon(boolean isPlusIcon) {
        if(isPlusIcon) {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_image_add));
        } else {
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_image_edit));
        }
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }

    public void setCurrentFragment(Fragment fragment) {
        /*if(fragment instanceof RecentPostsFragment) {
            navItemIndex = 0;
        } else {
            navItemIndex = -1;
        }*/
        this.mCurrentFragment = fragment;
        if(mCurrentFragment instanceof TabContainerFragment
                || mCurrentFragment instanceof NoticeFragment) {
            fab.setVisibility(View.VISIBLE);
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_image_edit));
        } else if(mCurrentFragment instanceof NewPostFragment
                || mCurrentFragment instanceof NewUtilityPostFragment
                || mCurrentFragment instanceof FeedbackFragment
                || mCurrentFragment instanceof NewNoticeFragment
                || mCurrentFragment instanceof NewCarPoolPostingFragment) {
            fab.setVisibility(View.VISIBLE);
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_navigation_check_24));
        } else if(mCurrentFragment instanceof CarPoolTabFragment) {
            fab.setVisibility(View.VISIBLE);
            fab.setImageDrawable(getResources().getDrawable(R.drawable.ic_directions_car_black_24dp));
        } else {
            fab.setVisibility(View.GONE);
        }
    }


    public void displayFragment(BaseKutumbFragment fragment) {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                    android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment);
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
            setCurrentFragment(fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }



    private int currentPosition = 0;
    public void setCurrentPosition(int position) {
        if(position == 1) {
            setFabIcon(true);
        } else {
            setFabIcon(false);
        }
        this.currentPosition = position;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        UserModel userModel = FirebaseManager.getInstance().getCurrentUser();

        // name, website
        txtName.setText(userModel.getName());
        txtFlat.setText(userModel.getFlatNumber());
        if(userModel.getRwa() != null) {
            txtRWAName.setVisibility(View.VISIBLE);
            txtRWAName.setText(userModel.getRwa().getRwaname());
        }else {
            txtRWAName.setVisibility(View.GONE);
        }
        //imgNavHeaderBg.setImageDrawable(getResources().getDrawable(R.drawable.profile_background));

        /*// loading header background image
        Glide.with(this).load(urlNavHeaderBg)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);*/

        // Loading profile image
        Glide.with(this).load(userModel.getPhoto_profile())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);

        // showing dot next to notifications label
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment(final int tabPosition) {
        // selecting appropriate nav menu item
        //selectNavMenu();

        // set toolbar title
        //setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            //toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                BaseKutumbFragment fragment = getHomeFragment(tabPosition);
                displayFragment(fragment);
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        //toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private BaseKutumbFragment getHomeFragment(int tabPosition) {
        switch (navItemIndex) {
            case 0:
                // home
                TabContainerFragment homeFragment = new TabContainerFragment();
                Bundle bundle = new Bundle();
                bundle.putInt(SHOW_TAB_POSITION, tabPosition);
                return homeFragment;
           /* case 1:
                // photos
                PhotosFragment photosFragment = new PhotosFragment();
                return photosFragment;
            case 2:
                // movies fragment
                MoviesFragment moviesFragment = new MoviesFragment();
                return moviesFragment;
            case 3:
                // notifications fragment
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                return notificationsFragment;

            case 4:
                // settings fragment
                SettingsFragment settingsFragment = new SettingsFragment();
                return settingsFragment;*/
            default:
                return new TabContainerFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        drawer.closeDrawers();
                        TabContainerFragment homefragment = new TabContainerFragment();
                        displayFragment(homefragment);
                        //toggleFab();
                        break;
                    case R.id.nav_notification:
                        CURRENT_TAG = TAG_NOTIFICATIONS;
                        drawer.closeDrawers();
                        NotificationFragment fragment = new NotificationFragment();
                        displayFragment(fragment);
                        break;
                    case R.id.nav_profile:
                        CURRENT_TAG = TAG_PROFILE;
                        drawer.closeDrawers();
                        UserProfileFragment userProfileFragment = new UserProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("userModel", FirebaseManager.getInstance().getCurrentUser());
                        userProfileFragment.setArguments(bundle);
                        displayFragment(userProfileFragment);
                        break;
                    case R.id.nav_notice:
                        CURRENT_TAG = TAG_NOTICE;
                        drawer.closeDrawers();
                        NoticeFragment noticeFragment = new NoticeFragment();
                        displayFragment(noticeFragment);
                        break;
                    case R.id.nav_search:
                        CURRENT_TAG = TAG_SEARCH;
                        drawer.closeDrawers();
                        SearchTabFragment searchTabFragment = new SearchTabFragment();
                        displayFragment(searchTabFragment);
                        break;
                    case R.id.nav_contanct:
                        CURRENT_TAG = TAG_IMP_CONTACTS;
                        drawer.closeDrawers();
                        ImportantContactFragment importantContactFragment = new ImportantContactFragment();
                        displayFragment(importantContactFragment);
                        break;
                    case R.id.nav_carpool:
                        CURRENT_TAG = TAG_CARPOOL;
                        drawer.closeDrawers();
                        CarPoolTabFragment carPoolTabFragment = new CarPoolTabFragment();
                        displayFragment(carPoolTabFragment);
                        break;
                    case R.id.nav_feedback:
                        CURRENT_TAG = TAG_FEEDBACK;
                        drawer.closeDrawers();
                        FeedbackFragment feedbackFragment = new FeedbackFragment();
                        displayFragment(feedbackFragment);
                        break;
                    case R.id.nav_about_us:
                        CURRENT_TAG = TAG_ABOUTUS;
                        drawer.closeDrawers();
                        AboutusFragment aboutusFragment = new AboutusFragment();
                        displayFragment(aboutusFragment);
                        break;
                    case R.id.nav_privacy_policy:
                        CURRENT_TAG = TAG_PRIVACY;
                        drawer.closeDrawers();
                        PrivacyPolicyFragment privacyPolicyFragment = new PrivacyPolicyFragment();
                        displayFragment(privacyPolicyFragment);
                        break;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
               /* if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }*/
                menuItem.setChecked(true);

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private boolean doubleBackToExitPressedOnce = false;

    public void homeIconClick(boolean isHome) {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        } else if(!drawer.isDrawerOpen(GravityCompat.START) && isHome) {
            drawer.openDrawer(GravityCompat.START);
            return;
        } else {
            onBackPressedHandling();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        onBackPressedHandling();
    }

    private void onBackPressedHandling() {

        int count = getSupportFragmentManager().getBackStackEntryCount();
        if(count == 1) {
            if(mCurrentFragment instanceof TabContainerFragment) {
                performDoubleExit();
            } else {
                getSupportFragmentManager().popBackStack();
                handleTabContainerFragment(null);
            }
        } else {
            getSupportFragmentManager().popBackStack();
            //toggleFab();
        }
    }

    public void removeFragment() {
        getSupportFragmentManager().popBackStack();
        int index = getSupportFragmentManager().getBackStackEntryCount() - 2;
        android.support.v4.app.FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
        String tag = backEntry.getName();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        setCurrentFragment(fragment);
    }

    private void performDoubleExit(){
        if (shouldLoadHomeFragOnBackPress) {
            if (doubleBackToExitPressedOnce) {
                finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            showMySnackbar(this, "Press back again to exit", false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;

                }
            }, 5000);
        }
    }

    private void showMySnackbar(Context context, String message, boolean isLongDuration) {
        Snackbar snackbar = null;
        if(!isLongDuration) {
            snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), "" + message, Snackbar.LENGTH_SHORT);
        } else {
            snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), "" + message, Snackbar.LENGTH_LONG);
        }
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.snack_bar_background_color));
        snackbar.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 3) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            FirebaseManager.getInstance().signout();
            Toast.makeText(getApplicationContext(), "Logout user!", Toast.LENGTH_LONG).show();
            return true;
        }

        // user is in notifications fragment
        // and selected 'Mark all as Read'
        if (id == R.id.action_mark_all_read) {
            Toast.makeText(getApplicationContext(), "All notifications marked as read!", Toast.LENGTH_LONG).show();
        }

        // user is in notifications fragment
        // and selected 'Clear All'
        if (id == R.id.action_clear_notifications) {
            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

   /* // show or hide the fab
    private void toggleFab() {
        if (navItemIndex == 0)
            fab.show();
        else
            fab.hide();
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab:
                if(mCurrentFragment instanceof TabContainerFragment) {
                    if (currentPosition == 0) {
                        NewPostFragment fragment = new NewPostFragment();
                        displayFragment(fragment);
                    } else if (currentPosition == 1) {
                        FlatChooserFragment fragment = new FlatChooserFragment();
                        displayFragment(fragment);
                    } else if (currentPosition == 2) {
                        NewUtilityPostFragment fragment = new NewUtilityPostFragment();
                        displayFragment(fragment);
                    }
                } else if(mCurrentFragment instanceof NewPostFragment) {
                    ((NewPostFragment)mCurrentFragment).submitPost();
                } else if(mCurrentFragment instanceof NewUtilityPostFragment) {
                    ((NewUtilityPostFragment)mCurrentFragment).submitPost();
                } else if(mCurrentFragment instanceof NoticeFragment) {
                    NewNoticeFragment fragment = new NewNoticeFragment();
                    displayFragment(fragment);
                } else if(mCurrentFragment instanceof NewNoticeFragment) {
                    ((NewNoticeFragment)mCurrentFragment).submitNotice();
                } else if(mCurrentFragment instanceof FeedbackFragment) {
                    ((FeedbackFragment)mCurrentFragment).submitFeedback();
                } else if(mCurrentFragment instanceof NewCarPoolPostingFragment) {
                    ((NewCarPoolPostingFragment)mCurrentFragment).submitCarPoolPost();
                } else if(mCurrentFragment instanceof CarPoolTabFragment) {
                    NewCarPoolPostingFragment fragment = new NewCarPoolPostingFragment();
                    displayFragment(fragment);
                }
                break;
        }
    }
}